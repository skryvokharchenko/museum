<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/team'], function (Router $router) {
    $router->bind('department', function ($id) {
        return app('Modules\Team\Repositories\DepartmentRepository')->find($id);
    });
    $router->get('departments', [
        'as' => 'admin.team.department.index',
        'uses' => 'DepartmentController@index',
        'middleware' => 'can:team.departments.index'
    ]);
    $router->get('departments/create', [
        'as' => 'admin.team.department.create',
        'uses' => 'DepartmentController@create',
        'middleware' => 'can:team.departments.create'
    ]);
    $router->post('departments', [
        'as' => 'admin.team.department.store',
        'uses' => 'DepartmentController@store',
        'middleware' => 'can:team.departments.create'
    ]);
    $router->get('departments/{department}/edit', [
        'as' => 'admin.team.department.edit',
        'uses' => 'DepartmentController@edit',
        'middleware' => 'can:team.departments.edit'
    ]);
    $router->put('departments/{department}', [
        'as' => 'admin.team.department.update',
        'uses' => 'DepartmentController@update',
        'middleware' => 'can:team.departments.edit'
    ]);
    $router->delete('departments/{department}', [
        'as' => 'admin.team.department.destroy',
        'uses' => 'DepartmentController@destroy',
        'middleware' => 'can:team.departments.destroy'
    ]);
    $router->bind('member', function ($id) {
        return app('Modules\Team\Repositories\MemberRepository')->find($id);
    });
    $router->get('members', [
        'as' => 'admin.team.member.index',
        'uses' => 'MemberController@index',
        'middleware' => 'can:team.members.index'
    ]);
    $router->get('members/create', [
        'as' => 'admin.team.member.create',
        'uses' => 'MemberController@create',
        'middleware' => 'can:team.members.create'
    ]);
    $router->post('members', [
        'as' => 'admin.team.member.store',
        'uses' => 'MemberController@store',
        'middleware' => 'can:team.members.create'
    ]);
    $router->get('members/{member}/edit', [
        'as' => 'admin.team.member.edit',
        'uses' => 'MemberController@edit',
        'middleware' => 'can:team.members.edit'
    ]);
    $router->put('members/{member}', [
        'as' => 'admin.team.member.update',
        'uses' => 'MemberController@update',
        'middleware' => 'can:team.members.edit'
    ]);
    $router->delete('members/{member}', [
        'as' => 'admin.team.member.destroy',
        'uses' => 'MemberController@destroy',
        'middleware' => 'can:team.members.destroy'
    ]);
// append


});
