<?php

namespace Modules\Team\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Team\Entities\Member;
use Modules\Team\Http\Requests\CreateMemberRequest;
use Modules\Team\Http\Requests\UpdateMemberRequest;
use Modules\Team\Repositories\DepartmentRepository;
use Modules\Team\Repositories\MemberRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class MemberController extends AdminBaseController
{
    /**
     * @var MemberRepository
     */
    private $member;
    /**
     * @var DepartmentRepository
     */
    private $department;

    public function __construct(MemberRepository $member, DepartmentRepository $department)
    {
        parent::__construct();

        $this->member = $member;
        $this->department = $department;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $members = $this->member->all();

        return view('team::admin.members.index', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $departments = $this->department->all()->pluck('name', 'id')->toArray();
        return view('team::admin.members.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateMemberRequest $request
     * @return Response
     */
    public function store(CreateMemberRequest $request)
    {
        $this->member->create($request->all());

        return redirect()->route('admin.team.member.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('team::members.title.members')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Member $member
     * @return Response
     */
    public function edit(Member $member)
    {
        $departments = $this->department->all()->pluck('name', 'id')->toArray();
        return view('team::admin.members.edit', compact('member', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Member $member
     * @param  UpdateMemberRequest $request
     * @return Response
     */
    public function update(Member $member, UpdateMemberRequest $request)
    {
        $this->member->update($member, $request->all());

        return redirect()->route('admin.team.member.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('team::members.title.members')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Member $member
     * @return Response
     */
    public function destroy(Member $member)
    {
        $this->member->destroy($member);

        return redirect()->route('admin.team.member.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('team::members.title.members')]));
    }
}
