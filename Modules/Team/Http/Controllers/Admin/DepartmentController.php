<?php

namespace Modules\Team\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Team\Entities\Department;
use Modules\Team\Http\Requests\CreateDepartmentRequest;
use Modules\Team\Http\Requests\UpdateDepartmentRequest;
use Modules\Team\Repositories\DepartmentRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class DepartmentController extends AdminBaseController
{
    /**
     * @var DepartmentRepository
     */
    private $department;

    public function __construct(DepartmentRepository $department)
    {
        parent::__construct();

        $this->department = $department;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $departments = $this->department->all();

        return view('team::admin.departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('team::admin.departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateDepartmentRequest $request
     * @return Response
     */
    public function store(CreateDepartmentRequest $request)
    {
        $this->department->create($request->all());

        return redirect()->route('admin.team.department.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('team::departments.title.departments')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Department $department
     * @return Response
     */
    public function edit(Department $department)
    {
        return view('team::admin.departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Department $department
     * @param  UpdateDepartmentRequest $request
     * @return Response
     */
    public function update(Department $department, UpdateDepartmentRequest $request)
    {
        $this->department->update($department, $request->all());

        if ($request->expectsJson()) {
            return response()->json(['newValue' => $department->order]);
        }

        return redirect()->route('admin.team.department.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('team::departments.title.departments')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Department $department
     * @return Response
     */
    public function destroy(Department $department)
    {
        $this->department->destroy($department);

        return redirect()->route('admin.team.department.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('team::departments.title.departments')]));
    }
}
