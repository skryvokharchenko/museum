<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMemberTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team__member_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name', 100);
            $table->string('role', 100);
            $table->integer('member_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['member_id', 'locale']);
            $table->foreign('member_id')->references('id')->on('team__members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team__member_translations', function (Blueprint $table) {
            $table->dropForeign(['member_id']);
        });
        Schema::dropIfExists('team__member_translations');
    }
}
