<?php

return [
    'team.departments' => [
        'index' => 'team::departments.list resource',
        'create' => 'team::departments.create resource',
        'edit' => 'team::departments.edit resource',
        'destroy' => 'team::departments.destroy resource',
    ],
    'team.members' => [
        'index' => 'team::members.list resource',
        'create' => 'team::members.create resource',
        'edit' => 'team::members.edit resource',
        'destroy' => 'team::members.destroy resource',
    ],
// append


];
