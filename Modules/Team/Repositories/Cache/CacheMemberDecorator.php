<?php

namespace Modules\Team\Repositories\Cache;

use Modules\Team\Repositories\MemberRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheMemberDecorator extends BaseCacheDecorator implements MemberRepository
{
    public function __construct(MemberRepository $member)
    {
        parent::__construct();
        $this->entityName = 'team.members';
        $this->repository = $member;
    }
}
