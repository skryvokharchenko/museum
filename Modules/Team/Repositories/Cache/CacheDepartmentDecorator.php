<?php

namespace Modules\Team\Repositories\Cache;

use Modules\Team\Repositories\DepartmentRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDepartmentDecorator extends BaseCacheDecorator implements DepartmentRepository
{
    public function __construct(DepartmentRepository $department)
    {
        parent::__construct();
        $this->entityName = 'team.departments';
        $this->repository = $department;
    }
}
