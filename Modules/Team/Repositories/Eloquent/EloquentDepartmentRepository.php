<?php

namespace Modules\Team\Repositories\Eloquent;

use Modules\Team\Repositories\DepartmentRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDepartmentRepository extends EloquentBaseRepository implements DepartmentRepository
{
}
