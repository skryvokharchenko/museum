<?php

namespace Modules\Team\Repositories\Eloquent;

use Modules\Team\Repositories\MemberRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentMemberRepository extends EloquentBaseRepository implements MemberRepository
{
    use Mediable;
}
