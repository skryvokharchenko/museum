<?php


namespace Modules\Team\Repositories\Eloquent;



use App\Events\EntityWasCreatedOrUpdated;

trait Mediable
{
    public function create($data)
    {
        $item = parent::create($data);
        if (isset($data['departments'])) {
            $item->departments()->sync($data['departments']);
        }
        event(new EntityWasCreatedOrUpdated($item, $data));
        return $item;
    }

    public function update($model, $data)
    {
        $item = parent::update($model, $data);
        if (isset($data['departments'])) {
            $item->departments()->sync($data['departments']);
        }
        event(new EntityWasCreatedOrUpdated($item, $data));
        return $item;
    }
}
