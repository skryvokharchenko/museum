<?php

namespace Modules\Team\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface DepartmentRepository extends BaseRepository
{
}
