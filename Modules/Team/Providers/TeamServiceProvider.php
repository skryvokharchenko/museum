<?php

namespace Modules\Team\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Team\Events\Handlers\RegisterTeamSidebar;

class TeamServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterTeamSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('departments', array_dot(trans('team::departments')));
            $event->load('members', array_dot(trans('team::members')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('team', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Team\Repositories\DepartmentRepository',
            function () {
                $repository = new \Modules\Team\Repositories\Eloquent\EloquentDepartmentRepository(new \Modules\Team\Entities\Department());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Team\Repositories\Cache\CacheDepartmentDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Team\Repositories\MemberRepository',
            function () {
                $repository = new \Modules\Team\Repositories\Eloquent\EloquentMemberRepository(new \Modules\Team\Entities\Member());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Team\Repositories\Cache\CacheMemberDecorator($repository);
            }
        );
// add bindings


    }
}
