@foreach($departments as $department)
    <section>
        @php
            $className = '';
            if ($loop->first) {
                $className = 'type1';
            } elseif($loop->iteration&1) {
                $className = 'type2';
            }
        @endphp
        <div class="team_section {{ $className }} bg_color font_color">
            <div class="content_container">
                <div class="team_title">
                    <h2>{{$department->name}}</h2>
                    <p>{{$department->description}}</p>
                </div>

                <div class="row">
                    @foreach($department->members as $member)
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="team_item bg_color">
                                <div class="team_foto">
                                    <img src="{!! $member->image_url !!}" alt="{{ $member->alt_text }}">
                                </div>
                                <div class="team_info">
                                    <h3>{{ $member->name }}</h3>
                                    <p>{{ $member->role }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endforeach
