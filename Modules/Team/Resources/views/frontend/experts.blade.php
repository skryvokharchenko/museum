<div class="row">
    @foreach($members as $member)
        <div class="col-12 col-md-6 col-lg-4">
            <div class="expert_item bg_color">
                <div class="expert_info">
                    <h2>{{ $member->name }}</h2>
                    <p>{{ $member->role }}</p>
                </div>
            </div>
        </div>
    @endforeach
</div>
