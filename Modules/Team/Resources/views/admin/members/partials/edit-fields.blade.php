{!! Form::normalSelect('departments', trans('team::members.form.departments'), $errors, $departments, $member, ['class' => '', 'multiple' => 'multiple']) !!}

@mediaSingle('memberPhoto', $member, null, trans('team::members.form.photo'))
