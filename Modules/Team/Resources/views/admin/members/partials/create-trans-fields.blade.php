<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('team::members.form.name')) !!}
    {!! Form::text("{$lang}[name]", old("{$lang}[name]"), ["class" => "form-control", "placeholder" => trans('team::members.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[role]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[role]", trans('team::members.form.role')) !!}
    {!! Form::text("{$lang}[role]", old("{$lang}[role]"), ["class" => "form-control", "placeholder" => trans('team::members.form.role')]) !!}
    {!! $errors->first("{$lang}[role]", '<span class="help-block">:message</span>') !!}
</div>
