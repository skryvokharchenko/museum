{!! Form::normalSelect('departments', trans('team::members.form.departments'), $errors, $departments, null, ['class' => '', 'multiple' => 'multiple']) !!}

@mediaSingle('memberPhoto', null, null, trans('team::members.form.photo'))
