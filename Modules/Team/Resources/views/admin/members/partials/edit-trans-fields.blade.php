<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('team::members.form.name')) !!}
    <?php $old = $member->hasTranslation($lang) ? $member->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('team::members.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[role]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[role]", trans('team::members.form.role')) !!}
    <?php $old = $member->hasTranslation($lang) ? $member->translate($lang)->role : '' ?>
    {!! Form::text("{$lang}[role]", old("{$lang}[role]", $old), ["class" => "form-control", "placeholder" => trans('team::members.form.role')]) !!}
    {!! $errors->first("{$lang}[role]", '<span class="help-block">:message</span>') !!}
</div>
