<div class="form-group {{ $errors->has("show_team") ? ' has-error' : '' }}">
    <input type="hidden" name="show_team" value="0">
    {!! Form::checkbox("show_team", 1, true, ["class" => "flat-blue", "placeholder" => trans('team::departments.form.show_team')]) !!}
    {!! Form::label("show_team", trans('team::departments.form.show_team')) !!}
    {!! $errors->first("show_team", '<span class="help-block">:message</span>') !!}
</div>
