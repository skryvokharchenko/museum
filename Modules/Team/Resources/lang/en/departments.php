<?php

return [
    'list resource' => 'List departments',
    'create resource' => 'Create departments',
    'edit resource' => 'Edit departments',
    'destroy resource' => 'Destroy departments',
    'title' => [
        'departments' => 'Department',
        'create department' => 'Create a department',
        'edit department' => 'Edit a department',
    ],
    'button' => [
        'create department' => 'Create a department',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'description' => 'Description',
        'show_team' => 'Show on Team page',
        'order' => 'Order'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'front' => [
        'experts' => 'Experts'
    ]
];
