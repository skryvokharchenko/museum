<?php

return [
    'list resource' => 'List members',
    'create resource' => 'Create members',
    'edit resource' => 'Edit members',
    'destroy resource' => 'Destroy members',
    'title' => [
        'members' => 'Member',
        'create member' => 'Create a member',
        'edit member' => 'Edit a member',
    ],
    'button' => [
        'create member' => 'Create a member',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'role' => 'Role',
        'departments' => 'Departments',
        'photo' => 'Photo'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
