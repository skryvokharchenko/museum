<?php


namespace Modules\Team\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Team\Presenters\TeamPresenter;

class TeamFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return TeamPresenter::class;
    }
}
