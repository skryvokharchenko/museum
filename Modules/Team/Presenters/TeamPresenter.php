<?php


namespace Modules\Team\Presenters;


use Illuminate\Support\Facades\View;
use Modules\Team\Repositories\DepartmentRepository;
use Modules\Team\Repositories\MemberRepository;

class TeamPresenter
{
    /**
     * @var DepartmentRepository
     */
    private $department;
    /**
     * @var MemberRepository
     */
    private $member;

    public function __construct(DepartmentRepository $department, MemberRepository $member)
    {

        $this->department = $department;
        $this->member = $member;
    }

    public function departments()
    {
        $template = 'team::frontend.departments';

        $departments = $this->department->allWithBuilder()->where('show_team', true)->orderBy('order')->get();
        $departments->load('members');

        $view = View::make($template)->with(compact('departments'));
        return $view->render();
    }

    public function experts()
    {
        $template = 'team::frontend.experts';

        $department = $this->department->allWithBuilder()->with('members')->where('show_team', false)->first();
        $members = $department->members;

        $view = View::make($template)->with(compact('members'));
        return $view->render();
    }
}
