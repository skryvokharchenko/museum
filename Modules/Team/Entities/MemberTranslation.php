<?php

namespace Modules\Team\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'role'];
    protected $table = 'team__member_translations';
}
