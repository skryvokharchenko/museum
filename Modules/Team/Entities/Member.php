<?php

namespace Modules\Team\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class Member extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'team__members';
    public $translatedAttributes = ['name', 'role'];
    protected $fillable = ['name', 'role'];

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'team__department_member');
    }

    public function getImageUrlAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->path : '';
    }

    public function getAltTextAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->alt_attribute : '';
    }
}
