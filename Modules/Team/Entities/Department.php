<?php

namespace Modules\Team\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use Translatable;

    protected $table = 'team__departments';
    public $translatedAttributes = ['name', 'description'];
    protected $fillable = ['name', 'description', 'show_team', 'order'];

    protected $casts = [
        'show_team' => 'boolean',
    ];

    public function members()
    {
        return $this->belongsToMany(Member::class, 'team__department_member');
    }
}
