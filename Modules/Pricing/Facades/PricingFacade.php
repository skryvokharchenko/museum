<?php


namespace Modules\Pricing\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Pricing\Presenters\PricingPresenter;

class PricingFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return PricingPresenter::class;
    }
}
