<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/pricing'], function (Router $router) {
    $router->bind('category', function ($id) {
        return app('Modules\Pricing\Repositories\CategoryRepository')->find($id);
    });
    $router->get('categories', [
        'as' => 'admin.pricing.category.index',
        'uses' => 'CategoryController@index',
        'middleware' => 'can:pricing.categories.index'
    ]);
    $router->get('categories/create', [
        'as' => 'admin.pricing.category.create',
        'uses' => 'CategoryController@create',
        'middleware' => 'can:pricing.categories.create'
    ]);
    $router->post('categories', [
        'as' => 'admin.pricing.category.store',
        'uses' => 'CategoryController@store',
        'middleware' => 'can:pricing.categories.create'
    ]);
    $router->get('categories/{category}/edit', [
        'as' => 'admin.pricing.category.edit',
        'uses' => 'CategoryController@edit',
        'middleware' => 'can:pricing.categories.edit'
    ]);
    $router->put('categories/{category}', [
        'as' => 'admin.pricing.category.update',
        'uses' => 'CategoryController@update',
        'middleware' => 'can:pricing.categories.edit'
    ]);
    $router->delete('categories/{category}', [
        'as' => 'admin.pricing.category.destroy',
        'uses' => 'CategoryController@destroy',
        'middleware' => 'can:pricing.categories.destroy'
    ]);
    $router->bind('entry', function ($id) {
        return app('Modules\Pricing\Repositories\EntryRepository')->find($id);
    });
    $router->get('entries', [
        'as' => 'admin.pricing.entry.index',
        'uses' => 'EntryController@index',
        'middleware' => 'can:pricing.entries.index'
    ]);
    $router->get('entries/create', [
        'as' => 'admin.pricing.entry.create',
        'uses' => 'EntryController@create',
        'middleware' => 'can:pricing.entries.create'
    ]);
    $router->post('entries', [
        'as' => 'admin.pricing.entry.store',
        'uses' => 'EntryController@store',
        'middleware' => 'can:pricing.entries.create'
    ]);
    $router->get('entries/{entry}/edit', [
        'as' => 'admin.pricing.entry.edit',
        'uses' => 'EntryController@edit',
        'middleware' => 'can:pricing.entries.edit'
    ]);
    $router->put('entries/{entry}', [
        'as' => 'admin.pricing.entry.update',
        'uses' => 'EntryController@update',
        'middleware' => 'can:pricing.entries.edit'
    ]);
    $router->delete('entries/{entry}', [
        'as' => 'admin.pricing.entry.destroy',
        'uses' => 'EntryController@destroy',
        'middleware' => 'can:pricing.entries.destroy'
    ]);
    $router->bind('tour', function ($id) {
        return app('Modules\Pricing\Repositories\TourRepository')->find($id);
    });
    $router->get('tours', [
        'as' => 'admin.pricing.tour.index',
        'uses' => 'TourController@index',
        'middleware' => 'can:pricing.tours.index'
    ]);
    $router->get('tours/create', [
        'as' => 'admin.pricing.tour.create',
        'uses' => 'TourController@create',
        'middleware' => 'can:pricing.tours.create'
    ]);
    $router->post('tours', [
        'as' => 'admin.pricing.tour.store',
        'uses' => 'TourController@store',
        'middleware' => 'can:pricing.tours.create'
    ]);
    $router->get('tours/{tour}/edit', [
        'as' => 'admin.pricing.tour.edit',
        'uses' => 'TourController@edit',
        'middleware' => 'can:pricing.tours.edit'
    ]);
    $router->put('tours/{tour}', [
        'as' => 'admin.pricing.tour.update',
        'uses' => 'TourController@update',
        'middleware' => 'can:pricing.tours.edit'
    ]);
    $router->delete('tours/{tour}', [
        'as' => 'admin.pricing.tour.destroy',
        'uses' => 'TourController@destroy',
        'middleware' => 'can:pricing.tours.destroy'
    ]);
// append



});
