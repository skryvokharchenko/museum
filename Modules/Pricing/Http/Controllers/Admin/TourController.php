<?php

namespace Modules\Pricing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Pricing\Entities\Tour;
use Modules\Pricing\Http\Requests\CreateTourRequest;
use Modules\Pricing\Http\Requests\UpdateTourRequest;
use Modules\Pricing\Repositories\CategoryRepository;
use Modules\Pricing\Repositories\TourRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class TourController extends AdminBaseController
{
    /**
     * @var TourRepository
     */
    private $tour;
    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(TourRepository $tour, CategoryRepository $category)
    {
        parent::__construct();

        $this->tour = $tour;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tours = $this->tour->all();

        return view('pricing::admin.tours.index', compact('tours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = $this->category->all()->pluck('name', 'id');
        return view('pricing::admin.tours.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateTourRequest $request
     * @return Response
     */
    public function store(CreateTourRequest $request)
    {
        $this->tour->create($request->all());

        return redirect()->route('admin.pricing.tour.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pricing::tours.title.tours')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Tour $tour
     * @return Response
     */
    public function edit(Tour $tour)
    {
        $categories = $this->category->all()->pluck('name', 'id');
        return view('pricing::admin.tours.edit', compact('tour', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Tour $tour
     * @param  UpdateTourRequest $request
     * @return Response
     */
    public function update(Tour $tour, UpdateTourRequest $request)
    {
        $this->tour->update($tour, $request->all());

        return redirect()->route('admin.pricing.tour.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pricing::tours.title.tours')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Tour $tour
     * @return Response
     */
    public function destroy(Tour $tour)
    {
        $this->tour->destroy($tour);

        return redirect()->route('admin.pricing.tour.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pricing::tours.title.tours')]));
    }
}
