<?php

namespace Modules\Pricing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Pricing\Entities\Entry;
use Modules\Pricing\Http\Requests\CreateEntryRequest;
use Modules\Pricing\Http\Requests\UpdateEntryRequest;
use Modules\Pricing\Repositories\EntryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class EntryController extends AdminBaseController
{
    /**
     * @var EntryRepository
     */
    private $entry;

    public function __construct(EntryRepository $entry)
    {
        parent::__construct();

        $this->entry = $entry;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $entries = $this->entry->all();

        return view('pricing::admin.entries.index', compact('entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pricing::admin.entries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateEntryRequest $request
     * @return Response
     */
    public function store(CreateEntryRequest $request)
    {
        $this->entry->create($request->all());

        return redirect()->route('admin.pricing.entry.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pricing::entries.title.entries')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Entry $entry
     * @return Response
     */
    public function edit(Entry $entry)
    {
        return view('pricing::admin.entries.edit', compact('entry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Entry $entry
     * @param  UpdateEntryRequest $request
     * @return Response
     */
    public function update(Entry $entry, UpdateEntryRequest $request)
    {
        $this->entry->update($entry, $request->all());

        return redirect()->route('admin.pricing.entry.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pricing::entries.title.entries')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Entry $entry
     * @return Response
     */
    public function destroy(Entry $entry)
    {
        $this->entry->destroy($entry);

        return redirect()->route('admin.pricing.entry.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pricing::entries.title.entries')]));
    }
}
