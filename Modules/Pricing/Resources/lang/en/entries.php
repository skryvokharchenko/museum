<?php

return [
    'list resource' => 'List entries',
    'create resource' => 'Create entries',
    'edit resource' => 'Edit entries',
    'destroy resource' => 'Destroy entries',
    'title' => [
        'entries' => 'Entry',
        'create entry' => 'Create a entry',
        'edit entry' => 'Edit a entry',
    ],
    'button' => [
        'create entry' => 'Create a entry',
    ],
    'table' => [
    ],
    'form' => [
        'full_price' => 'Adults',
        'students_price' => 'Students',
        'retiree_price' => 'Pupils and retirees',
        'name' => 'Name',
        'description' => 'Description'
    ],
    'front' => [
        'full_price' => 'adults',
        'students_price' => 'students',
        'retiree_price' => 'pupils | retirees',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
