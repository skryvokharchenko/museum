<?php

return [
    'list resource' => 'List tours',
    'create resource' => 'Create tours',
    'edit resource' => 'Edit tours',
    'destroy resource' => 'Destroy tours',
    'title' => [
        'tours' => 'Tour',
        'create tour' => 'Create a tour',
        'edit tour' => 'Edit a tour',
    ],
    'button' => [
        'create tour' => 'Create a tour',
    ],
    'table' => [
    ],
    'form' => [
        'category_id' => 'Category',
        'selected' => 'Selected',
        'name' => 'Name',
        'description' => 'Description',
        'price' => 'Price',
        'price_notice' => 'Price notice'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
