<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('pricing::entries.form.name')) !!}
    <?php $old = $entry->hasTranslation($lang) ? $entry->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('pricing::entries.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('pricing::entries.form.description')) !!}
    <?php $old = $entry->hasTranslation($lang) ? $entry->translate($lang)->description : '' ?>
    {!! Form::text("{$lang}[description]", old("{$lang}[description]", $old), ["class" => "form-control", "placeholder" => trans('pricing::entries.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>
