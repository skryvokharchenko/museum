<div class="form-group {{ $errors->has("full_price") ? ' has-error' : '' }}">
    {!! Form::label("full_price", trans('pricing::entries.form.full_price')) !!}
    {!! Form::number("full_price", old("full_price", $entry->full_price), ["class" => "form-control", "placeholder" => trans('pricing::entries.form.full_price')]) !!}
    {!! $errors->first("full_price", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("students_price") ? ' has-error' : '' }}">
    {!! Form::label("students_price", trans('pricing::entries.form.students_price')) !!}
    {!! Form::number("students_price", old("students_price", $entry->students_price), ["class" => "form-control", "placeholder" => trans('pricing::entries.form.students_price')]) !!}
    {!! $errors->first("students_price", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("retiree_price") ? ' has-error' : '' }}">
    {!! Form::label("retiree_price", trans('pricing::entries.form.retiree_price')) !!}
    {!! Form::number("retiree_price", old("retiree_price", $entry->retiree_price), ["class" => "form-control", "placeholder" => trans('pricing::entries.form.retiree_price')]) !!}
    {!! $errors->first("retiree_price", '<span class="help-block">:message</span>') !!}
</div>
