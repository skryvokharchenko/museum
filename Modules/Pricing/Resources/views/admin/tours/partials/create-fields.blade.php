<div class="form-group {{ $errors->has("category_id") ? ' has-error' : '' }}">
    {!! Form::label("category_id", trans('pricing::tours.form.category_id')) !!}
    {!! Form::select("category_id", $categories, old("category_id"), ["class" => "form-control"]) !!}
    {!! $errors->first("category_id", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("selected") ? ' has-error' : '' }}">
    {!! Form::checkbox("selected", 1, old('selected'), ["class" => "flat-blue"]) !!}
    {!! Form::label("selected", trans('pricing::tours.form.selected')) !!}
    {!! $errors->first("selected", '<span class="help-block">:message</span>') !!}
</div>
