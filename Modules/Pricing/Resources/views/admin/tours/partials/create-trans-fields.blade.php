<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('pricing::tours.form.name')) !!}
    {!! Form::text("{$lang}[name]", old("{$lang}[name]"), ["class" => "form-control", "placeholder" => trans('pricing::tours.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('pricing::tours.form.description')) !!}
    {!! Form::text("{$lang}[description]", old("{$lang}[description]"), ["class" => "form-control", "placeholder" => trans('pricing::tours.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[price]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[price]", trans('pricing::tours.form.price')) !!}
    {!! Form::text("{$lang}[price]", old("{$lang}[price]"), ["class" => "form-control", "placeholder" => trans('pricing::tours.form.price')]) !!}
    {!! $errors->first("{$lang}[price]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[price_notice]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[price_notice]", trans('pricing::tours.form.price_notice')) !!}
    {!! Form::text("{$lang}[price_notice]", old("{$lang}[price_notice]"), ["class" => "form-control", "placeholder" => trans('pricing::tours.form.price_notice')]) !!}
    {!! $errors->first("{$lang}[price_notice]", '<span class="help-block">:message</span>') !!}
</div>
