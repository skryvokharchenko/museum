<div class="form-group {{ $errors->has("field") ? ' has-error' : '' }}">
    {!! Form::label("field", trans('pricing::categories.form.field')) !!}
    {!! Form::text("field", old("field"), ["class" => "form-control", "placeholder" => trans('pricing::categories.form.field')]) !!}
    {!! $errors->first("field", '<span class="help-block">:message</span>') !!}
</div>
