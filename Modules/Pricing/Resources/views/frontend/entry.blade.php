<div class="cost_item_head font_color bg_color">
    <div class="row h-100 align-items-center">
        <div class="col-0 col-lg-3"></div>
        <div class="col-4 col-lg-3">
            <div class="cost_it_head font_color">
                <h4>{{ trans('pricing::entries.front.full_price') }}</h4>
            </div>
        </div>
        <div class="col-4 col-lg-3">
            <div class="cost_it_head font_color">
                <h4>{{ trans('pricing::entries.front.students_price') }}*</h4>
            </div>
        </div>
        <div class="col-4 col-lg-3">
            <div class="cost_it_head font_color">
                <h4>{{ trans('pricing::entries.front.retiree_price') }}*</h4>
            </div>
        </div>
    </div>
</div>
@foreach($prices as $price)
<div class="cost_item font_color bg_color">
    <div class="row h-100 align-items-end align-items-lg-stretch">
        <div class="col-12 col-lg-3">
            <div class="cost_it_container mod1 bg_color">
                <div class="cost_it_info">
                    <h2>{{ $price->name }}</h2>
                    <p>{{ $price->description }}</p>
                </div>
            </div>
        </div>

        @if($loop->first)
            <div class="col-4 col-lg-0 head_adaptive">
                <div class="cost_it_head font_color">
                    <h4>{{ trans('pricing::entries.front.full_price') }}</h4>
                </div>
            </div>
            <div class="col-4 col-lg-0 head_adaptive">
                <div class="cost_it_head font_color">
                    <h4>{{ trans('pricing::entries.front.students_price') }}*</h4>
                </div>
            </div>
            <div class="col-4 col-lg-0 head_adaptive">
                <div class="cost_it_head font_color">
                    <h4>{{ trans('pricing::entries.front.retiree_price') }}*</h4>
                </div>
            </div>
        @endif

        <div class="col-4 col-lg-3">
            <div class="cost_it_container mod2 bg_color">
                <div class="cost_it_info">
                    <h3>{{ $price->full_price }}</h3>
                </div>
            </div>
        </div>
        <div class="col-4 col-lg-3">
            <div class="cost_it_container bg_color">
                <div class="cost_it_info">
                    <h3>{{ $price->students_price }}</h3>
                </div>
            </div>
        </div>
        <div class="col-4 col-lg-3">
            <div class="cost_it_container bg_color">
                <div class="cost_it_info">
                    <h3>{{ $price->retiree_price }}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
