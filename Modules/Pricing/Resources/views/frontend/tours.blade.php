@foreach($categories as $category)
    <div class="cost_section_title mod1">
        <h2>{{ $category->name }}:</h2>
    </div>
    <div class="cost_section_text">
        <p>{{ $category->description }}</p>
    </div>
    @foreach($category->tours as $tour)
        <div class="cost_item mod1 font_color bg_color">
            <div class="row h-100 ">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="cost_it_container type1 bg_color">
                        <div class="cost_it_info type1">
                            <h2 class="{{ ($tour->selected ? 'cost_it_color font_color' : '') }}">{{ $tour->name }}</h2>
                            @if($tour->description)
                                <p>{{ $tour->description }}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="cost_it_price font_color">
                        @if ($tour->price)
                            <h3>{{ $tour->price }}</h3>
                        @endif
                        @if ($tour->price_notice)
                            <p>{{ $tour->price_notice }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="cost_it_btn font_color">
                        <a href="#" role="button" aria-label="{{ trans('pricing::pricings.send request') }}" title="{{ trans('pricing::pricings.send request') }}" class="cost_btn font_color">{{ trans('pricing::pricings.send request') }}</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endforeach
