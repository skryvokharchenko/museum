<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingTourTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing__tour_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name', 100);
            $table->string('description', 100)->nullable();
            $table->string('price', 20)->nullable();
            $table->string('price_notice', 60)->nullable();
            $table->integer('tour_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['tour_id', 'locale']);
            $table->foreign('tour_id')->references('id')->on('pricing__tours')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pricing__tour_translations', function (Blueprint $table) {
            $table->dropForeign(['tour_id']);
        });
        Schema::dropIfExists('pricing__tour_translations');
    }
}
