<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingEntryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing__entry_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name', 100);
            $table->string('description', 100)->nullable();
            $table->integer('entry_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['entry_id', 'locale']);
            $table->foreign('entry_id')->references('id')->on('pricing__entries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pricing__entry_translations', function (Blueprint $table) {
            $table->dropForeign(['entry_id']);
        });
        Schema::dropIfExists('pricing__entry_translations');
    }
}
