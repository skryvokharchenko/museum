<?php

namespace Modules\Pricing\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface EntryRepository extends BaseRepository
{
}
