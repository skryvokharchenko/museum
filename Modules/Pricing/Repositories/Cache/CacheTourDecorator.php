<?php

namespace Modules\Pricing\Repositories\Cache;

use Modules\Pricing\Repositories\TourRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTourDecorator extends BaseCacheDecorator implements TourRepository
{
    public function __construct(TourRepository $tour)
    {
        parent::__construct();
        $this->entityName = 'pricing.tours';
        $this->repository = $tour;
    }
}
