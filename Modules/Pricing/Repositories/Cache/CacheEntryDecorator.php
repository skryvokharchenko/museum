<?php

namespace Modules\Pricing\Repositories\Cache;

use Modules\Pricing\Repositories\EntryRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheEntryDecorator extends BaseCacheDecorator implements EntryRepository
{
    public function __construct(EntryRepository $entry)
    {
        parent::__construct();
        $this->entityName = 'pricing.entries';
        $this->repository = $entry;
    }
}
