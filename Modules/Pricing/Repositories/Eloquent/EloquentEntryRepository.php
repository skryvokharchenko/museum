<?php

namespace Modules\Pricing\Repositories\Eloquent;

use Modules\Pricing\Repositories\EntryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentEntryRepository extends EloquentBaseRepository implements EntryRepository
{
}
