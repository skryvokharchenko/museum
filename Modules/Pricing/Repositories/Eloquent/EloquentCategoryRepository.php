<?php

namespace Modules\Pricing\Repositories\Eloquent;

use Modules\Pricing\Repositories\CategoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCategoryRepository extends EloquentBaseRepository implements CategoryRepository
{
}
