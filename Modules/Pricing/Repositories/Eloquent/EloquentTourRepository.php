<?php

namespace Modules\Pricing\Repositories\Eloquent;

use Modules\Pricing\Repositories\TourRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTourRepository extends EloquentBaseRepository implements TourRepository
{
}
