<?php

namespace Modules\Pricing\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Pricing\Events\Handlers\RegisterPricingSidebar;

class PricingServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterPricingSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('categories', array_dot(trans('pricing::categories')));
            $event->load('entries', array_dot(trans('pricing::entries')));
            $event->load('tours', array_dot(trans('pricing::tours')));
            // append translations



        });
    }

    public function boot()
    {
        $this->publishConfig('pricing', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Pricing\Repositories\CategoryRepository',
            function () {
                $repository = new \Modules\Pricing\Repositories\Eloquent\EloquentCategoryRepository(new \Modules\Pricing\Entities\Category());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pricing\Repositories\Cache\CacheCategoryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pricing\Repositories\EntryRepository',
            function () {
                $repository = new \Modules\Pricing\Repositories\Eloquent\EloquentEntryRepository(new \Modules\Pricing\Entities\Entry());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pricing\Repositories\Cache\CacheEntryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pricing\Repositories\TourRepository',
            function () {
                $repository = new \Modules\Pricing\Repositories\Eloquent\EloquentTourRepository(new \Modules\Pricing\Entities\Tour());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pricing\Repositories\Cache\CacheTourDecorator($repository);
            }
        );
// add bindings



    }
}
