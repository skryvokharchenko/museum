<?php


namespace Modules\Pricing\Presenters;


use Illuminate\Support\Facades\View;
use Modules\Pricing\Repositories\CategoryRepository;
use Modules\Pricing\Repositories\EntryRepository;

class PricingPresenter
{
    /**
     * @var EntryRepository
     */
    private $entry;
    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(EntryRepository $entry, CategoryRepository $category)
    {

        $this->entry = $entry;
        $this->category = $category;
    }

    public function entry()
    {
        $template = 'pricing::frontend.entry';
        $prices = $this->entry->all();

        $view = View::make($template)->with(compact('prices'));
        return $view->render();
    }

    public function tours()
    {
        $template = 'pricing::frontend.tours';
        $categories = $this->category->all();
        $categories->load('tours');

        $view = View::make($template)->with(compact('categories'));
        return $view->render();
    }

}
