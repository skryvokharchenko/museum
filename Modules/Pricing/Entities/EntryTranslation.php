<?php

namespace Modules\Pricing\Entities;

use Illuminate\Database\Eloquent\Model;

class EntryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'description'];
    protected $table = 'pricing__entry_translations';
}
