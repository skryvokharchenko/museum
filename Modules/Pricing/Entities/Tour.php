<?php

namespace Modules\Pricing\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    use Translatable;

    protected $table = 'pricing__tours';
    public $translatedAttributes = [
        'name',
        'description',
        'price',
        'price_notice'
    ];
    protected $fillable = [
        'category_id',
        'selected',
        'name',
        'description',
        'price',
        'price_notice'
    ];

    protected $casts = [
        'selected' => 'boolean'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
