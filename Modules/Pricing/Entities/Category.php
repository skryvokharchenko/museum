<?php

namespace Modules\Pricing\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Translatable;

    protected $table = 'pricing__categories';
    public $translatedAttributes = ['name', 'description'];
    protected $fillable = ['name', 'description'];

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }
}
