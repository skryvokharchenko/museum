<?php

namespace Modules\Pricing\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use Translatable;

    protected $table = 'pricing__entries';
    public $translatedAttributes = ['name', 'description'];
    protected $fillable = [
        'full_price',
        'students_price',
        'retiree_price',
        'name',
        'description'
    ];
}
