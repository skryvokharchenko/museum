<?php

namespace Modules\Pricing\Entities;

use Illuminate\Database\Eloquent\Model;

class TourTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'price',
        'price_notice'];
    protected $table = 'pricing__tour_translations';
}
