<?php

return [
    'pricing.categories' => [
        'index' => 'pricing::categories.list resource',
        'create' => 'pricing::categories.create resource',
        'edit' => 'pricing::categories.edit resource',
        'destroy' => 'pricing::categories.destroy resource',
    ],
    'pricing.entries' => [
        'index' => 'pricing::entries.list resource',
        'create' => 'pricing::entries.create resource',
        'edit' => 'pricing::entries.edit resource',
        'destroy' => 'pricing::entries.destroy resource',
    ],
    'pricing.tours' => [
        'index' => 'pricing::tours.list resource',
        'create' => 'pricing::tours.create resource',
        'edit' => 'pricing::tours.edit resource',
        'destroy' => 'pricing::tours.destroy resource',
    ],
// append



];
