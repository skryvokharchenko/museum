<?php

namespace Modules\Pricing\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterPricingSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('pricing::pricings.title.pricings'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('pricing::categories.title.categories'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.pricing.category.create');
                    $item->route('admin.pricing.category.index');
                    $item->authorize(
                        $this->auth->hasAccess('pricing.categories.index')
                    );
                });
                $item->item(trans('pricing::entries.title.entries'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.pricing.entry.create');
                    $item->route('admin.pricing.entry.index');
                    $item->authorize(
                        $this->auth->hasAccess('pricing.entries.index')
                    );
                });
                $item->item(trans('pricing::tours.title.tours'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.pricing.tour.create');
                    $item->route('admin.pricing.tour.index');
                    $item->authorize(
                        $this->auth->hasAccess('pricing.tours.index')
                    );
                });
// append



            });
        });

        return $menu;
    }
}
