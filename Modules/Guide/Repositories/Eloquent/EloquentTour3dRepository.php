<?php

namespace Modules\Guide\Repositories\Eloquent;

use Modules\Guide\Repositories\Tour3dRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTour3dRepository extends EloquentBaseRepository implements Tour3dRepository
{
}
