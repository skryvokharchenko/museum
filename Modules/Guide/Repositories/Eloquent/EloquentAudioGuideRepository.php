<?php

namespace Modules\Guide\Repositories\Eloquent;

use App\Repositories\Traits\Mediable;
use Modules\Guide\Repositories\AudioGuideRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentAudioGuideRepository extends EloquentBaseRepository implements AudioGuideRepository
{
    use Mediable;
}
