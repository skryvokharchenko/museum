<?php

namespace Modules\Guide\Repositories\Cache;

use Modules\Guide\Repositories\AudioGuideRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheAudioGuideDecorator extends BaseCacheDecorator implements AudioGuideRepository
{
    public function __construct(AudioGuideRepository $audioguide)
    {
        parent::__construct();
        $this->entityName = 'guide.audioguides';
        $this->repository = $audioguide;
    }
}
