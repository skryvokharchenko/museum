<?php

namespace Modules\Guide\Repositories\Cache;

use Modules\Guide\Repositories\Tour3dRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTour3dDecorator extends BaseCacheDecorator implements Tour3dRepository
{
    public function __construct(Tour3dRepository $tour3d)
    {
        parent::__construct();
        $this->entityName = 'guide.tour3ds';
        $this->repository = $tour3d;
    }
}
