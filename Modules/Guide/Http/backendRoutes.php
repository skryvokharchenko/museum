<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/guide'], function (Router $router) {
    $router->bind('audioguide', function ($id) {
        return app('Modules\Guide\Repositories\AudioGuideRepository')->find($id);
    });
    $router->get('audioguides', [
        'as' => 'admin.guide.audioguide.index',
        'uses' => 'AudioGuideController@index',
        'middleware' => 'can:guide.audioguides.index'
    ]);
    $router->get('audioguides/create', [
        'as' => 'admin.guide.audioguide.create',
        'uses' => 'AudioGuideController@create',
        'middleware' => 'can:guide.audioguides.create'
    ]);
    $router->post('audioguides', [
        'as' => 'admin.guide.audioguide.store',
        'uses' => 'AudioGuideController@store',
        'middleware' => 'can:guide.audioguides.create'
    ]);
    $router->get('audioguides/{audioguide}/edit', [
        'as' => 'admin.guide.audioguide.edit',
        'uses' => 'AudioGuideController@edit',
        'middleware' => 'can:guide.audioguides.edit'
    ]);
    $router->put('audioguides/{audioguide}', [
        'as' => 'admin.guide.audioguide.update',
        'uses' => 'AudioGuideController@update',
        'middleware' => 'can:guide.audioguides.edit'
    ]);
    $router->delete('audioguides/{audioguide}', [
        'as' => 'admin.guide.audioguide.destroy',
        'uses' => 'AudioGuideController@destroy',
        'middleware' => 'can:guide.audioguides.destroy'
    ]);
    $router->bind('tour3d', function ($id) {
        return app('Modules\Guide\Repositories\Tour3dRepository')->find($id);
    });
    $router->get('tour3ds', [
        'as' => 'admin.guide.tour3d.index',
        'uses' => 'Tour3dController@index',
        'middleware' => 'can:guide.tour3ds.index'
    ]);
    $router->get('tour3ds/create', [
        'as' => 'admin.guide.tour3d.create',
        'uses' => 'Tour3dController@create',
        'middleware' => 'can:guide.tour3ds.create'
    ]);
    $router->post('tour3ds', [
        'as' => 'admin.guide.tour3d.store',
        'uses' => 'Tour3dController@store',
        'middleware' => 'can:guide.tour3ds.create'
    ]);
    $router->get('tour3ds/{tour3d}/edit', [
        'as' => 'admin.guide.tour3d.edit',
        'uses' => 'Tour3dController@edit',
        'middleware' => 'can:guide.tour3ds.edit'
    ]);
    $router->put('tour3ds/{tour3d}', [
        'as' => 'admin.guide.tour3d.update',
        'uses' => 'Tour3dController@update',
        'middleware' => 'can:guide.tour3ds.edit'
    ]);
    $router->delete('tour3ds/{tour3d}', [
        'as' => 'admin.guide.tour3d.destroy',
        'uses' => 'Tour3dController@destroy',
        'middleware' => 'can:guide.tour3ds.destroy'
    ]);
// append


});
