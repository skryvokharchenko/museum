<?php

namespace Modules\Guide\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Guide\Entities\AudioGuide;
use Modules\Guide\Http\Requests\CreateAudioGuideRequest;
use Modules\Guide\Http\Requests\UpdateAudioGuideRequest;
use Modules\Guide\Repositories\AudioGuideRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class AudioGuideController extends AdminBaseController
{
    /**
     * @var AudioGuideRepository
     */
    private $audioguide;

    public function __construct(AudioGuideRepository $audioguide)
    {
        parent::__construct();

        $this->audioguide = $audioguide;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $audioguides = $this->audioguide->all();

        return view('guide::admin.audioguides.index', compact('audioguides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('guide::admin.audioguides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateAudioGuideRequest $request
     * @return Response
     */
    public function store(CreateAudioGuideRequest $request)
    {
        $this->audioguide->create($request->all());

        return redirect()->route('admin.guide.audioguide.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('guide::audioguides.title.audioguides')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  AudioGuide $audioguide
     * @return Response
     */
    public function edit(AudioGuide $audioguide)
    {
        return view('guide::admin.audioguides.edit', compact('audioguide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AudioGuide $audioguide
     * @param  UpdateAudioGuideRequest $request
     * @return Response
     */
    public function update(AudioGuide $audioguide, UpdateAudioGuideRequest $request)
    {
        $this->audioguide->update($audioguide, $request->all());

        return redirect()->route('admin.guide.audioguide.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('guide::audioguides.title.audioguides')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  AudioGuide $audioguide
     * @return Response
     */
    public function destroy(AudioGuide $audioguide)
    {
        $this->audioguide->destroy($audioguide);

        return redirect()->route('admin.guide.audioguide.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('guide::audioguides.title.audioguides')]));
    }
}
