<?php

namespace Modules\Guide\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Guide\Entities\Tour3d;
use Modules\Guide\Http\Requests\CreateTour3dRequest;
use Modules\Guide\Http\Requests\UpdateTour3dRequest;
use Modules\Guide\Repositories\Tour3dRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class Tour3dController extends AdminBaseController
{
    /**
     * @var Tour3dRepository
     */
    private $tour3d;

    public function __construct(Tour3dRepository $tour3d)
    {
        parent::__construct();

        $this->tour3d = $tour3d;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$tour3ds = $this->tour3d->all();

        return view('guide::admin.tour3ds.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('guide::admin.tour3ds.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateTour3dRequest $request
     * @return Response
     */
    public function store(CreateTour3dRequest $request)
    {
        $this->tour3d->create($request->all());

        return redirect()->route('admin.guide.tour3d.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('guide::tour3ds.title.tour3ds')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Tour3d $tour3d
     * @return Response
     */
    public function edit(Tour3d $tour3d)
    {
        return view('guide::admin.tour3ds.edit', compact('tour3d'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Tour3d $tour3d
     * @param  UpdateTour3dRequest $request
     * @return Response
     */
    public function update(Tour3d $tour3d, UpdateTour3dRequest $request)
    {
        $this->tour3d->update($tour3d, $request->all());

        return redirect()->route('admin.guide.tour3d.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('guide::tour3ds.title.tour3ds')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Tour3d $tour3d
     * @return Response
     */
    public function destroy(Tour3d $tour3d)
    {
        $this->tour3d->destroy($tour3d);

        return redirect()->route('admin.guide.tour3d.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('guide::tour3ds.title.tour3ds')]));
    }
}
