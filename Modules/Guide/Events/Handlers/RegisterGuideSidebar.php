<?php

namespace Modules\Guide\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterGuideSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('guide::guides.title.guides'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('guide::audioguides.title.audioguides'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.guide.audioguide.create');
                    $item->route('admin.guide.audioguide.index');
                    $item->authorize(
                        $this->auth->hasAccess('guide.audioguides.index')
                    );
                });
//                $item->item(trans('guide::tour3ds.title.tour3ds'), function (Item $item) {
//                    $item->icon('fa fa-copy');
//                    $item->weight(0);
//                    $item->append('admin.guide.tour3d.create');
//                    $item->route('admin.guide.tour3d.index');
//                    $item->authorize(
//                        $this->auth->hasAccess('guide.tour3ds.index')
//                    );
//                });
// append


            });
        });

        return $menu;
    }
}
