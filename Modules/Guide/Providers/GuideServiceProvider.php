<?php

namespace Modules\Guide\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Guide\Events\Handlers\RegisterGuideSidebar;

class GuideServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterGuideSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('audioguides', array_dot(trans('guide::audioguides')));
            $event->load('tour3ds', array_dot(trans('guide::tour3ds')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('guide', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Guide\Repositories\AudioGuideRepository',
            function () {
                $repository = new \Modules\Guide\Repositories\Eloquent\EloquentAudioGuideRepository(new \Modules\Guide\Entities\AudioGuide());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Guide\Repositories\Cache\CacheAudioGuideDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Guide\Repositories\Tour3dRepository',
            function () {
                $repository = new \Modules\Guide\Repositories\Eloquent\EloquentTour3dRepository(new \Modules\Guide\Entities\Tour3d());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Guide\Repositories\Cache\CacheTour3dDecorator($repository);
            }
        );
// add bindings


    }
}
