<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 28.10.2019
 * Time: 08:04
 */

namespace Modules\Guide\Presenters;


use Illuminate\Support\Facades\View;
use Modules\Guide\Repositories\AudioGuideRepository;

class AudioGuidePresenter
{
    /**
     * @var AudioGuideRepository
     */
    private $guide;

    public function __construct(AudioGuideRepository $guide)
    {

        $this->guide = $guide;
    }

    public function list()
    {
        $audios = $this->guide->allWithBuilder()->orderBy('created_at')->get();

        $view = View::make('guide::frontend.items')->with(compact('audios'));
        return $view->render();
    }
}