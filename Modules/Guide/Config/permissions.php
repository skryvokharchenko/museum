<?php

return [
    'guide.audioguides' => [
        'index' => 'guide::audioguides.list resource',
        'create' => 'guide::audioguides.create resource',
        'edit' => 'guide::audioguides.edit resource',
        'destroy' => 'guide::audioguides.destroy resource',
    ],
    'guide.tour3ds' => [
        'index' => 'guide::tour3ds.list resource',
        'create' => 'guide::tour3ds.create resource',
        'edit' => 'guide::tour3ds.edit resource',
        'destroy' => 'guide::tour3ds.destroy resource',
    ],
// append


];
