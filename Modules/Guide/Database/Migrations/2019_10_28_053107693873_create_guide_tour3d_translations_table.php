<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuideTour3dTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guide__tour3d_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('tour3d_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['tour3d_id', 'locale']);
            $table->foreign('tour3d_id')->references('id')->on('guide__tour3ds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guide__tour3d_translations', function (Blueprint $table) {
            $table->dropForeign(['tour3d_id']);
        });
        Schema::dropIfExists('guide__tour3d_translations');
    }
}
