<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuideAudioGuideTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guide__audioguide_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name', 100);
            $table->text('description')->nullable();
            $table->integer('audioguide_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['audioguide_id', 'locale']);
            $table->foreign('audioguide_id')->references('id')->on('guide__audioguides')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guide__audioguide_translations', function (Blueprint $table) {
            $table->dropForeign(['audioguide_id']);
        });
        Schema::dropIfExists('guide__audioguide_translations');
    }
}
