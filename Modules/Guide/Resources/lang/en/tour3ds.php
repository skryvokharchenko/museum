<?php

return [
    'list resource' => 'List tour3ds',
    'create resource' => 'Create tour3ds',
    'edit resource' => 'Edit tour3ds',
    'destroy resource' => 'Destroy tour3ds',
    'title' => [
        'tour3ds' => 'Tour3d',
        'create tour3d' => 'Create a tour3d',
        'edit tour3d' => 'Edit a tour3d',
    ],
    'button' => [
        'create tour3d' => 'Create a tour3d',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
