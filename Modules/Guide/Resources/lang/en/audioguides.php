<?php

return [
    'list resource' => 'List audioguides',
    'create resource' => 'Create audioguides',
    'edit resource' => 'Edit audioguides',
    'destroy resource' => 'Destroy audioguides',
    'title' => [
        'audioguides' => 'AudioGuide',
        'create audioguide' => 'Create a audioguide',
        'edit audioguide' => 'Edit a audioguide',
    ],
    'button' => [
        'create audioguide' => 'Create a audioguide',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'description' => 'Description',
        'audio' => 'Audio'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
