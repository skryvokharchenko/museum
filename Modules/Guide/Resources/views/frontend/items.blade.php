<div class="col-12 col-lg-5">
    <div class="gaide_item_container">
        @foreach($audios as $item)
            <div class="gaide_item font_color">
                <div class="gaide_btn">
                    <audio src="{!! $item->audio_link !!}" controls></audio>
                    {{--<a href="#" title="включити аудіо" class="font_color" role="button" aria-label="включити аудіо">--}}
                        {{--<span class="icon-foto"></span>--}}
                    {{--</a>--}}
                </div>
                <div class="gaide_title font_color">
                    <h2>{{ $item->name }}</h2>
                </div>
                <div class="gaide_text font_color">
                    <p>{{ $item->description }}</p>
                </div>
            </div>
        @endforeach
    </div>

</div>
