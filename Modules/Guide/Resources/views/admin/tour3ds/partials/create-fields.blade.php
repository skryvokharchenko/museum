<div class="form-group {{ $errors->has("field") ? ' has-error' : '' }}">
    {!! Form::label("field", trans('guide::tour3ds.form.field')) !!}
    {!! Form::text("field", old("field"), ["class" => "form-control", "placeholder" => trans('guide::tour3ds.form.field')]) !!}
    {!! $errors->first("field", '<span class="help-block">:message</span>') !!}
</div>
