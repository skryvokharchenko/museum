<div class="form-group {{ $errors->has("{$lang}[field]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[field]", trans('guide::tour3ds.form.field')) !!}
    {!! Form::text("{$lang}[field]", old("{$lang}[field]"), ["class" => "form-control", "placeholder" => trans('guide::tour3ds.form.field')]) !!}
    {!! $errors->first("{$lang}[field]", '<span class="help-block">:message</span>') !!}
</div>
