<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('guide::audioguides.form.name')) !!}
    <?php $old = $audioguide->hasTranslation($lang) ? $audioguide->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('guide::audioguides.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('guide::audioguides.form.description')) !!}
    <?php $old = $audioguide->hasTranslation($lang) ? $audioguide->translate($lang)->description : '' ?>
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]", $old), ["class" => "form-control", "placeholder" => trans('guide::audioguides.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('audioGuide-' . $lang, $audioguide, null, trans('guide::audioguides.form.audio'))
    </div>
</div>