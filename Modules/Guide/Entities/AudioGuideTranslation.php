<?php

namespace Modules\Guide\Entities;

use Illuminate\Database\Eloquent\Model;

class AudioGuideTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'description'];
    protected $table = 'guide__audioguide_translations';
}
