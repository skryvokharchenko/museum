<?php

namespace Modules\Guide\Entities;

use Illuminate\Database\Eloquent\Model;

class Tour3dTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'guide__tour3d_translations';
}
