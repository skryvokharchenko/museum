<?php

namespace Modules\Guide\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Tour3d extends Model
{
    use Translatable;

    protected $table = 'guide__tour3ds';
    public $translatedAttributes = [];
    protected $fillable = [];
}
