<?php

namespace Modules\Guide\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class AudioGuide extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'guide__audioguides';
    protected $translationForeignKey = 'audioguide_id';
    public $translatedAttributes = ['name', 'description'];
    protected $fillable = ['name', 'description'];

    public function getAudioLinkAttribute()
    {
        $file = $this->files()->where('zone', 'audioGuide-' . app()->getLocale())->first();
        if ($file === null) {
            return '';
        }
        return $file->path;
    }
}
