<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 28.10.2019
 * Time: 08:05
 */

namespace Modules\Guide\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Guide\Presenters\AudioGuidePresenter;

class AudioGuideFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return AudioGuidePresenter::class;
    }
}