<?php


namespace Modules\Publications\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Publications\Presenters\PublicationsPresenter;

class PublicationsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return PublicationsPresenter::class;
    }
}
