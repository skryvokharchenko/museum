<?php

namespace Modules\Publications\Repositories\Eloquent;

use App\Repositories\Traits\Mediable;
use Modules\Publications\Repositories\PublicationRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPublicationRepository extends EloquentBaseRepository implements PublicationRepository
{
    use Mediable;
}
