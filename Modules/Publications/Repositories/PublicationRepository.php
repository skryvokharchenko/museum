<?php

namespace Modules\Publications\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface PublicationRepository extends BaseRepository
{
}
