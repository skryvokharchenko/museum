<?php

namespace Modules\Publications\Repositories\Cache;

use Modules\Publications\Repositories\PublicationRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePublicationDecorator extends BaseCacheDecorator implements PublicationRepository
{
    public function __construct(PublicationRepository $publication)
    {
        parent::__construct();
        $this->entityName = 'publications.publications';
        $this->repository = $publication;
    }
}
