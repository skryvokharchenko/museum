<?php

namespace Modules\Publications\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Publications\Events\Handlers\RegisterPublicationsSidebar;

class PublicationsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterPublicationsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('publications', array_dot(trans('publications::publications')));
            // append translations

        });
    }

    public function boot()
    {
        $this->publishConfig('publications', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Publications\Repositories\PublicationRepository',
            function () {
                $repository = new \Modules\Publications\Repositories\Eloquent\EloquentPublicationRepository(new \Modules\Publications\Entities\Publication());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Publications\Repositories\Cache\CachePublicationDecorator($repository);
            }
        );
// add bindings

    }
}
