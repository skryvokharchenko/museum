<?php

return [
    'publications.publications' => [
        'index' => 'publications::publications.list resource',
        'create' => 'publications::publications.create resource',
        'edit' => 'publications::publications.edit resource',
        'destroy' => 'publications::publications.destroy resource',
    ],
// append

];
