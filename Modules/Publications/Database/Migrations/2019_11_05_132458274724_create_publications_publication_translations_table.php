<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsPublicationTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications__publication_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('title', 40);
            $table->string('short_description', 120)->nullable();
            $table->text('description')->nullable();
            $table->integer('publication_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['publication_id', 'locale'], 'publication_locale_unique');
            $table->foreign('publication_id')->references('id')->on('publications__publications')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publications__publication_translations', function (Blueprint $table) {
            $table->dropForeign(['publication_id']);
        });
        Schema::dropIfExists('publications__publication_translations');
    }
}
