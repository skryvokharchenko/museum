<?php

namespace Modules\Publications\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Page\Repositories\PageRepository;
use Modules\Publications\Entities\Publication;
use Modules\Publications\Http\Requests\CreatePublicationRequest;
use Modules\Publications\Http\Requests\UpdatePublicationRequest;
use Modules\Publications\Repositories\PublicationRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PublicationController extends AdminBaseController
{
    /**
     * @var PublicationRepository
     */
    private $publication;
    /**
     * @var PageRepository
     */
    private $page;

    public function __construct(PublicationRepository $publication, PageRepository $page)
    {
        parent::__construct();

        $this->publication = $publication;
        $this->page = $page;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $publications = $this->publication->all();
        $publications->load('page');

        return view('publications::admin.publications.index', compact('publications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $pages = $this->page->all()->pluck('title', 'id');
        return view('publications::admin.publications.create', compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePublicationRequest $request
     * @return Response
     */
    public function store(CreatePublicationRequest $request)
    {
        $this->publication->create($request->all());

        return redirect()->route('admin.publications.publication.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('publications::publications.title.publications')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Publication $publication
     * @return Response
     */
    public function edit(Publication $publication)
    {
        $pages = $this->page->all()->pluck('title', 'id');
        return view('publications::admin.publications.edit', compact('publication', 'pages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Publication $publication
     * @param  UpdatePublicationRequest $request
     * @return Response
     */
    public function update(Publication $publication, UpdatePublicationRequest $request)
    {
        $this->publication->update($publication, $request->all());

        return redirect()->route('admin.publications.publication.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('publications::publications.title.publications')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Publication $publication
     * @return Response
     */
    public function destroy(Publication $publication)
    {
        $this->publication->destroy($publication);

        return redirect()->route('admin.publications.publication.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('publications::publications.title.publications')]));
    }
}
