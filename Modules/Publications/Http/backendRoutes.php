<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/publications'], function (Router $router) {
    $router->bind('publication', function ($id) {
        return app('Modules\Publications\Repositories\PublicationRepository')->find($id);
    });
    $router->get('publications', [
        'as' => 'admin.publications.publication.index',
        'uses' => 'PublicationController@index',
        'middleware' => 'can:publications.publications.index'
    ]);
    $router->get('publications/create', [
        'as' => 'admin.publications.publication.create',
        'uses' => 'PublicationController@create',
        'middleware' => 'can:publications.publications.create'
    ]);
    $router->post('publications', [
        'as' => 'admin.publications.publication.store',
        'uses' => 'PublicationController@store',
        'middleware' => 'can:publications.publications.create'
    ]);
    $router->get('publications/{publication}/edit', [
        'as' => 'admin.publications.publication.edit',
        'uses' => 'PublicationController@edit',
        'middleware' => 'can:publications.publications.edit'
    ]);
    $router->put('publications/{publication}', [
        'as' => 'admin.publications.publication.update',
        'uses' => 'PublicationController@update',
        'middleware' => 'can:publications.publications.edit'
    ]);
    $router->delete('publications/{publication}', [
        'as' => 'admin.publications.publication.destroy',
        'uses' => 'PublicationController@destroy',
        'middleware' => 'can:publications.publications.destroy'
    ]);
// append

});
