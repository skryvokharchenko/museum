<div class="form-group {{ $errors->has("{$lang}[title]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[title]", trans('publications::publications.form.title')) !!}
    <?php $old = $publication->hasTranslation($lang) ? $publication->translate($lang)->title : '' ?>
    {!! Form::text("{$lang}[title]", old("{$lang}[title]", $old), ["class" => "form-control", "placeholder" => trans('publications::publications.form.title')]) !!}
    {!! $errors->first("{$lang}[title]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[short_description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[short_description]", trans('publications::publications.form.short_description')) !!}
    <?php $old = $publication->hasTranslation($lang) ? $publication->translate($lang)->short_description : '' ?>
    {!! Form::textarea("{$lang}[short_description]", old("{$lang}[short_description]", $old), ["class" => "form-control", "placeholder" => trans('publications::publications.form.short_description')]) !!}
    {!! $errors->first("{$lang}[short_description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('publications::publications.form.description')) !!}
    <?php $old = $publication->hasTranslation($lang) ? $publication->translate($lang)->description : '' ?>
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]", $old), ["class" => "form-control", "placeholder" => trans('publications::publications.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('publicationPdf-' . $lang, $publication, null, trans('publications::publications.form.pdf'))
    </div>
</div>
