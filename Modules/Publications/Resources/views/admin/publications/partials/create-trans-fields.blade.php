<div class="form-group {{ $errors->has("{$lang}[title]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[title]", trans('publications::publications.form.title')) !!}
    {!! Form::text("{$lang}[title]", old("{$lang}[title]"), ["class" => "form-control", "placeholder" => trans('publications::publications.form.title')]) !!}
    {!! $errors->first("{$lang}[title]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[short_description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[short_description]", trans('publications::publications.form.short_description')) !!}
    {!! Form::textarea("{$lang}[short_description]", old("{$lang}[short_description]"), ["class" => "form-control", "placeholder" => trans('publications::publications.form.short_description')]) !!}
    {!! $errors->first("{$lang}[short_description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('publications::publications.form.description')) !!}
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]"), ["class" => "form-control", "placeholder" => trans('publications::publications.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('publicationPdf-' . $lang, null, null, trans('publications::publications.form.pdf'))
    </div>
</div>
