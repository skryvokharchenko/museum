<div class="form-group {{ $errors->has("page_id") ? ' has-error' : '' }}">
    {!! Form::label("page_id", trans('publications::publications.form.page_id')) !!}
    {!! Form::select("page_id", $pages, null, ["class" => "form-control"]) !!}
    {!! $errors->first("page_id", '<span class="help-block">:message</span>') !!}
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('publicationImage', null, null, trans('publications::publications.form.image'))
    </div>
</div>
