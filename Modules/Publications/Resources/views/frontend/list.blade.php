<div class="content_container">
    @foreach($publications as $publication)
        <div class="store_item {{ $class }} collection_it_about font_color bg_color">
            <div class="item_foto">
                <img src="{!! $publication->image_url !!}" alt="{{ $publication->alt_text }}">
            </div>
            <div class="store_info color2 font_color">
                <div class="store_info_name about_collection_info_name type2">
                    <h2>{{ $publication->title }}</h2>
                    <p>{{ $publication->short_description }}</p>

                    @if($publication->description)
                    <a href="#worK_text_link" role="button" data-more class="worK_text_link type2 font_color" aria-label="читати більше">{{ trans('front.read more') }}..</a>
                    @endif
                    @if($publication->pdf_url)
                        <div class="collect_btn_container">
                            <a target="_blank" href="{!! $publication->pdf_url !!}" class="download_btn font_color bg_color" role="button" aria-label="PDF" title="PDF">PDF</a>
                        </div>
                    @endif
                </div>
            </div>

            @if($publication->description)
            <div class="item_sub_text text_more color2 font_color">
                <p>{{ $publication->description }}</p>

                <a href="#worK_text_link" role="button" data-hide class="worK_text_link type2 font_color" aria-label="{{ trans('front.hide') }}">{{ trans('front.hide') }}</a>

            </div>
            @endif
        </div>
    @endforeach
</div>
