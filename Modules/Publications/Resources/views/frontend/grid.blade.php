<div class="row">
    <div class="col-12 col-md-6">
        @foreach($items[0] as $item)
            <div class="volunteers_item bg_color">
                <div class="volunteers_foto">
                    <img src="{!! $item->image_url !!}" alt="{{ $item->alt_text }}">
                </div>
                <div class="volunteers_info">
                    <h2>{{ $item->short_description }}</h2>
                </div>
            </div>
        @endforeach
    </div>
    <div class="col-12 col-md-6">
        @foreach($items[1] as $item)
            <div class="volunteers_item bg_color">
                <div class="volunteers_foto">
                    <img src="{!! $item->image_url !!}" alt="{{ $item->alt_text }}">
                </div>
                <div class="volunteers_info">
                    <h2>{{ $item->short_description }}</h2>
                </div>
            </div>
        @endforeach
    </div>
</div>
