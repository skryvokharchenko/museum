<?php

return [
    'list resource' => 'List publications',
    'create resource' => 'Create publications',
    'edit resource' => 'Edit publications',
    'destroy resource' => 'Destroy publications',
    'title' => [
        'publications' => 'Publication',
        'create publication' => 'Create a publication',
        'edit publication' => 'Edit a publication',
    ],
    'button' => [
        'create publication' => 'Create a publication',
    ],
    'table' => [
    ],
    'form' => [
        'title' => 'Title',
        'page_id' => 'Page',
        'short_description' => 'Short Description',
        'description' => 'Full Description',
        'pdf' => 'PDF File',
        'image' => 'Image'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
