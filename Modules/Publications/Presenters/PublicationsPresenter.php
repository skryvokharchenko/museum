<?php


namespace Modules\Publications\Presenters;


use Illuminate\Support\Facades\View;
use Modules\Page\Repositories\PageRepository;
use Modules\Publications\Repositories\PublicationRepository;

class PublicationsPresenter
{
    /**
     * @var PublicationRepository
     */
    private $publication;

    public function __construct(PublicationRepository $publication)
    {

        $this->publication = $publication;
    }

    public function list($page_id, $class = 'type4')
    {
        $template = 'publications::frontend.list';
        $publications = $this->publication->getByAttributes(['page_id' => $page_id]);

        $view = View::make($template)->with(compact('publications', 'class'));
        return $view->render();
    }

    public function grid($page_id)
    {
        $template = 'publications::frontend.grid';
        $publications = $this->publication->getByAttributes(['page_id' => $page_id]);

        $results = [];
        foreach ($publications as $i => $item) {
            $results[$i % 2][] = $item;
        }
        $items = array_pad($results, 2, []);

        $view = View::make($template)->with(compact('items'));
        return $view->render();
    }
}
