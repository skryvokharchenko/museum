<?php

namespace Modules\Publications\Entities;

use Illuminate\Database\Eloquent\Model;

class PublicationTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'description', 'short_description'];
    protected $table = 'publications__publication_translations';
}
