<?php

namespace Modules\Publications\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Page\Entities\Page;

class Publication extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'publications__publications';
    public $translatedAttributes = ['title', 'description', 'short_description'];
    protected $fillable = ['title', 'description', 'short_description', 'page_id'];

    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function images()
    {
        return $this->files()->where('zone', 'publicationImage');
    }

    public function pdf()
    {
        return $this->files()->where('zone', 'publicationPdf-' . app()->getLocale());
    }

    public function getImageUrlAttribute()
    {
        return $this->images()->first() ? $this->images()->first()->path : '';
    }

    public function getAltTextAttribute()
    {
        return $this->images()->first() ? $this->images()->first()->alt_attribute : '';
    }

    public function getPdfUrlAttribute()
    {
        return $this->pdf()->first() ? $this->pdf()->first()->path : '';
    }


}
