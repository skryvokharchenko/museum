<?php

return [
    'events.events' => [
        'index' => 'events::events.list resource',
        'create' => 'events::events.create resource',
        'edit' => 'events::events.edit resource',
        'destroy' => 'events::events.destroy resource',
    ],
    'events.expos' => [
        'index' => 'events::expos.list resource',
        'create' => 'events::expos.create resource',
        'edit' => 'events::expos.edit resource',
        'destroy' => 'events::expos.destroy resource',
    ],
// append


];
