<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsExpoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events__expo_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->text('description')->nullable();
            $table->text('short_description')->nullable();
            $table->integer('expo_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['expo_id', 'locale']);
            $table->foreign('expo_id')->references('id')->on('events__expos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events__expo_translations', function (Blueprint $table) {
            $table->dropForeign(['expo_id']);
        });
        Schema::dropIfExists('events__expo_translations');
    }
}
