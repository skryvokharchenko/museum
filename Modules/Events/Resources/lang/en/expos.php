<?php

return [
    'list resource' => 'List expos',
    'create resource' => 'Create expos',
    'edit resource' => 'Edit expos',
    'destroy resource' => 'Destroy expos',
    'title' => [
        'expos' => 'Expo',
        'create expo' => 'Create a expo',
        'edit expo' => 'Edit a expo',
    ],
    'button' => [
        'create expo' => 'Create a expo',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'description' => 'Description',
        'short_description' => 'Short Description',
        'alt_text' => 'Image Alt Text',
        'start_date' => 'Start Date',
        'end_date' => 'End Date',
        'archived' => 'Archived',
        'image' => 'Photo'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
