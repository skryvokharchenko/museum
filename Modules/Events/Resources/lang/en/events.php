<?php

return [
    'list resource' => 'List events',
    'create resource' => 'Create events',
    'edit resource' => 'Edit events',
    'destroy resource' => 'Destroy events',
    'title' => [
        'events' => 'Event',
        'create event' => 'Create a event',
        'edit event' => 'Edit a event',
    ],
    'button' => [
        'create event' => 'Create a event',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'description' => 'Description',
        'short_description' => 'Short Description',
        'alt_text' => 'Image Alt Text',
        'date' => 'Date',
        'archived' => 'Archived',
        'image' => 'Photo'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
