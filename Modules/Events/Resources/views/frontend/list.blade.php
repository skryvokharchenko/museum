<section>
    <div class="action_section bg_color">
        <div class="content_container">
            @foreach($items as $item)
                <div class="store_item font_color bg_color flex-wrap">
                    <div class="item_foto">
                        <img src="{{ $item->image_url }}" alt="{{ $item->alt_text }}" title="{{ $item->alt_text }}">
                    </div>
                    <div class="store_info">
                        <div class="store_info_name action_info_name">
                            <h2 aria-label="назва книги">{{ $item->name }}</h2>
                            <p aria-label="інформація книги">{{ $item->short_description }}</p>
                            @if($item->description)
                                <a href="#" class="worK_text_link font_color" data-more role="button" aria-label="{{trans('front.read more')}}">{{ trans('front.read more') }}..</a>
                            @endif
                        </div>

                    </div>

                    @if($item->description)
                    <div class="item_sub_text text_more font_color">
                        <p>{{ $item->description }}</p>
                        <a href="#worK_text_link" role="button" data-hide class="worK_text_link font_color" aria-label="{{ trans('front.hide') }}">{{ trans('front.hide') }}</a>
                    </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</section>
