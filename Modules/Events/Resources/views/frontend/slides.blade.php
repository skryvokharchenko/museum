<section>
    <div class="exhi_slider font_color bg_color">
        <div class="slider_container" id="slider" role="region" aria-label="слайдер">
            @foreach($items as $index => $item)
                <div class="slider_item" aria-live="polite" aria-label="{{$index + 1}} слайд із {{count($items)}}">
                    <div class="slider_foto">
                        <img src="{{ $item->image_url }}" role="tabpanel" alt="{{ $item->alt_text }}">
                    </div>
                    <article>
                        <div class="slider_info exhi_info font_color bg_color">
                            <a href="#">
                                <h2>{{ $item->name }}</h2>
                            </a>
                        </div>
                    </article>
                </div>
            @endforeach
        </div>

        <div class="slider_btn_container slider_btn_exhi">
            <div class="btn_pause bg_color font_color active" tabindex="-1" title="призупинити слайдер" aria-label="зупинити слайдер" role="button" id="btn_pause">
                <span class="icon-iconfinder_icon-pause_211871"></span>
            </div>
            <div class="btn_play bg_color font_color active" tabindex="-1" title="увімкнути слайдер" aria-label="програти слайдер" role="button" id="btn_play">
                <span class="icon-iconfinder_icon-play_211876"></span>
            </div>
        </div>
    </div>
</section>
