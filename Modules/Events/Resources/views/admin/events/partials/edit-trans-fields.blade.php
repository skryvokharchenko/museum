<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('events::events.form.name')) !!}
    <?php $old = $event->hasTranslation($lang) ? $event->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('events::events.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[short_description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[short_description]", trans('events::events.form.short_description')) !!}
    <?php $old = $event->hasTranslation($lang) ? $event->translate($lang)->short_description : '' ?>
    {!! Form::textarea("{$lang}[short_description]", old("{$lang}[short_description]", $old), ["class" => "form-control", "placeholder" => trans('events::events.form.short_description')]) !!}
    {!! $errors->first("{$lang}[short_description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('events::events.form.description')) !!}
    <?php $old = $event->hasTranslation($lang) ? $event->translate($lang)->description : '' ?>
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]", $old), ["class" => "form-control", "placeholder" => trans('events::events.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>
