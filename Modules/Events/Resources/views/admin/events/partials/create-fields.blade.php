<div class="form-group {{ $errors->has("date") ? ' has-error' : '' }}">
    {!! Form::label("date", trans('events::events.form.date')) !!}
    {!! Form::text("date", old('date'), ["class" => "form-control", "placeholder" => trans('events::events.form.date')]) !!}
    {!! $errors->first("date", '<span class="help-block">:message</span>') !!}
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('eventImage', null, null, trans('events::events.form.image'))
    </div>
</div>

<div class="form-group {{ $errors->has("archived") ? ' has-error' : '' }}">
    {!! Form::checkbox("archived", 1, false, ["class" => "flat-blue", "placeholder" => trans('events::events.form.archived')]) !!}
    {!! Form::label("archived", trans('events::events.form.archived')) !!}
    {!! $errors->first("archived", '<span class="help-block">:message</span>') !!}
</div>

@push('css-stack')
    {!! Theme::style('css/vendor/jQueryUI/jquery-ui-1.10.3.custom.min.css') !!}
@endpush

@push('js-stack')
    {!! Theme::script('vendor/jquery-ui/ui/minified/datepicker.min.js') !!}
{{--    {!! Theme::script('js/vendor/timepicker/bootstrap-timepicker.min.js') !!}--}}
    <script>
        $('input#date').datepicker();
    </script>
@endpush
