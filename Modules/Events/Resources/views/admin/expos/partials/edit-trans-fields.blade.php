<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('events::expos.form.name')) !!}
    <?php $old = $expo->hasTranslation($lang) ? $expo->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('events::expos.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[short_description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[short_description]", trans('events::expos.form.short_description')) !!}
    <?php $old = $expo->hasTranslation($lang) ? $expo->translate($lang)->short_description : '' ?>
    {!! Form::textarea("{$lang}[short_description]", old("{$lang}[short_description]", $old), ["class" => "form-control", "placeholder" => trans('events::expos.form.short_description')]) !!}
    {!! $errors->first("{$lang}[short_description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('events::expos.form.description')) !!}
    <?php $old = $expo->hasTranslation($lang) ? $expo->translate($lang)->description : '' ?>
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]", $old), ["class" => "form-control", "placeholder" => trans('events::expos.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>
