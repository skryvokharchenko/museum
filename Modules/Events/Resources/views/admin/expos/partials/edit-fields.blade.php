<div class="form-group {{ $errors->has("start_date") ? ' has-error' : '' }}">
    {!! Form::label("start_date", trans('events::expos.form.start_date')) !!}
    {!! Form::text("start_date", old('start_date', $expo->start_date->format('m/d/Y')), ["class" => "form-control", "placeholder" => trans('events::expos.form.start_date')]) !!}
    {!! $errors->first("start_date", '<span class="help-block">:message</span>') !!}
</div>
                                                                                                                       
<div class="form-group {{ $errors->has("end_date") ? ' has-error' : '' }}">
    {!! Form::label("end_date", trans('events::expos.form.end_date')) !!}
    {!! Form::text("end_date", old('end_date', $expo->end_date->format('m/d/Y')), ["class" => "form-control", "placeholder" => trans('events::expos.form.end_date')]) !!}
    {!! $errors->first("end_date", '<span class="help-block">:message</span>') !!}
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('expoImage', $expo, null, trans('events::expos.form.image'))
    </div>
</div>

<div class="form-group {{ $errors->has("archived") ? ' has-error' : '' }}">
    <input type="hidden" name="archived" value="0">
    {!! Form::checkbox("archived", 1, $expo->archived, ["class" => "flat-blue", "placeholder" => trans('events::expos.form.archived')]) !!}
    {!! Form::label("archived", trans('events::expos.form.archived')) !!}
    {!! $errors->first("archived", '<span class="help-block">:message</span>') !!}
</div>

@push('css-stack')
    {!! Theme::style('css/vendor/jQueryUI/jquery-ui-1.10.3.custom.min.css') !!}
@endpush

@push('js-stack')
    {!! Theme::script('vendor/jquery-ui/ui/minified/datepicker.min.js') !!}
    {{--    {!! Theme::script('js/vendor/timepicker/bootstrap-timepicker.min.js') !!}--}}
    <script>
        $('input#start_date, input#end_date').datepicker();
    </script>
@endpush
