<?php

namespace Modules\Events\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface EventRepository extends BaseRepository
{
    public function allActual();

    public function allArchived();
}
