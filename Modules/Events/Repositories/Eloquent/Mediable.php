<?php


namespace Modules\Events\Repositories\Eloquent;


use Modules\Events\Events\EntityWasCreatedOrUpdated;

trait Mediable
{
    public function create($data)
    {
        $item = parent::create($data);
        event(new EntityWasCreatedOrUpdated($item, $data));
        return $item;
    }

    public function update($model, $data)
    {
        $item = parent::update($model, $data);
        event(new EntityWasCreatedOrUpdated($item, $data));
        return $item;
    }
}
