<?php

namespace Modules\Events\Repositories\Eloquent;

use Illuminate\Support\Carbon;
use Modules\Events\Repositories\ExpoRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentExpoRepository extends EloquentBaseRepository implements ExpoRepository
{
    use Mediable;

    public function allActual()
    {
        return $this->allWithBuilder()->actual()->get();
    }

    public function allArchived()
    {
        return $this->allWithBuilder()->archived()->get();
    }

    public function allFuture()
    {
        return $this->allWithBuilder()->future()->get();
    }
}
