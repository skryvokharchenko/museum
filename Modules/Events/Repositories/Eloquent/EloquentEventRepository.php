<?php

namespace Modules\Events\Repositories\Eloquent;

use Illuminate\Support\Carbon;
use Modules\Events\Repositories\EventRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentEventRepository extends EloquentBaseRepository implements EventRepository
{
    use Mediable;

    public function allActual()
    {
        return $this->allWithBuilder()->actual()->get();
    }

    public function allArchived()
    {
        return $this->allWithBuilder()->archived()->get();
    }
}
