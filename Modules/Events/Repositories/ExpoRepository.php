<?php

namespace Modules\Events\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface ExpoRepository extends BaseRepository
{
    public function allActual();

    public function allArchived();

    public function allFuture();
}
