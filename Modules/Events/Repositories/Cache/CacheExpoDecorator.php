<?php

namespace Modules\Events\Repositories\Cache;

use Modules\Events\Repositories\ExpoRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheExpoDecorator extends BaseCacheDecorator implements ExpoRepository
{
    public function __construct(ExpoRepository $expo)
    {
        parent::__construct();
        $this->entityName = 'events.expos';
        $this->repository = $expo;
    }
}
