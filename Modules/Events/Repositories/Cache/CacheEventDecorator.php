<?php

namespace Modules\Events\Repositories\Cache;

use Modules\Events\Repositories\EventRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheEventDecorator extends BaseCacheDecorator implements EventRepository
{
    public function __construct(EventRepository $event)
    {
        parent::__construct();
        $this->entityName = 'events.events';
        $this->repository = $event;
    }
}
