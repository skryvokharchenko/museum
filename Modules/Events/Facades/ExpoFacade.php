<?php


namespace Modules\Events\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Events\Presenters\ExpoPresenter;

class ExpoFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ExpoPresenter::class;
    }
}
