<?php


namespace Modules\Events\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Events\Presenters\EventPresenter;

class EventFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return EventPresenter::class;
    }
}
