<?php

namespace Modules\Events\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Modules\Media\Support\Traits\MediaRelation;

class Expo extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'events__expos';
    public $translatedAttributes = [
        'name',
        'description',
        'short_description',
    ];
    protected $fillable = [
        'start_date',
        'end_date',
        'name',
        'description',
        'short_description',
        'archived'
    ];

    protected $casts = [
        'archived' => 'boolean',
        'start_date' => 'date',
        'end_date' => 'date'
    ];

    public function scopeActual(Builder $query)
    {
        return $query->whereDate('start_date', '<=', Carbon::now())
            ->whereDate('end_date', '>=', Carbon::now())
            ->where('archived', false)
            ->orderBy('start_date');
    }

    public function scopeFuture(Builder $query)
    {
        return $query->whereDate('start_date', '>', Carbon::now())
            ->orderBy('start_date');
    }

    public function scopeArchived(Builder $query)
    {
        return $query->where('archived', true)
            ->orderByDesc('end_date');
    }

    public function getImageUrlAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->path : '';
    }

    public function getAltTextAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->alt_attribute : '';
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = (new Carbon($value))->toDateString();
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = (new Carbon($value))->toDateString();
    }
}
