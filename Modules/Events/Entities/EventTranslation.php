<?php

namespace Modules\Events\Entities;

use Illuminate\Database\Eloquent\Model;

class EventTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'short_description',
    ];
    protected $table = 'events__event_translations';
}
