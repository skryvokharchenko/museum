<?php

namespace Modules\Events\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Modules\Media\Support\Traits\MediaRelation;

class Event extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'events__events';
    public $translatedAttributes = [
        'name',
        'description',
        'short_description',
    ];
    protected $fillable = [
        'date',
        'name',
        'description',
        'short_description',
        'archived'
    ];
    protected $casts = [
        'archived' => 'boolean',
        'date' => 'date'
    ];


    public function scopeActual(Builder $query)
    {
        return $query->where('archived', false)
            ->orderByDesc('date');
    }


    public function scopeArchived(Builder $query)
    {
        return $query->where('archived', true)
            ->orderByDesc('date');
    }

    public function getImageUrlAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->path : '';
    }

    public function getAltTextAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->alt_attribute : '';
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = (new Carbon($value))->toDateString();
    }
}
