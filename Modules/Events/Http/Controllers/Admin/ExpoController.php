<?php

namespace Modules\Events\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Events\Entities\Expo;
use Modules\Events\Http\Requests\CreateExpoRequest;
use Modules\Events\Http\Requests\UpdateExpoRequest;
use Modules\Events\Repositories\ExpoRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ExpoController extends AdminBaseController
{
    /**
     * @var ExpoRepository
     */
    private $expo;

    public function __construct(ExpoRepository $expo)
    {
        parent::__construct();

        $this->expo = $expo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $expos = $this->expo->all();

        return view('events::admin.expos.index', compact('expos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('events::admin.expos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateExpoRequest $request
     * @return Response
     */
    public function store(CreateExpoRequest $request)
    {
        $this->expo->create($request->all());

        return redirect()->route('admin.events.expo.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('events::expos.title.expos')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Expo $expo
     * @return Response
     */
    public function edit(Expo $expo)
    {
        return view('events::admin.expos.edit', compact('expo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Expo $expo
     * @param  UpdateExpoRequest $request
     * @return Response
     */
    public function update(Expo $expo, UpdateExpoRequest $request)
    {
        $this->expo->update($expo, $request->all());

        return redirect()->route('admin.events.expo.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('events::expos.title.expos')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Expo $expo
     * @return Response
     */
    public function destroy(Expo $expo)
    {
        $this->expo->destroy($expo);

        return redirect()->route('admin.events.expo.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('events::expos.title.expos')]));
    }
}
