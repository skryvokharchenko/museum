<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/events'], function (Router $router) {
    $router->bind('event', function ($id) {
        return app('Modules\Events\Repositories\EventRepository')->find($id);
    });
    $router->get('events', [
        'as' => 'admin.events.event.index',
        'uses' => 'EventController@index',
        'middleware' => 'can:events.events.index'
    ]);
    $router->get('events/create', [
        'as' => 'admin.events.event.create',
        'uses' => 'EventController@create',
        'middleware' => 'can:events.events.create'
    ]);
    $router->post('events', [
        'as' => 'admin.events.event.store',
        'uses' => 'EventController@store',
        'middleware' => 'can:events.events.create'
    ]);
    $router->get('events/{event}/edit', [
        'as' => 'admin.events.event.edit',
        'uses' => 'EventController@edit',
        'middleware' => 'can:events.events.edit'
    ]);
    $router->put('events/{event}', [
        'as' => 'admin.events.event.update',
        'uses' => 'EventController@update',
        'middleware' => 'can:events.events.edit'
    ]);
    $router->delete('events/{event}', [
        'as' => 'admin.events.event.destroy',
        'uses' => 'EventController@destroy',
        'middleware' => 'can:events.events.destroy'
    ]);
    $router->bind('expo', function ($id) {
        return app('Modules\Events\Repositories\ExpoRepository')->find($id);
    });
    $router->get('expos', [
        'as' => 'admin.events.expo.index',
        'uses' => 'ExpoController@index',
        'middleware' => 'can:events.expos.index'
    ]);
    $router->get('expos/create', [
        'as' => 'admin.events.expo.create',
        'uses' => 'ExpoController@create',
        'middleware' => 'can:events.expos.create'
    ]);
    $router->post('expos', [
        'as' => 'admin.events.expo.store',
        'uses' => 'ExpoController@store',
        'middleware' => 'can:events.expos.create'
    ]);
    $router->get('expos/{expo}/edit', [
        'as' => 'admin.events.expo.edit',
        'uses' => 'ExpoController@edit',
        'middleware' => 'can:events.expos.edit'
    ]);
    $router->put('expos/{expo}', [
        'as' => 'admin.events.expo.update',
        'uses' => 'ExpoController@update',
        'middleware' => 'can:events.expos.edit'
    ]);
    $router->delete('expos/{expo}', [
        'as' => 'admin.events.expo.destroy',
        'uses' => 'ExpoController@destroy',
        'middleware' => 'can:events.expos.destroy'
    ]);
// append


});
