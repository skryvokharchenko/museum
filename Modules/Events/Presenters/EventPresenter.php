<?php


namespace Modules\Events\Presenters;


use Illuminate\Support\Facades\View;
use Modules\Events\Repositories\EventRepository;
use Modules\Events\Repositories\ExpoRepository;

class EventPresenter implements EventPresenterInterface
{
    /**
     * @var ExpoRepository
     */
    private $event;

    public function __construct(EventRepository $event)
    {

        $this->event = $event;
    }

    public function actual()
    {
        $template = 'events::frontend.list';
        $items = $this->event->allActual();

        $view = View::make($template, compact('items'));
        return $view->render();
    }

    public function archive()
    {
        $template = 'events::frontend.list';
        $items = $this->event->allArchived();

        $view = View::make($template, compact('items'));
        return $view->render();
    }
}
