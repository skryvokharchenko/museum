<?php


namespace Modules\Events\Presenters;


interface EventPresenterInterface
{
    public function actual();

    public function archive();
}
