<?php


namespace Modules\Events\Presenters;


interface ExpoPresenterInterface
{
    public function actual();

    public function archive();

    public function future();
}
