<?php


namespace Modules\Events\Presenters;


use Illuminate\Support\Facades\View;
use Modules\Events\Repositories\ExpoRepository;

class ExpoPresenter implements ExpoPresenterInterface
{
    /**
     * @var ExpoRepository
     */
    private $expo;

    public function __construct(ExpoRepository $expo)
    {

        $this->expo = $expo;
    }

    public function actual()
    {
        $template = 'events::frontend.slides';
        $items = $this->expo->allActual();

        $view = View::make($template, compact('items'));
        return $view->render();
    }

    public function archive()
    {
        $template = 'events::frontend.list';
        $items = $this->expo->allArchived();

        $view = View::make($template, compact('items'));
        return $view->render();
    }

    public function future()
    {
        $template = 'events::frontend.slides';
        $items = $this->expo->allFuture();

        $view = View::make($template, compact('items'));
        return $view->render();
    }
}
