<?php

namespace Modules\Events\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Events\Events\Handlers\RegisterEventsSidebar;

class EventsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterEventsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('events', array_dot(trans('events::events')));
            $event->load('expos', array_dot(trans('events::expos')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('events', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Events\Repositories\EventRepository',
            function () {
                $repository = new \Modules\Events\Repositories\Eloquent\EloquentEventRepository(new \Modules\Events\Entities\Event());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Events\Repositories\Cache\CacheEventDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Events\Repositories\ExpoRepository',
            function () {
                $repository = new \Modules\Events\Repositories\Eloquent\EloquentExpoRepository(new \Modules\Events\Entities\Expo());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Events\Repositories\Cache\CacheExpoDecorator($repository);
            }
        );
// add bindings


    }
}
