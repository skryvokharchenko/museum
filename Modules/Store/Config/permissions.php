<?php

return [
    'store.items' => [
        'index' => 'store::items.list resource',
        'create' => 'store::items.create resource',
        'edit' => 'store::items.edit resource',
        'destroy' => 'store::items.destroy resource',
    ],
    'store.types' => [
        'index' => 'store::types.list resource',
        'create' => 'store::types.create resource',
        'edit' => 'store::types.edit resource',
        'destroy' => 'store::types.destroy resource',
    ],
// append


];
