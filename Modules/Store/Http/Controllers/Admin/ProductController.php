<?php

namespace Modules\Store\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Store\Entities\Product;
use Modules\Store\Http\Requests\CreateItemRequest;
use Modules\Store\Http\Requests\UpdateItemRequest;
use Modules\Store\Repositories\ProductRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Store\Repositories\TypeRepository;

class ProductController extends AdminBaseController
{
    /**
     * @var ProductRepository
     */
    private $item;
    /**
     * @var TypeRepository
     */
    private $type;

    public function __construct(ProductRepository $item, TypeRepository $type)
    {
        parent::__construct();

        $this->item = $item;
        $this->type = $type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = $this->item->all();

        return view('store::admin.items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $types = $this->type->all()->pluck('name', 'id')->toArray();
        return view('store::admin.items.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateItemRequest $request
     * @return Response
     */
    public function store(CreateItemRequest $request)
    {
        $this->item->create($request->all());

        return redirect()->route('admin.store.item.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('store::items.title.items')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $item
     * @return Response
     */
    public function edit(Product $item)
    {
        $types = $this->type->all()->pluck('name', 'id')->toArray();
        return view('store::admin.items.edit', compact('item', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Product $item
     * @param  UpdateItemRequest $request
     * @return Response
     */
    public function update(Product $item, UpdateItemRequest $request)
    {
        $this->item->update($item, $request->all());

        return redirect()->route('admin.store.item.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('store::items.title.items')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $item
     * @return Response
     */
    public function destroy(Product $item)
    {
        $this->item->destroy($item);

        return redirect()->route('admin.store.item.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('store::items.title.items')]));
    }
}
