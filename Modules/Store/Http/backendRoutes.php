<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/store'], function (Router $router) {
    $router->bind('product', function ($id) {
        return app('Modules\Store\Repositories\ProductRepository')->find($id);
    });
    $router->get('products', [
        'as' => 'admin.store.item.index',
        'uses' => 'ProductController@index',
        'middleware' => 'can:store.items.index'
    ]);
    $router->get('products/create', [
        'as' => 'admin.store.item.create',
        'uses' => 'ProductController@create',
        'middleware' => 'can:store.items.create'
    ]);
    $router->post('products', [
        'as' => 'admin.store.item.store',
        'uses' => 'ProductController@store',
        'middleware' => 'can:store.items.create'
    ]);
    $router->get('products/{product}/edit', [
        'as' => 'admin.store.item.edit',
        'uses' => 'ProductController@edit',
        'middleware' => 'can:store.items.edit'
    ]);
    $router->put('products/{product}', [
        'as' => 'admin.store.item.update',
        'uses' => 'ProductController@update',
        'middleware' => 'can:store.items.edit'
    ]);
    $router->delete('products/{product}', [
        'as' => 'admin.store.item.destroy',
        'uses' => 'ProductController@destroy',
        'middleware' => 'can:store.items.destroy'
    ]);
    $router->bind('product_type', function ($id) {
        return app('Modules\Store\Repositories\TypeRepository')->find($id);
    });
    $router->get('types', [
        'as' => 'admin.store.type.index',
        'uses' => 'TypeController@index',
        'middleware' => 'can:store.types.index'
    ]);
    $router->get('types/create', [
        'as' => 'admin.store.type.create',
        'uses' => 'TypeController@create',
        'middleware' => 'can:store.types.create'
    ]);
    $router->post('types', [
        'as' => 'admin.store.type.store',
        'uses' => 'TypeController@store',
        'middleware' => 'can:store.types.create'
    ]);
    $router->get('types/{product_type}/edit', [
        'as' => 'admin.store.type.edit',
        'uses' => 'TypeController@edit',
        'middleware' => 'can:store.types.edit'
    ]);
    $router->put('types/{product_type}', [
        'as' => 'admin.store.type.update',
        'uses' => 'TypeController@update',
        'middleware' => 'can:store.types.edit'
    ]);
    $router->delete('types/{product_type}', [
        'as' => 'admin.store.type.destroy',
        'uses' => 'TypeController@destroy',
        'middleware' => 'can:store.types.destroy'
    ]);
// append


});
