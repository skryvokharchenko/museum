<?php

namespace Modules\Store\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class Product extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'store__products';
    public $translatedAttributes = ['name', 'description'];
    protected $fillable = [
        'name',
        'description',
        'price',
        'type_id'
    ];

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function getImageUrlAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->path : '';
    }

    public function getAltTextAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->alt_attribute : '';
    }
}
