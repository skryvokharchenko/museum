<?php

namespace Modules\Store\Entities;

use Illuminate\Database\Eloquent\Model;

class TypeTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $table = 'store__type_translations';
}
