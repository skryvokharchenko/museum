<?php

namespace Modules\Store\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use Translatable;

    protected $table = 'store__types';
    public $translatedAttributes = ['name'];
    protected $fillable = ['name', 'active'];
}
