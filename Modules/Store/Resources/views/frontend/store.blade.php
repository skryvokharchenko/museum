<div class="store_head font_color bg_color">
    <div class="content_container">
        <ul class="nav nav-pills nav_store mb-3" id="pills-tab" role="tablist">
            @foreach($types as $type)
                <li class="tab-item font_color">
                    <a class="tab-link {{ $loop->first ? 'active' : '' }}" id="pills-home-tab" title="{{ $type->name }}"
                       aria-label="{{ $type->name }}" data-toggle="pill" href="#type{{$type->id}}" role="tab" aria-controls="pills-home" aria-selected="true">{{ $type->name }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>

<div class="store_body font_color bg_color">
    <div class="content_container">
        <div class="tab-content" id="pills-tabContent">
            @foreach($types as $type)
            <div class="tab-pane fade {{ $loop->first ? 'show active' : '' }}" id="type{{$type->id}}" title="{{$type->name}}" aria-label="{{$type->name}}" role="tabpanel" aria-labelledby="pills-home-tab">
                @foreach($items->where('type_id', $type->id) as $item)
                    <div class="store_item font_color bg_color">
                        <div class="item_foto">
                            <img src="{!! $item->image_url !!}" alt="{{ $item->alt_text }}" title="фото">
                        </div>
                        <div class="store_info">
                            <div class="store_info_name">
                                <h2 aria-label="назва">{{ $item->name }}</h2>
                                <p aria-label="інформація">{{ $item->description }}</p>
                            </div>

                            <div class="store_price" aria-label="ціна">
                                <h3>{{ $item->price }}</h3>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
</div>