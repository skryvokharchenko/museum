<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('store::types.form.name')) !!}
    <?php $old = $type->hasTranslation($lang) ? $type->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('store::types.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>
