<div class="form-group {{ $errors->has("active") ? ' has-error' : '' }}">
    {!! Form::label("active", trans('store::types.form.active')) !!}
    <input type="hidden" name="active" value="0">
    {!! Form::checkbox("active", 1, old("active", $type->active), ["class" => "flat-blue", "placeholder" => trans('store::types.form.active')]) !!}
    {!! $errors->first("active", '<span class="help-block">:message</span>') !!}
</div>
