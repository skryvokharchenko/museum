<div class="form-group {{ $errors->has("price") ? ' has-error' : '' }}">
    {!! Form::label("price", trans('store::items.form.price')) !!}
    {!! Form::number("price", old("price", $item->price), ["class" => "form-control", "placeholder" => trans('store::items.form.price')]) !!}
    {!! $errors->first("price", '<span class="help-block">:message</span>') !!}
</div>

{!! Form::normalSelect('type_id', trans('store::items.form.type'), $errors, $types, $item, ['class' => '']) !!}

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('storeItemImage', $item, null, trans('store::items.form.photo'))
    </div>
</div>