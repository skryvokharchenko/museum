<?php

namespace Modules\Store\Repositories\Cache;

use Modules\Store\Repositories\ProductRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheProductDecorator extends BaseCacheDecorator implements ProductRepository
{
    public function __construct(ProductRepository $item)
    {
        parent::__construct();
        $this->entityName = 'store.items';
        $this->repository = $item;
    }
}
