<?php

namespace Modules\Store\Repositories\Cache;

use Modules\Store\Repositories\TypeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTypeDecorator extends BaseCacheDecorator implements TypeRepository
{
    public function __construct(TypeRepository $type)
    {
        parent::__construct();
        $this->entityName = 'store.types';
        $this->repository = $type;
    }
}
