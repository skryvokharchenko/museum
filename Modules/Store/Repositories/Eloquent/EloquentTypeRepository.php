<?php

namespace Modules\Store\Repositories\Eloquent;

use Modules\Store\Repositories\TypeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTypeRepository extends EloquentBaseRepository implements TypeRepository
{
}
