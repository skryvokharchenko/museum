<?php

namespace Modules\Store\Repositories\Eloquent;

use App\Repositories\Traits\Mediable;
use Modules\Store\Repositories\ProductRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentProductRepository extends EloquentBaseRepository implements ProductRepository
{
    use Mediable;
}
