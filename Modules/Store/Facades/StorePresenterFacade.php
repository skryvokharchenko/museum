<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 28.10.2019
 * Time: 10:26
 */

namespace Modules\Store\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Store\Presenters\StorePresenter;

class StorePresenterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return StorePresenter::class;
    }
}