<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 28.10.2019
 * Time: 10:23
 */

namespace Modules\Store\Presenters;


use Illuminate\Support\Facades\View;
use Modules\Store\Repositories\ProductRepository;
use Modules\Store\Repositories\TypeRepository;

class StorePresenter
{
    /**
     * @var TypeRepository
     */
    private $type;
    /**
     * @var ProductRepository
     */
    private $item;

    public function __construct(TypeRepository $type, ProductRepository $item)
    {

        $this->type = $type;
        $this->item = $item;
    }

    public function show()
    {
        $template = 'store::frontend.store';

        $types = $this->type->all();
        $items = $this->item->all();

        $view = View::make($template)->with(compact('types', 'items'));
        return $view->render();
    }
}