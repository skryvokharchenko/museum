<?php

return [
    'templateparts.pageparts' => [
        'index' => 'templateparts::pageparts.list resource',
        'create' => 'templateparts::pageparts.create resource',
        'edit' => 'templateparts::pageparts.edit resource',
        'destroy' => 'templateparts::pageparts.destroy resource',
    ],
// append

];
