<?php

namespace Modules\Templateparts\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Templateparts\Events\Handlers\RegisterTemplatepartsSidebar;

class TemplatepartsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterTemplatepartsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('pageparts', array_dot(trans('templateparts::pageparts')));
            // append translations

        });
    }

    public function boot()
    {
        $this->publishConfig('templateparts', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Templateparts\Repositories\PagePartRepository',
            function () {
                $repository = new \Modules\Templateparts\Repositories\Eloquent\EloquentPagePartRepository(new \Modules\Templateparts\Entities\PagePart());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Templateparts\Repositories\Cache\CachePagePartDecorator($repository);
            }
        );
// add bindings

    }
}
