<?php

namespace Modules\Templateparts\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface PagePartRepository extends BaseRepository
{
}
