<?php

namespace Modules\Templateparts\Repositories\Eloquent;

use App\Repositories\Traits\Mediable;
use Modules\Templateparts\Repositories\PagePartRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPagePartRepository extends EloquentBaseRepository implements PagePartRepository
{
    use Mediable;
}
