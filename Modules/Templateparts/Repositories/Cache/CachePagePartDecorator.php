<?php

namespace Modules\Templateparts\Repositories\Cache;

use Modules\Templateparts\Repositories\PagePartRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePagePartDecorator extends BaseCacheDecorator implements PagePartRepository
{
    public function __construct(PagePartRepository $pagepart)
    {
        parent::__construct();
        $this->entityName = 'templateparts.pageparts';
        $this->repository = $pagepart;
    }
}
