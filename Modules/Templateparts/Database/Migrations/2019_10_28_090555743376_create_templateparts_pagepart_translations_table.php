<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatepartsPagePartTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templateparts__pagepart_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('title', 100)->nullable();
            $table->text('body')->nullable();
            $table->integer('pagepart_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['pagepart_id', 'locale']);
            $table->foreign('pagepart_id')->references('id')->on('templateparts__pageparts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('templateparts__pagepart_translations', function (Blueprint $table) {
            $table->dropForeign(['pagepart_id']);
        });
        Schema::dropIfExists('templateparts__pagepart_translations');
    }
}
