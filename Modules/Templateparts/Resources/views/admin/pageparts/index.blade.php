@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('templateparts::pageparts.title.pageparts') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('templateparts::pageparts.title.pageparts') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.templateparts.pagepart.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('templateparts::pageparts.button.create pagepart') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('templateparts::pageparts.form.title') }}</th>
                                <th>{{ trans('templateparts::pageparts.form.page_id') }}</th>
                                <th>{{ trans('templateparts::pageparts.form.system_name') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($pageparts)): ?>
                            <?php foreach ($pageparts as $pagepart): ?>
                            <tr>
                                <td>
                                    <a href="{{ route('admin.templateparts.pagepart.edit', [$pagepart->id]) }}">
                                        {{ $pagepart->created_at }}
                                    </a>
                                </td>
                                <td>
                                    {{ $pagepart->title }}
                                </td>
                                <td>
                                    {{ $pagepart->page->title }}
                                </td>
                                <td>
                                    {{ $pagepart->system_name }}
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.templateparts.pagepart.edit', [$pagepart->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.templateparts.pagepart.destroy', [$pagepart->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('templateparts::pageparts.form.title') }}</th>
                                <th>{{ trans('templateparts::pageparts.form.page_id') }}</th>
                                <th>{{ trans('templateparts::pageparts.form.system_name') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('templateparts::pageparts.title.create pagepart') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.templateparts.pagepart.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "pageLength": 25,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
