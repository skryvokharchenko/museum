<div class="form-group {{ $errors->has("system_name") ? ' has-error' : '' }}">
    {!! Form::label("system_name", trans('templateparts::pageparts.form.system_name')) !!}
    {!! Form::text("system_name", old("system_name"), ["class" => "form-control", "placeholder" => trans('templateparts::pageparts.form.system_name')]) !!}
    {!! $errors->first("system_name", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("page_id") ? ' has-error' : '' }}">
    {!! Form::label("page_id", trans('templateparts::pageparts.form.page_id')) !!}
    {!! Form::select("page_id", $pages, old('page_id'), ["class" => "form-control"]) !!}
    {!! $errors->first("page_id", '<span class="help-block">:message</span>') !!}
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('partImage', null, null, trans('templateparts::pageparts.form.photo'))
    </div>
</div>