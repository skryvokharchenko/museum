<div class="form-group {{ $errors->has("{$lang}[title]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[title]", trans('templateparts::pageparts.form.title')) !!}
    <?php $old = $pagepart->hasTranslation($lang) ? $pagepart->translate($lang)->title : '' ?>
    {!! Form::text("{$lang}[title]", old("{$lang}[title]", $old), ["class" => "form-control", "placeholder" => trans('templateparts::pageparts.form.title')]) !!}
    {!! $errors->first("{$lang}[title]", '<span class="help-block">:message</span>') !!}
</div>

{!! Form::i18nTextarea('body', trans('templateparts::pageparts.form.body'), $errors, $lang, $pagepart) !!}