<?php

return [
    'list resource' => 'List pageparts',
    'create resource' => 'Create pageparts',
    'edit resource' => 'Edit pageparts',
    'destroy resource' => 'Destroy pageparts',
    'title' => [
        'pageparts' => 'PagePart',
        'create pagepart' => 'Create a pagepart',
        'edit pagepart' => 'Edit a pagepart',
    ],
    'button' => [
        'create pagepart' => 'Create a pagepart',
    ],
    'table' => [
    ],
    'form' => [
        'system_name' => 'System Name',
        'title' => 'Title',
        'body' => 'Body',
        'photo' => 'Image',
        'page_id' => 'Page'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
