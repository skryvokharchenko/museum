<?php

namespace Modules\Templateparts\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Page\Entities\Page;

class PagePart extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'templateparts__pageparts';
    protected $translationForeignKey = 'pagepart_id';
    public $translatedAttributes = ['title', 'body'];
    protected $fillable = [
        'system_name',
        'title',
        'body',
        'page_id'
    ];

    public function getImageUrlAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->path : '';
    }

    public function getAltTextAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->alt_attribute : '';
    }

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
