<?php

namespace Modules\Templateparts\Entities;

use Illuminate\Database\Eloquent\Model;

class PagePartTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'body'];
    protected $table = 'templateparts__pagepart_translations';
}
