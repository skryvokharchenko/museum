<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/templateparts'], function (Router $router) {
    $router->bind('pagepart', function ($id) {
        return app('Modules\Templateparts\Repositories\PagePartRepository')->find($id);
    });
    $router->get('pageparts', [
        'as' => 'admin.templateparts.pagepart.index',
        'uses' => 'PagePartController@index',
        'middleware' => 'can:templateparts.pageparts.index'
    ]);
    $router->get('pageparts/create', [
        'as' => 'admin.templateparts.pagepart.create',
        'uses' => 'PagePartController@create',
        'middleware' => 'can:templateparts.pageparts.create'
    ]);
    $router->post('pageparts', [
        'as' => 'admin.templateparts.pagepart.store',
        'uses' => 'PagePartController@store',
        'middleware' => 'can:templateparts.pageparts.create'
    ]);
    $router->get('pageparts/{pagepart}/edit', [
        'as' => 'admin.templateparts.pagepart.edit',
        'uses' => 'PagePartController@edit',
        'middleware' => 'can:templateparts.pageparts.edit'
    ]);
    $router->put('pageparts/{pagepart}', [
        'as' => 'admin.templateparts.pagepart.update',
        'uses' => 'PagePartController@update',
        'middleware' => 'can:templateparts.pageparts.edit'
    ]);
    $router->delete('pageparts/{pagepart}', [
        'as' => 'admin.templateparts.pagepart.destroy',
        'uses' => 'PagePartController@destroy',
        'middleware' => 'can:templateparts.pageparts.destroy'
    ]);
// append

});
