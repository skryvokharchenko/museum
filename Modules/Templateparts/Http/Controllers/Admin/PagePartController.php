<?php

namespace Modules\Templateparts\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Page\Repositories\PageRepository;
use Modules\Templateparts\Entities\PagePart;
use Modules\Templateparts\Http\Requests\CreatePagePartRequest;
use Modules\Templateparts\Http\Requests\UpdatePagePartRequest;
use Modules\Templateparts\Repositories\PagePartRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PagePartController extends AdminBaseController
{
    /**
     * @var PagePartRepository
     */
    private $pagepart;
    /**
     * @var PageRepository
     */
    private $page;

    public function __construct(PagePartRepository $pagepart, PageRepository $page)
    {
        parent::__construct();

        $this->pagepart = $pagepart;
        $this->page = $page;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $pageparts = $this->pagepart->all();

        return view('templateparts::admin.pageparts.index', compact('pageparts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $pages = $this->page->all()->pluck('title', 'id');
        return view('templateparts::admin.pageparts.create', compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePagePartRequest $request
     * @return Response
     */
    public function store(CreatePagePartRequest $request)
    {
        $this->pagepart->create($request->all());

        return redirect()->route('admin.templateparts.pagepart.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('templateparts::pageparts.title.pageparts')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  PagePart $pagepart
     * @return Response
     */
    public function edit(PagePart $pagepart)
    {
        $pages = $this->page->all()->pluck('title', 'id');
        return view('templateparts::admin.pageparts.edit', compact('pagepart', 'pages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PagePart $pagepart
     * @param  UpdatePagePartRequest $request
     * @return Response
     */
    public function update(PagePart $pagepart, UpdatePagePartRequest $request)
    {
        $this->pagepart->update($pagepart, $request->all());

        return redirect()->route('admin.templateparts.pagepart.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('templateparts::pageparts.title.pageparts')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PagePart $pagepart
     * @return Response
     */
    public function destroy(PagePart $pagepart)
    {
        $this->pagepart->destroy($pagepart);

        return redirect()->route('admin.templateparts.pagepart.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('templateparts::pageparts.title.pageparts')]));
    }
}
