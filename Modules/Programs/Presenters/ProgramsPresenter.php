<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 28.10.2019
 * Time: 08:28
 */

namespace Modules\Programs\Presenters;


use Illuminate\Support\Facades\View;
use Modules\Programs\Repositories\ExcursionRepository;
use Modules\Programs\Repositories\ProgramRepository;
use Modules\Programs\Repositories\TypeRepository;

class ProgramsPresenter
{
    /**
     * @var ProgramRepository
     */
    private $program;
    /**
     * @var TypeRepository
     */
    private $type;
    /**
     * @var ExcursionRepository
     */
    private $excursion;

    public function __construct(ProgramRepository $program, TypeRepository $type, ExcursionRepository $excursion)
    {

        $this->program = $program;
        $this->type = $type;
        $this->excursion = $excursion;
    }

    public function show($pageId)
    {
        $types = $this->type->all();
        $currentType = $types->firstWhere('page_id', $pageId);
        if ($currentType) {
            $programs = $this->program->getByAttributes(['type_id' => $currentType->id], 'order');
            $excursions = $this->excursion->getByAttributes(['type_id' => $currentType->id], 'order');

            $view = View::make('programs::frontend.programs-tabs')->with(compact('types', 'programs', 'excursions', 'currentType'));
            return $view->render();

        } else {
            abort(404);
        }
    }

}
