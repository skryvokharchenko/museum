<?php

namespace Modules\Programs\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Page\Repositories\PageRepository;
use Modules\Programs\Entities\Type;
use Modules\Programs\Http\Requests\CreateTypeRequest;
use Modules\Programs\Http\Requests\UpdateTypeRequest;
use Modules\Programs\Repositories\TypeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class TypeController extends AdminBaseController
{
    /**
     * @var TypeRepository
     */
    private $type;
    /**
     * @var PageRepository
     */
    private $page;

    public function __construct(TypeRepository $type, PageRepository $page)
    {
        parent::__construct();

        $this->type = $type;
        $this->page = $page;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $types = $this->type->all();

        return view('programs::admin.types.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $pages = $this->page->all()->pluck('title', 'id')->toArray();
        return view('programs::admin.types.create', compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateTypeRequest $request
     * @return Response
     */
    public function store(CreateTypeRequest $request)
    {
        $this->type->create($request->all());

        return redirect()->route('admin.programs.type.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('programs::types.title.types')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Type $type
     * @return Response
     */
    public function edit(Type $type)
    {
        $pages = $this->page->all()->pluck('title', 'id')->toArray();
        return view('programs::admin.types.edit', compact('type', 'pages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Type $type
     * @param  UpdateTypeRequest $request
     * @return Response
     */
    public function update(Type $type, UpdateTypeRequest $request)
    {
        $this->type->update($type, $request->all());

        return redirect()->route('admin.programs.type.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('programs::types.title.types')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Type $type
     * @return Response
     */
    public function destroy(Type $type)
    {
        $this->type->destroy($type);

        return redirect()->route('admin.programs.type.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('programs::types.title.types')]));
    }
}
