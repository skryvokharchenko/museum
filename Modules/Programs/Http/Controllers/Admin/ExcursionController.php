<?php

namespace Modules\Programs\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Programs\Entities\Excursion;
use Modules\Programs\Http\Requests\CreateExcursionRequest;
use Modules\Programs\Http\Requests\UpdateExcursionRequest;
use Modules\Programs\Repositories\ExcursionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Programs\Repositories\FeatureRepository;
use Modules\Programs\Repositories\TypeRepository;

class ExcursionController extends AdminBaseController
{
    /**
     * @var ExcursionRepository
     */
    private $excursion;
    /**
     * @var FeatureRepository
     */
    private $feature;
    /**
     * @var TypeRepository
     */
    private $type;

    public function __construct(ExcursionRepository $excursion, FeatureRepository $feature, TypeRepository $type)
    {
        parent::__construct();

        $this->excursion = $excursion;
        $this->feature = $feature;
        $this->type = $type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $excursions = $this->excursion->all();

        return view('programs::admin.excursions.index', compact('excursions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $types = $this->type->all()->pluck('name', 'id')->toArray();
        $features = $this->feature->all()->pluck('name', 'id')->toArray();
        return view('programs::admin.excursions.create', compact('types', 'features'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateExcursionRequest $request
     * @return Response
     */
    public function store(CreateExcursionRequest $request)
    {
        $this->excursion->create($request->all());

        return redirect()->route('admin.programs.excursion.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('programs::excursions.title.excursions')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Excursion $excursion
     * @return Response
     */
    public function edit(Excursion $excursion)
    {
        $types = $this->type->all()->pluck('name', 'id')->toArray();
        $features = $this->feature->all()->pluck('name', 'id')->toArray();
        return view('programs::admin.excursions.edit', compact('excursion', 'types', 'features'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Excursion $excursion
     * @param  UpdateExcursionRequest $request
     * @return Response
     */
    public function update(Excursion $excursion, UpdateExcursionRequest $request)
    {
        $this->excursion->update($excursion, $request->all());

        return redirect()->route('admin.programs.excursion.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('programs::excursions.title.excursions')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Excursion $excursion
     * @return Response
     */
    public function destroy(Excursion $excursion)
    {
        $this->excursion->destroy($excursion);

        return redirect()->route('admin.programs.excursion.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('programs::excursions.title.excursions')]));
    }
}
