<?php

namespace Modules\Programs\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Programs\Entities\Program;
use Modules\Programs\Http\Requests\CreateProgramRequest;
use Modules\Programs\Http\Requests\UpdateProgramRequest;
use Modules\Programs\Repositories\FeatureRepository;
use Modules\Programs\Repositories\ProgramRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Programs\Repositories\TypeRepository;

class ProgramController extends AdminBaseController
{
    /**
     * @var ProgramRepository
     */
    private $program;
    /**
     * @var FeatureRepository
     */
    private $feature;
    /**
     * @var TypeRepository
     */
    private $type;

    public function __construct(ProgramRepository $program, FeatureRepository $feature, TypeRepository $type)
    {
        parent::__construct();

        $this->program = $program;
        $this->feature = $feature;
        $this->type = $type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $programs = $this->program->all();

        return view('programs::admin.programs.index', compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $types = $this->type->all()->pluck('name', 'id')->toArray();
        $features = $this->feature->all()->pluck('name', 'id')->toArray();
        return view('programs::admin.programs.create', compact('types', 'features'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProgramRequest $request
     * @return Response
     */
    public function store(CreateProgramRequest $request)
    {
        $this->program->create($request->all());

        return redirect()->route('admin.programs.program.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('programs::programs.title.programs')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Program $program
     * @return Response
     */
    public function edit(Program $program)
    {
        $types = $this->type->all()->pluck('name', 'id')->toArray();
        $features = $this->feature->all()->pluck('name', 'id')->toArray();
        return view('programs::admin.programs.edit', compact('program', 'types', 'features'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Program $program
     * @param  UpdateProgramRequest $request
     * @return Response
     */
    public function update(Program $program, UpdateProgramRequest $request)
    {
        $this->program->update($program, $request->all());

        return redirect()->route('admin.programs.program.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('programs::programs.title.programs')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Program $program
     * @return Response
     */
    public function destroy(Program $program)
    {
        $this->program->destroy($program);

        return redirect()->route('admin.programs.program.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('programs::programs.title.programs')]));
    }
}
