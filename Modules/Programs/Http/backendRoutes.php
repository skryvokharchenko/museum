<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/programs'], function (Router $router) {
    $router->bind('program', function ($id) {
        return app('Modules\Programs\Repositories\ProgramRepository')->find($id);
    });
    $router->get('programs', [
        'as' => 'admin.programs.program.index',
        'uses' => 'ProgramController@index',
        'middleware' => 'can:programs.programs.index'
    ]);
    $router->get('programs/create', [
        'as' => 'admin.programs.program.create',
        'uses' => 'ProgramController@create',
        'middleware' => 'can:programs.programs.create'
    ]);
    $router->post('programs', [
        'as' => 'admin.programs.program.store',
        'uses' => 'ProgramController@store',
        'middleware' => 'can:programs.programs.create'
    ]);
    $router->get('programs/{program}/edit', [
        'as' => 'admin.programs.program.edit',
        'uses' => 'ProgramController@edit',
        'middleware' => 'can:programs.programs.edit'
    ]);
    $router->put('programs/{program}', [
        'as' => 'admin.programs.program.update',
        'uses' => 'ProgramController@update',
        'middleware' => 'can:programs.programs.edit'
    ]);
    $router->delete('programs/{program}', [
        'as' => 'admin.programs.program.destroy',
        'uses' => 'ProgramController@destroy',
        'middleware' => 'can:programs.programs.destroy'
    ]);
    $router->bind('excursion', function ($id) {
        return app('Modules\Programs\Repositories\ExcursionRepository')->find($id);
    });
    $router->get('excursions', [
        'as' => 'admin.programs.excursion.index',
        'uses' => 'ExcursionController@index',
        'middleware' => 'can:programs.excursions.index'
    ]);
    $router->get('excursions/create', [
        'as' => 'admin.programs.excursion.create',
        'uses' => 'ExcursionController@create',
        'middleware' => 'can:programs.excursions.create'
    ]);
    $router->post('excursions', [
        'as' => 'admin.programs.excursion.store',
        'uses' => 'ExcursionController@store',
        'middleware' => 'can:programs.excursions.create'
    ]);
    $router->get('excursions/{excursion}/edit', [
        'as' => 'admin.programs.excursion.edit',
        'uses' => 'ExcursionController@edit',
        'middleware' => 'can:programs.excursions.edit'
    ]);
    $router->put('excursions/{excursion}', [
        'as' => 'admin.programs.excursion.update',
        'uses' => 'ExcursionController@update',
        'middleware' => 'can:programs.excursions.edit'
    ]);
    $router->delete('excursions/{excursion}', [
        'as' => 'admin.programs.excursion.destroy',
        'uses' => 'ExcursionController@destroy',
        'middleware' => 'can:programs.excursions.destroy'
    ]);
    $router->bind('program_type', function ($id) {
        return app('Modules\Programs\Repositories\TypeRepository')->find($id);
    });
    $router->get('types', [
        'as' => 'admin.programs.type.index',
        'uses' => 'TypeController@index',
        'middleware' => 'can:programs.types.index'
    ]);
    $router->get('types/create', [
        'as' => 'admin.programs.type.create',
        'uses' => 'TypeController@create',
        'middleware' => 'can:programs.types.create'
    ]);
    $router->post('types', [
        'as' => 'admin.programs.type.store',
        'uses' => 'TypeController@store',
        'middleware' => 'can:programs.types.create'
    ]);
    $router->get('types/{program_type}/edit', [
        'as' => 'admin.programs.type.edit',
        'uses' => 'TypeController@edit',
        'middleware' => 'can:programs.types.edit'
    ]);
    $router->put('types/{program_type}', [
        'as' => 'admin.programs.type.update',
        'uses' => 'TypeController@update',
        'middleware' => 'can:programs.types.edit'
    ]);
    $router->delete('types/{program_type}', [
        'as' => 'admin.programs.type.destroy',
        'uses' => 'TypeController@destroy',
        'middleware' => 'can:programs.types.destroy'
    ]);
    $router->bind('feature', function ($id) {
        return app('Modules\Programs\Repositories\FeatureRepository')->find($id);
    });
    $router->get('features', [
        'as' => 'admin.programs.feature.index',
        'uses' => 'FeatureController@index',
        'middleware' => 'can:programs.features.index'
    ]);
    $router->get('features/create', [
        'as' => 'admin.programs.feature.create',
        'uses' => 'FeatureController@create',
        'middleware' => 'can:programs.features.create'
    ]);
    $router->post('features', [
        'as' => 'admin.programs.feature.store',
        'uses' => 'FeatureController@store',
        'middleware' => 'can:programs.features.create'
    ]);
    $router->get('features/{feature}/edit', [
        'as' => 'admin.programs.feature.edit',
        'uses' => 'FeatureController@edit',
        'middleware' => 'can:programs.features.edit'
    ]);
    $router->put('features/{feature}', [
        'as' => 'admin.programs.feature.update',
        'uses' => 'FeatureController@update',
        'middleware' => 'can:programs.features.edit'
    ]);
    $router->delete('features/{feature}', [
        'as' => 'admin.programs.feature.destroy',
        'uses' => 'FeatureController@destroy',
        'middleware' => 'can:programs.features.destroy'
    ]);
// append




});
