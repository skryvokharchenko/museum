<?php

namespace Modules\Programs\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterProgramsSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('programs::programs.title.programs'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('programs::programs.title.programs'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.programs.program.create');
                    $item->route('admin.programs.program.index');
                    $item->authorize(
                        $this->auth->hasAccess('programs.programs.index')
                    );
                });
                $item->item(trans('programs::excursions.title.excursions'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.programs.excursion.create');
                    $item->route('admin.programs.excursion.index');
                    $item->authorize(
                        $this->auth->hasAccess('programs.excursions.index')
                    );
                });
                $item->item(trans('programs::types.title.types'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.programs.type.create');
                    $item->route('admin.programs.type.index');
                    $item->authorize(
                        $this->auth->hasAccess('programs.types.index')
                    );
                });
                $item->item(trans('programs::features.title.features'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.programs.feature.create');
                    $item->route('admin.programs.feature.index');
                    $item->authorize(
                        $this->auth->hasAccess('programs.features.index')
                    );
                });
// append




            });
        });

        return $menu;
    }
}
