<?php

return [
    'list resource' => 'List excursions',
    'create resource' => 'Create excursions',
    'edit resource' => 'Edit excursions',
    'destroy resource' => 'Destroy excursions',
    'title' => [
        'excursions' => 'Excursion',
        'create excursion' => 'Create a excursion',
        'edit excursion' => 'Edit a excursion',
    ],
    'button' => [
        'create excursion' => 'Create a excursion',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'description' => 'Description',
        'time' => 'Time',
        'guide' => 'Guide',
        'category' => 'Category',
        'min_age' => 'Min age',
        'max_age' => 'Max age',
        'max_group' => 'Max group size',
        'duration' => 'Lesson duration',
        'phone' => 'Contact Phone',
        'price' => 'Price',
        'photo' => 'Photo',
        'type' => 'Type',
        'features' => 'Features',
        'dates' => 'Available Dates'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
