<?php

return [
    'list resource' => 'List types',
    'create resource' => 'Create types',
    'edit resource' => 'Edit types',
    'destroy resource' => 'Destroy types',
    'title' => [
        'types' => 'Type',
        'create type' => 'Create a type',
        'edit type' => 'Edit a type',
    ],
    'button' => [
        'create type' => 'Create a type',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'program_description' => 'Description (Programs)',
        'excursion_description' => 'Description (Excursions)',
        'active' => 'Active',
        'page_id' => 'Page',
        'photo' => 'Photo'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
