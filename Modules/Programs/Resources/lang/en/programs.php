<?php

return [
    'list resource' => 'List programs',
    'create resource' => 'Create programs',
    'edit resource' => 'Edit programs',
    'destroy resource' => 'Destroy programs',
    'title' => [
        'programs' => 'Program',
        'create program' => 'Create a program',
        'edit program' => 'Edit a program',
    ],
    'button' => [
        'create program' => 'Create a program',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'description' => 'Description',
        'lessons_schedule' => 'Schedule',
        'narrator' => 'Narrator',
        'min_age' => 'Min age',
        'max_age' => 'Max age',
        'max_group' => 'Max group size',
        'lessons_count' => 'Lessons count',
        'duration' => 'Lesson duration',
        'category' => 'Category',
        'phone' => 'Contact Phone',
        'price' => 'Price',
        'photo' => 'Photo',
        'type' => 'Type',
        'features' => 'Features',
        'dates' => 'Available Dates'
    ],
    'frontend' => [
        'leave_comment' => 'Leave comment',
        'leave_request' => 'Send request',
        'programs' => 'Programs',
        'excursions' => 'Excursions',
        'name' => 'Title',
        'description' => 'Description',
        'lessons_schedule' => 'When',
        'narrator' => 'Tour guide',
        'guide' => 'Tour guide',
        'time' => 'Time',
        'category' => 'Category',
        'age' => 'Age',
        'max_group' => 'Group size',
        'lessons_count' => 'Lessons count',
        'duration' => 'Duration',
        'phone' => 'Contact Phone',
        'price' => 'Price',
        'photo' => 'Photo',
        'type' => 'Type',
        'dates' => 'Dates'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
