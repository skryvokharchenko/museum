<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('programs::types.form.name')) !!}
    <?php $old = $type->hasTranslation($lang) ? $type->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('programs::types.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[program_description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[program_description]", trans('programs::types.form.program_description')) !!}
    <?php $old = $type->hasTranslation($lang) ? $type->translate($lang)->program_description : '' ?>
    {!! Form::textarea("{$lang}[program_description]", old("{$lang}[program_description]", $old), ["class" => "form-control", "placeholder" => trans('programs::types.form.program_description')]) !!}
    {!! $errors->first("{$lang}[program_description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[excursion_description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[excursion_description]", trans('programs::types.form.excursion_description')) !!}
    <?php $old = $type->hasTranslation($lang) ? $type->translate($lang)->excursion_description : '' ?>
    {!! Form::textarea("{$lang}[excursion_description]", old("{$lang}[excursion_description]", $old), ["class" => "form-control", "placeholder" => trans('programs::types.form.excursion_description')]) !!}
    {!! $errors->first("{$lang}[excursion_description]", '<span class="help-block">:message</span>') !!}
</div>