<div class="form-group {{ $errors->has("page_id") ? ' has-error' : '' }}">
    {!! Form::label("page_id", trans('programs::types.form.page_id')) !!}
    {!! Form::select("page_id", $pages, old('page_id'), ["class" => "form-control"]) !!}
    {!! $errors->first("page_id", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("active") ? ' has-error' : '' }}">
    {!! Form::checkbox("active", 1, true, ["class" => "flat-blue"]) !!}
    {!! Form::label("active", trans('programs::types.form.active')) !!}
    {!! $errors->first("active", '<span class="help-block">:message</span>') !!}
</div>
