<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('programs::features.form.name')) !!}
    <?php $old = $feature->hasTranslation($lang) ? $feature->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('programs::features.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>
