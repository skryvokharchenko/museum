<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('featureIcon', $feature, null, trans('programs::features.form.icon'))
    </div>
</div>

<div class="form-group {{ $errors->has("active") ? ' has-error' : '' }}">
    <input type="hidden" name="active" value="0">
    {!! Form::checkbox("active", 1, old("active", $feature->active), ["class" => "flat-blue"]) !!}
    {!! Form::label("active", trans('programs::features.form.active')) !!}
    {!! $errors->first("active", '<span class="help-block">:message</span>') !!}
</div>
