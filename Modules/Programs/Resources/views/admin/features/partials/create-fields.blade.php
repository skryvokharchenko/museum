<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('featureIcon', null, null, trans('programs::features.form.icon'))
    </div>
</div>

<div class="form-group {{ $errors->has("active") ? ' has-error' : '' }}">
    {!! Form::checkbox("active", 1, true, ["class" => "flat-blue"]) !!}
    {!! Form::label("active", trans('programs::features.form.active')) !!}
    {!! $errors->first("active", '<span class="help-block">:message</span>') !!}
</div>
