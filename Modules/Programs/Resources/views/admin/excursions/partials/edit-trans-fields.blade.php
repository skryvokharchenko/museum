<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('programs::excursions.form.name')) !!}
    <?php $old = $excursion->hasTranslation($lang) ? $excursion->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('programs::excursions.form.description')) !!}
    <?php $old = $excursion->hasTranslation($lang) ? $excursion->translate($lang)->description : '' ?>
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]", $old), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[guide]") ? ' has-error' : '' }}">
    @if($lang == 'en')
        @foreach(['Guide', 'Guides'] as $caption)
            <label><input type="radio" name="{{$lang}}[guide_caption]" class="flat-blue" value="{{ $caption }}" {{$excursion->guide_caption == $caption ? 'checked' : ''}}> {{$caption}}</label>
        @endforeach
    @else
        @foreach(['Ведучий', 'Ведуча', 'Ведучі'] as $caption)
            <label><input type="radio" name="{{$lang}}[guide_caption]" class="flat-blue" value="{{ $caption }}" {{$excursion->guide_caption == $caption ? 'checked' : ''}}> {{$caption}}</label>
        @endforeach
    @endif
    <?php $old = $excursion->hasTranslation($lang) ? $excursion->translate($lang)->guide : '' ?>
    {!! Form::text("{$lang}[guide]", old("{$lang}[guide]", $old), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.guide')]) !!}
    {!! $errors->first("{$lang}[guide]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[max_group]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[max_group]", trans('programs::excursions.form.max_group')) !!}
    <?php $old = $excursion->hasTranslation($lang) ? $excursion->translate($lang)->max_group : '' ?>
    {!! Form::text("{$lang}[max_group]", old("{$lang}[max_group]", $old), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.max_group')]) !!}
    {!! $errors->first("{$lang}[max_group]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[category]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[category]", trans('programs::excursions.form.category')) !!}
    <?php $old = $excursion->hasTranslation($lang) ? $excursion->translate($lang)->category : '' ?>
    {!! Form::text("{$lang}[category]", old("{$lang}[category]", $old), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.category')]) !!}
    {!! $errors->first("{$lang}[category]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[phone]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[phone]", trans('programs::excursions.form.phone')) !!}
    <?php $old = $excursion->hasTranslation($lang) ? $excursion->translate($lang)->phone : '' ?>
    {!! Form::text("{$lang}[phone]", old("{$lang}[phone]", $old), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.phone')]) !!}
    {!! $errors->first("{$lang}[phone]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[price]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[price]", trans('programs::excursions.form.price')) !!}
    <?php $old = $excursion->hasTranslation($lang) ? $excursion->translate($lang)->price : '' ?>
    {!! Form::text("{$lang}[price]", old("{$lang}[price]", $old), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.price')]) !!}
    {!! $errors->first("{$lang}[price]", '<span class="help-block">:message</span>') !!}
</div>
