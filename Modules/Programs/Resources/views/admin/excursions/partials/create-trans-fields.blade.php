<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('programs::excursions.form.name')) !!}
    {!! Form::text("{$lang}[name]", old("{$lang}[name]"), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('programs::excursions.form.description')) !!}
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]"), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[guide]") ? ' has-error' : '' }}">
    @if($lang == 'en')
        @foreach(['Guide', 'Guides'] as $caption)
            <label><input type="radio" name="{{$lang}}[guide_caption]" class="flat-blue" value="{{ $caption }}" {{$loop->first ? 'checked' : ''}}> {{$caption}}</label>
        @endforeach
    @else
        @foreach(['Ведучий', 'Ведуча', 'Ведучі'] as $caption)
            <label><input type="radio" name="{{$lang}}[guide_caption]" class="flat-blue" value="{{ $caption }}" {{$loop->first ? 'checked' : ''}}> {{$caption}}</label>
        @endforeach
    @endif
    {!! Form::text("{$lang}[guide]", old("{$lang}[guide]"), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.guide')]) !!}
    {!! $errors->first("{$lang}[guide]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[max_group]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[max_group]", trans('programs::excursions.form.max_group')) !!}
    {!! Form::text("{$lang}[max_group]", old("{$lang}[max_group]"), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.max_group')]) !!}
    {!! $errors->first("{$lang}[max_group]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[category]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[category]", trans('programs::excursions.form.category')) !!}
    {!! Form::text("{$lang}[category]", old("{$lang}[category]"), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.category')]) !!}
    {!! $errors->first("{$lang}[category]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[phone]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[phone]", trans('programs::excursions.form.phone')) !!}
    {!! Form::text("{$lang}[phone]", old("{$lang}[phone]"), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.phone')]) !!}
    {!! $errors->first("{$lang}[phone]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[price]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[price]", trans('programs::excursions.form.price')) !!}
    {!! Form::text("{$lang}[price]", old("{$lang}[price]"), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.price')]) !!}
    {!! $errors->first("{$lang}[price]", '<span class="help-block">:message</span>') !!}
</div>
