<div class="form-group {{ $errors->has("time") ? ' has-error' : '' }}">
    {!! Form::label("time", trans('programs::excursions.form.time')) !!}
    {!! Form::text("time", old("time"), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.time')]) !!}
    {!! $errors->first("time", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("duration") ? ' has-error' : '' }}">
    {!! Form::label("duration", trans('programs::excursions.form.duration')) !!}
    {!! Form::number("duration", old("duration"), ["class" => "form-control", "placeholder" => trans('programs::excursions.form.duration')]) !!}
    {!! $errors->first("duration", '<span class="help-block">:message</span>') !!}
</div>

{!! Form::normalSelect('type_id', trans('programs::excursions.form.type'), $errors, $types, null, ['class' => '']) !!}
{!! Form::normalSelect('dates', trans('programs::excursions.form.dates'), $errors, [], null, ['class' => '', 'multiple' => 'multiple']) !!}
{!! Form::normalSelect('features', trans('programs::excursions.form.features'), $errors, $features, null, ['class' => '', 'multiple' => 'multiple']) !!}

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaMultiple('excursionImage', null, null, trans('programs::excursions.form.photo'))
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('excursionPdf', null, null, 'PDF')
    </div>
</div>
