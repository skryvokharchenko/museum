@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('programs::excursions.title.excursions') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('programs::excursions.title.excursions') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.programs.excursion.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('programs::excursions.button.create excursion') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('programs::excursions.form.name') }}</th>
                                <th>{{ trans('programs::excursions.form.type') }}</th>
                                <th>{{ trans('team::departments.form.order') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($excursions)): ?>
                            <?php foreach ($excursions as $excursion): ?>
                            <tr>
                                <td>
                                    <a href="{{ route('admin.programs.excursion.edit', [$excursion->id]) }}">
                                        {{ $excursion->created_at }}
                                    </a>
                                </td>
                                <td>
                                    {{ $excursion->name }}
                                </td>
                                <td>
                                    {{ $excursion->type->name }}
                                </td>
                                <td><a class="order editable" style="text-decoration: underline dotted" data-pk="{{ $excursion->id }}">{{ $excursion->order }}</a></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.programs.excursion.edit', [$excursion->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.programs.excursion.destroy', [$excursion->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('programs::excursions.form.name') }}</th>
                                <th>{{ trans('programs::excursions.form.type') }}</th>
                                <th>{{ trans('team::departments.form.order') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('programs::excursions.title.create excursion') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.programs.excursion.create') ?>" }
                ]
            });
        });
        $('a.order').editable({
            url: function(params) {
                var that = this;
                var pk = params.pk;
                var value = params.value;

                if (!pk) {
                    return false;
                }

                var data = {
                    order: value,
                    _method: 'PUT'
                };

                $.ajax({
                    url: '/backend/programs/excursions/' + pk,
                    headers: {
                        'Authorization': 'Bearer {{ $currentUser->getFirstApiKey() }}',
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: 'POST',
                    dataType: 'JSON',
                    data: data,
                    success: function(res, newValue) {
                        $(that).show();
                        return {newValue: res.newValue};
                    }
                })
            },
            type: 'text',
            mode: 'inline',
            send: 'always', /* Always send, because we have no 'pk' which editable expects */
            inputclass: 'translation_input'
        });

    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "pageLength": 25,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
