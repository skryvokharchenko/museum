<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('programs::programs.form.name')) !!}
    {!! Form::text("{$lang}[name]", old("{$lang}[name]"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('programs::programs.form.description')) !!}
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[lessons_schedule]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[lessons_schedule]", trans('programs::programs.form.lessons_schedule')) !!}
    {!! Form::text("{$lang}[lessons_schedule]", old("{$lang}[lessons_schedule]"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.lessons_schedule')]) !!}
    {!! $errors->first("{$lang}[lessons_schedule]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[narrator]") ? ' has-error' : '' }}">
    @if($lang == 'en')
        @foreach(['Guide', 'Guides'] as $caption)
            <label><input type="radio" name="{{$lang}}[guide_caption]" class="flat-blue" value="{{ $caption }}" {{$loop->first ? 'checked' : ''}}> {{$caption}}</label>
        @endforeach
    @else
        @foreach(['Ведучий', 'Ведуча', 'Ведучі'] as $caption)
            <label><input type="radio" name="{{$lang}}[guide_caption]" class="flat-blue" value="{{ $caption }}" {{$loop->first ? 'checked' : ''}}> {{$caption}}</label>
        @endforeach
    @endif
    {!! Form::text("{$lang}[narrator]", old("{$lang}[narrator]"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.narrator')]) !!}
    {!! $errors->first("{$lang}[narrator]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[max_group]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[max_group]", trans('programs::programs.form.max_group')) !!}
    {!! Form::text("{$lang}[max_group]", old("{$lang}[max_group]"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.max_group')]) !!}
    {!! $errors->first("{$lang}[max_group]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[category]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[category]", trans('programs::programs.form.category')) !!}
    {!! Form::text("{$lang}[category]", old("{$lang}[category]"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.category')]) !!}
    {!! $errors->first("{$lang}[category]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[phone]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[phone]", trans('programs::programs.form.phone')) !!}
    {!! Form::text("{$lang}[phone]", old("{$lang}[phone]"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.phone')]) !!}
    {!! $errors->first("{$lang}[phone]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[price]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[price]", trans('programs::programs.form.price')) !!}
    {!! Form::text("{$lang}[price]", old("{$lang}[price]"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.price')]) !!}
    {!! $errors->first("{$lang}[price]", '<span class="help-block">:message</span>') !!}
</div>
