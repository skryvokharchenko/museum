<div class="form-group {{ $errors->has("lessons_count") ? ' has-error' : '' }}">
    {!! Form::label("lessons_count", trans('programs::programs.form.lessons_count')) !!}
    {!! Form::number("lessons_count", old("lessons_count", $program->lessons_count), ["class" => "form-control", "placeholder" => trans('programs::programs.form.lessons_count')]) !!}
    {!! $errors->first("lessons_count", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("duration") ? ' has-error' : '' }}">
    {!! Form::label("duration", trans('programs::programs.form.duration')) !!}
    {!! Form::number("duration", old("duration", $program->duration), ["class" => "form-control", "placeholder" => trans('programs::programs.form.duration')]) !!}
    {!! $errors->first("duration", '<span class="help-block">:message</span>') !!}
</div>

{!! Form::normalSelect('type_id', trans('programs::programs.form.type'), $errors, $types, $program, ['class' => '']) !!}
{!! Form::normalSelect('dates', trans('programs::programs.form.dates'), $errors, $program->form_dates, $program, ['class' => '', 'multiple' => 'multiple']) !!}
{!! Form::normalSelect('features', trans('programs::programs.form.features'), $errors, $features, $program, ['class' => '', 'multiple' => 'multiple']) !!}

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaMultiple('programImage', $program, null, trans('programs::programs.form.photo'))
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('programPdf', $program, null, 'PDF')
    </div>
</div>

