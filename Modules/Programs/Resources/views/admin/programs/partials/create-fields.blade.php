<div class="form-group {{ $errors->has("lessons_count") ? ' has-error' : '' }}">
    {!! Form::label("lessons_count", trans('programs::programs.form.lessons_count')) !!}
    {!! Form::number("lessons_count", old("lessons_count"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.lessons_count')]) !!}
    {!! $errors->first("lessons_count", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("duration") ? ' has-error' : '' }}">
    {!! Form::label("duration", trans('programs::programs.form.duration')) !!}
    {!! Form::number("duration", old("duration"), ["class" => "form-control", "placeholder" => trans('programs::programs.form.duration')]) !!}
    {!! $errors->first("duration", '<span class="help-block">:message</span>') !!}
</div>

{!! Form::normalSelect('type_id', trans('programs::programs.form.type'), $errors, $types, null, ['class' => '']) !!}
{!! Form::normalSelect('dates', trans('programs::programs.form.dates'), $errors, [], null, ['class' => '', 'multiple' => 'multiple']) !!}
{!! Form::normalSelect('features', trans('programs::programs.form.features'), $errors, $features, null, ['class' => '', 'multiple' => 'multiple']) !!}

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaMultiple('programImage', null, null, trans('programs::programs.form.photo'))
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaSingle('programPdf', null, null, 'PDF')
    </div>
</div>
