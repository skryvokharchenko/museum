<div class="store_head type1 font_color bg_color">
    <div class="content_container">
        <ul class="nav nav-pills nav_store nav-program mb-3" id="pills-tab" role="tablist">
            @foreach($types as $type)
                <li class="tab-item type2 font_color">
                    <a class="tab-link type1 {{ $currentType->id == $type->id ? 'active' : '' }}"
                       id="pills-tab-{{$type->page_id}}" title="{{ $type->name  }}" aria-label="{{ $type->name }}"
                       href="{{ route('page', $type->page->slug) }}" role="tab" aria-controls="pills-home" aria-selected="true">{{ $type->name }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>

<section>
    <div class="programs_container">
        <div class="programs_container_top">
            <div class="content_container">
                <div class="work_text mod1 font_color">
                    <form action="">
                        <div class="programs_radio">
                            <div class="radio_it">
                                <input type="radio" name="kind" id="excursions" value="excursions" class="radio_input" aria-label="Екскурсії">
                                <label for="excursions">{{ trans('programs::programs.frontend.excursions') }}</label>
                            </div>
                            <div class="radio_it">
                                <input type="radio" name="kind" id="programs" value="programs" class="radio_input" aria-label="Програми" checked>
                                <label for="programs">{{ trans('programs::programs.frontend.programs') }}</label>
                            </div>
                        </div>
                    </form>
                    <div id="excursionsText" style="display: none; text-indent: 20px;">{!! $currentType->excursion_formatted !!}</div>
                    <div id="programsText" style="text-indent: 20px;">{!! $currentType->program_formatted !!}</div>
                </div>
            </div>
        </div>


        <div class="programs_body_container bg_color">
            <div class="content_container">
                <div id="excursionsTab" class="pseudo-tab" style="display: none;">
                    @include('programs::frontend.items', ['items' => $excursions])
                </div>
                <div id="programsTab" class="pseudo-tab">
                    @include('programs::frontend.items', ['items' => $programs])
                </div>
            </div>
        </div>
    </div>
</section>
