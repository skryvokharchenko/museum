@foreach($items as $item)
    <div class="programs_item bg_color font_color">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="programs_slider_container">
                    <div class="programs_slider" data-slider role="region" aria-label="слайдер">
                        @foreach($item->images as $k => $slide)
                            <div class="programs_slider_it" aria-live="polite" aria-label="{{ $k + 1 }} слайд із {{ $item->files->count() }} ">
                                <img src="{!! $slide->path !!}" alt="{{ $slide->alt_attribute }}">
                            </div>
                        @endforeach
                    </div>
                    <div class="slider_btn_container type1">
                        <div class="btn_pause bg_color font_color active" tabindex="-1" title="призупинити слайдер" aria-label="зупинити слайдер" role="button" id="btn_pause">
                            <span class="icon-iconfinder_icon-pause_211871"></span>
                        </div>
                        <div class="btn_play bg_color font_color active" tabindex="-1" title="увімкнути слайдер" aria-label="програти слайдер" role="button" id="btn_play">
                            <span class="icon-iconfinder_icon-play_211876"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="programs_info_container">
                    <div class="programs_info_title">
                        <h2>{{ $item->name }}</h2>
                    </div>
                    <div class="programs_info_text" style="text-indent: 20px; text-align: justify;">
                        {!! $item->formatted_text !!}
                    </div>

                    <div class="icons_container">
                        @foreach($item->features as $feature)
                            <div class="icons_item mod1 bg_color font_color" title="{{$feature->name}}" aria-label="{{$feature->name}}">
                                <img src="{!! $feature->icon !!}" alt="{{$feature->name}}">
                            </div>
                        @endforeach
                    </div>

                    <div class="programs_info_list">
                        <ul>
                            @if($item->dates)
                            <li>
                                <div class="programs_select_container">
                                    <span class="icon-calendar list_icon font_color"></span>
                                    <label for="inputState"><b>{{trans('programs::programs.frontend.dates')}}:</b></label>
                                    <div class="select_container font_color">
                                        <select id="inputState" aria-label="вибір дати" class="form-control programs_select bg_color font_color">
                                            @foreach(($item->dates_ordered ?? []) as $val)
                                                <option selected value="{{ $val }}" aria-label="{{ $val }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                        <span class="icon-arrow_down"></span>
                                    </div>
                                </div>
                            </li>
                            @endif
                            @if ($item->category)
                                <li>
                                    <span class="icon-cart list_icon font_color"></span>
                                    <p><b>{{ trans('programs::programs.frontend.category') }}:</b> {{$item->category}}</p>
                                </li>
                            @endif
                            <li>
                                <span class="icon-user list_icon font_color"></span>
                                <p><b>{{trans('programs::programs.frontend.max_group')}}:</b>{{$item->max_group}}</p>
                            </li>
                            @if($item->lessons_count)
                                <li>
                                    <span class="icon-grid list_icon font_color"></span>
                                    <p><b>{{trans('programs::programs.frontend.lessons_count')}}:</b> {{$item->lessons_count}}</p>
                                </li>
                            @endif
                            @if(isset($item->lessons_schedule))
                                <li>
                                    <span class="icon-clock2 list_icon font_color"></span>
                                    <p><b>{{trans('programs::programs.frontend.lessons_schedule')}}:</b> {{$item->lessons_schedule}}</p>
                                </li>
                            @else
                                <li>
                                    <span class="icon-clock2 list_icon font_color"></span>
                                    <p><b>{{trans('programs::programs.frontend.time')}}:</b> {{$item->time}}</p>
                                </li>
                            @endif
                            <li>
                                <span class="icon-glass list_icon font_color"></span>
                                <p><b>{{trans('programs::programs.frontend.duration')}}:</b> {{$item->duration}}</p>
                            </li>
                            <li>
                                <span class="icon-phohe2 list_icon font_color"></span>
                                <p><b>{{trans('programs::programs.frontend.phone')}}:</b> {{$item->phone}}</p>
                            </li>
                            @if(isset($item->narrator))
                                <li>
                                    <span class="icon-users list_icon font_color"></span>
                                    <p><b>{{$item->guide_caption}}:</b> {{$item->narrator}}</p>
                                </li>
                            @else
                                <li>
                                    <span class="icon-users list_icon font_color"></span>
                                    <p><b>{{$item->guide_caption}}:</b> {{$item->guide}}</p>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="programs_info_price">
                        <h3>{{$item->price}}</h3>
                        <a href="#" role="button" data-toggle="modal" data-target="#modal2"
                           onclick="$('#modal2 input[name=\'program_id\']').val('{{$item->id}}')"
                           title="Надіслати запит" class="programs_btn programs_btn_info bg_color font_color" aria-label="{{trans('programs::programs.frontend.leave_request')}}">{{trans('programs::programs.frontend.leave_request')}}</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endforeach

