<?php

return [
    'programs.programs' => [
        'index' => 'programs::programs.list resource',
        'create' => 'programs::programs.create resource',
        'edit' => 'programs::programs.edit resource',
        'destroy' => 'programs::programs.destroy resource',
    ],
    'programs.excursions' => [
        'index' => 'programs::excursions.list resource',
        'create' => 'programs::excursions.create resource',
        'edit' => 'programs::excursions.edit resource',
        'destroy' => 'programs::excursions.destroy resource',
    ],
    'programs.types' => [
        'index' => 'programs::types.list resource',
        'create' => 'programs::types.create resource',
        'edit' => 'programs::types.edit resource',
        'destroy' => 'programs::types.destroy resource',
    ],
    'programs.features' => [
        'index' => 'programs::features.list resource',
        'create' => 'programs::features.create resource',
        'edit' => 'programs::features.edit resource',
        'destroy' => 'programs::features.destroy resource',
    ],
// append




];
