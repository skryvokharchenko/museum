<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs__programs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->text('description');
            $table->tinyInteger('min_age')->nullable();
            $table->tinyInteger('max_age')->nullable();
            $table->integer('order')->default(0);
            $table->tinyInteger('max_group');
            $table->tinyInteger('lessons_count')->nullable();
            $table->string('lessons_schedule', 100);
            $table->smallInteger('duration');
            $table->string('phone', 20);
            $table->string('narrator', 40);
            $table->text('dates');
            $table->string('price', 40);
            $table->integer('type_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs__programs');
    }
}
