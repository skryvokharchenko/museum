<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsExcursionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs__excursion_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name', 100);
            $table->text('description');
            $table->string('lessons_schedule', 100);
            $table->string('guide', 40);
            $table->string('guide_caption', 40);
            $table->string('max_group', 40);
            $table->string('price', 40);
            $table->string('phone', 20);
            $table->integer('excursion_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['excursion_id', 'locale']);
            $table->foreign('excursion_id')->references('id')->on('programs__excursions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programs__excursion_translations', function (Blueprint $table) {
            $table->dropForeign(['excursion_id']);
        });
        Schema::dropIfExists('programs__excursion_translations');
    }
}
