<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsProgramTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs__program_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name', 100);
            $table->text('description');
            $table->string('lessons_schedule', 100);
            $table->string('narrator', 40);
            $table->string('guide_caption', 40);
            $table->string('max_group', 40);
            $table->string('price', 40);
            $table->string('phone', 20);
            $table->integer('program_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['program_id', 'locale']);
            $table->foreign('program_id')->references('id')->on('programs__programs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programs__program_translations', function (Blueprint $table) {
            $table->dropForeign(['program_id']);
        });
        Schema::dropIfExists('programs__program_translations');
    }
}
