<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 28.10.2019
 * Time: 08:29
 */

namespace Modules\Programs\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Programs\Presenters\ProgramsPresenter;

class ProgramsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ProgramsPresenter::class;
    }
}