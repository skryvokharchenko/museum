<?php

namespace Modules\Programs\Entities;

use Illuminate\Database\Eloquent\Model;

class ProgramTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'lessons_schedule',
        'narrator',
        'max_group',
        'phone',
        'price',
        'guide_caption',
        'category'
    ];
    protected $table = 'programs__program_translations';
}
