<?php

namespace Modules\Programs\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Page\Entities\Page;
use Modules\Programs\Entities\Traits\Activateable;

class Type extends Model
{
    use Translatable, Activateable;

    protected $table = 'programs__types';
    public $translatedAttributes = [
        'name',
        'program_description',
        'excursion_description'
    ];
    protected $fillable = [
        'name',
        'active',
        'program_description',
        'excursion_description',
        'page_id'
    ];

    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    protected function format($value)
    {
        $text = str_replace("\n", '</p><p>', $value);

        return '<p>' . $text . '</p>';
    }

    public function getExcursionFormattedAttribute()
    {
        return $this->format($this['excursion_description']);
    }

    public function getProgramFormattedAttribute()
    {
        return $this->format($this['program_description']);
    }
}
