<?php

namespace Modules\Programs\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Programs\Entities\Traits\Featurable;
use Modules\Programs\Entities\Traits\Typeable;

class Excursion extends Model
{
    use Translatable, Featurable, Typeable, MediaRelation;

    protected $table = 'programs__excursions';
    public $translatedAttributes = [
        'name',
        'description',
        'guide',
        'max_group',
        'phone',
        'price',
        'guide_caption',
        'category',
        'order'
    ];
    protected $fillable = [
        'name',
        'description',
        'max_group',
        'time',
        'duration',
        'phone',
        'guide',
        'dates',
        'price',
        'type_id',
        'category',
        'guide_caption'
    ];
    public $casts = [
        'dates' => 'array'
    ];

    public function getDatesOrderedAttribute()
    {
        return collect($this['dates'])->sort()->values()->all();
    }

    public function getFormDatesAttribute()
    {
        return $this['dates'] ? array_combine($this['dates'], $this['dates']) : [];
    }

    public function getFormattedTextAttribute()
    {
        $text = str_replace("\n", '</p><p>', $this['description']);

        return '<p>' . $text . '</p>';
    }

    public function images()
    {
        return $this->files()->where('zone', 'excursionImage');
    }

    public function pdf()
    {
        return $this->files()->where('zone', 'excursionPdf');
    }
}
