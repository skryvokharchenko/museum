<?php


namespace Modules\Programs\Entities\Traits;


use Modules\Programs\Entities\Type;

trait Typeable
{
    public function type()
    {
        return $this->belongsTo(Type::class);
    }
}
