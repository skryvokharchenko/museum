<?php


namespace Modules\Programs\Entities\Traits;


use Modules\Programs\Entities\Feature;

trait Featurable
{
    public function features()
    {
        return $this->morphToMany(Feature::class, 'featurable', 'programs__featureables');
    }
}
