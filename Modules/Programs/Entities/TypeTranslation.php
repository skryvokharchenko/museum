<?php

namespace Modules\Programs\Entities;

use Illuminate\Database\Eloquent\Model;

class TypeTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'program_description',
        'excursion_description'
    ];
    protected $table = 'programs__type_translations';
}
