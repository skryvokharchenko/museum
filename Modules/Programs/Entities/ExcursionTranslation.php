<?php

namespace Modules\Programs\Entities;

use Illuminate\Database\Eloquent\Model;

class ExcursionTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'guide',
        'max_group',
        'phone',
        'price',
        'guide_caption',
        'category'
    ];
    protected $table = 'programs__excursion_translations';
}
