<?php

namespace Modules\Programs\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Programs\Entities\Traits\Activateable;

class Feature extends Model
{
    use Translatable, Activateable, MediaRelation;

    protected $table = 'programs__features';
    public $translatedAttributes = ['name'];
    protected $fillable = [
        'name',
        'active'
    ];

    public function programs()
    {
        return $this->morphedByMany(Program::class, 'featureable', 'programs__featureables');
    }

    public function excursions()
    {
        return $this->morphedByMany(Excursion::class, 'featureable', 'programs__featureables');
    }

    public function getIconAttribute()
    {
        return $this->files()->first() ? $this->files()->first()->path : '';
    }
}
