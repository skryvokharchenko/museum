<?php

namespace Modules\Programs\Entities;

use Illuminate\Database\Eloquent\Model;

class FeatureTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $table = 'programs__feature_translations';
}
