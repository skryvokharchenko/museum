<?php


namespace Modules\Programs\Repositories\Eloquent;


use Modules\Programs\Events\EntityWasCreatedOrUpdated;

trait Mediable
{
    public function create($data)
    {
        $item = parent::create($data);
        if (isset($data['features'])) {
            $item->features()->sync($data['features']);
        }
        event(new EntityWasCreatedOrUpdated($item, $data));
        return $item;
    }

    public function update($model, $data)
    {
        $item = parent::update($model, $data);
        if (isset($data['features'])) {
            $item->features()->sync($data['features']);
        }
        event(new EntityWasCreatedOrUpdated($item, $data));
        return $item;
    }
}
