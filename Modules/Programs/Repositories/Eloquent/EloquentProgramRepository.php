<?php

namespace Modules\Programs\Repositories\Eloquent;

use Modules\Programs\Repositories\ProgramRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentProgramRepository extends EloquentBaseRepository implements ProgramRepository
{
    use Mediable;
}
