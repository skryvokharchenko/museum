<?php

namespace Modules\Programs\Repositories\Eloquent;

use Modules\Programs\Repositories\ExcursionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentExcursionRepository extends EloquentBaseRepository implements ExcursionRepository
{
    use Mediable;
}
