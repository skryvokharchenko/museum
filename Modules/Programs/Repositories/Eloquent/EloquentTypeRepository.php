<?php

namespace Modules\Programs\Repositories\Eloquent;

use Modules\Programs\Repositories\TypeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTypeRepository extends EloquentBaseRepository implements TypeRepository
{
}
