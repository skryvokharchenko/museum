<?php

namespace Modules\Programs\Repositories\Eloquent;

use Modules\Programs\Repositories\FeatureRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentFeatureRepository extends EloquentBaseRepository implements FeatureRepository
{
    use Mediable;
}
