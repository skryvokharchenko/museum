<?php

namespace Modules\Programs\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface TypeRepository extends BaseRepository
{
}
