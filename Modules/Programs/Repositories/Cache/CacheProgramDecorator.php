<?php

namespace Modules\Programs\Repositories\Cache;

use Modules\Programs\Repositories\ProgramRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheProgramDecorator extends BaseCacheDecorator implements ProgramRepository
{
    public function __construct(ProgramRepository $program)
    {
        parent::__construct();
        $this->entityName = 'programs.programs';
        $this->repository = $program;
    }
}
