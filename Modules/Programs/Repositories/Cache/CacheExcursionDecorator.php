<?php

namespace Modules\Programs\Repositories\Cache;

use Modules\Programs\Repositories\ExcursionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheExcursionDecorator extends BaseCacheDecorator implements ExcursionRepository
{
    public function __construct(ExcursionRepository $excursion)
    {
        parent::__construct();
        $this->entityName = 'programs.excursions';
        $this->repository = $excursion;
    }
}
