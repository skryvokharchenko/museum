<?php

namespace Modules\Programs\Repositories\Cache;

use Modules\Programs\Repositories\FeatureRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheFeatureDecorator extends BaseCacheDecorator implements FeatureRepository
{
    public function __construct(FeatureRepository $feature)
    {
        parent::__construct();
        $this->entityName = 'programs.features';
        $this->repository = $feature;
    }
}
