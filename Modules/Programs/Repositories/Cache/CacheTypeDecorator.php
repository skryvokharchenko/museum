<?php

namespace Modules\Programs\Repositories\Cache;

use Modules\Programs\Repositories\TypeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTypeDecorator extends BaseCacheDecorator implements TypeRepository
{
    public function __construct(TypeRepository $type)
    {
        parent::__construct();
        $this->entityName = 'programs.types';
        $this->repository = $type;
    }
}
