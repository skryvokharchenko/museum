<?php

namespace Modules\Programs\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Programs\Events\Handlers\RegisterProgramsSidebar;

class ProgramsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterProgramsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('programs', array_dot(trans('programs::programs')));
            $event->load('excursions', array_dot(trans('programs::excursions')));
            $event->load('types', array_dot(trans('programs::types')));
            $event->load('features', array_dot(trans('programs::features')));
            // append translations




        });
    }

    public function boot()
    {
        $this->publishConfig('programs', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Programs\Repositories\ProgramRepository',
            function () {
                $repository = new \Modules\Programs\Repositories\Eloquent\EloquentProgramRepository(new \Modules\Programs\Entities\Program());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Programs\Repositories\Cache\CacheProgramDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Programs\Repositories\ExcursionRepository',
            function () {
                $repository = new \Modules\Programs\Repositories\Eloquent\EloquentExcursionRepository(new \Modules\Programs\Entities\Excursion());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Programs\Repositories\Cache\CacheExcursionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Programs\Repositories\TypeRepository',
            function () {
                $repository = new \Modules\Programs\Repositories\Eloquent\EloquentTypeRepository(new \Modules\Programs\Entities\Type());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Programs\Repositories\Cache\CacheTypeDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Programs\Repositories\FeatureRepository',
            function () {
                $repository = new \Modules\Programs\Repositories\Eloquent\EloquentFeatureRepository(new \Modules\Programs\Entities\Feature());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Programs\Repositories\Cache\CacheFeatureDecorator($repository);
            }
        );
// add bindings




    }
}
