<?php

return [
    'week' => [
        'mo' => 'MO',
        'tu' => 'TU',
        'we' => 'WE',
        'th' => 'TH',
        'fr' => 'FR',
        'sa' => 'SA',
        'su' => 'SU',
        'days off' => 'DAYS OFF',
        'work days' => '(Open: Wednesday - Sunday, 10:30 - 17:30)'
    ],
    'logo class' => 'icon-logo_site-en',
    'color' => [
        'title' => 'Website color scheme configuration',
        'name' => 'Color scheme',
        'sepia' => 'sepia',
        'black-white' => 'black on white',
        'black-yellow' => 'black on yellow',
        'blue-yellow' => 'blue on yellow',
        'green-white' => 'green on white',
        'inverted' => 'inverted',
        'white-black' => 'white on black',
        'yellow-black' => 'yellow on black',
        'yellow-blue' => 'yellow on blue',
        'white-green' => 'white on green',
        'select' => 'Select Color',
        'select text color' => 'Select text color',
        'select back color' => 'Select background color'
    ],
    'font' => [
        'size' => 'Fonts size',

    ],
    'form' => [
        'subject' => 'Subject',
        'message' => 'Message',
        'send' => 'Send'
    ],
    'show route description' => 'Show route description',
    'address' => 'Address',
    'address value' => 'Ukraine, Kyiv, Tereshchenkivska St., 15-17',
    'address first line' => 'Ukraine, Kyiv,',
    'address second line' => 'Tereshchenkivska St., 15-17',
    'phone' => 'Phone',
    'open' => 'Open',
    'closed' => 'Closed',
    'skip navigation' => 'Skip navigation',
    'search' => 'Search',
    'search input' => 'Search input',
    'read more' => 'read more',
    'hide' => 'hide',
    'write us' => 'Write us',
    'reception phones' => 'Reception phone numbers',
    'reception' => 'reception',
    'for press' => 'for press',
    '3d tour' => '3D tour',
    'filter by' => 'Filter By',
    'apply' => 'APPLY',
    'collection' => [
        'section' => 'Section of the collection',
        'place' => 'Place of origin',
        'type' => 'Type / material',
        'date' => 'Date'
    ],
    'email' => [
        'success' => 'Your request has been sent successfully.',
        'full_name' => 'Full Name',
        'name' => 'Your name',
        'phone' => 'Phone number',
        'comment' => 'Comment',
        'question' => 'Your comment or question',
        'email' => 'Your Email',
        'notice' => 'specify number of persons, convenient date and time, etc '
    ]
];
