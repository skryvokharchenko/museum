// getCookie(), setCookie(), deleteCookie()


// возвращает cookie если есть или undefined
function getCookie(name) {

	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined
}

// уcтанавливает cookie
function setCookie(name, value, props) {

	props = props || {};
	var exp = props.expires;

	if (typeof exp == "number" && exp) {
		var d = new Date();
		d.setTime(d.getTime() + exp*1000);
		exp = props.expires = d
	}

	if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

	value = encodeURIComponent(value);
	var updatedCookie = name + "=" + value;

	for(var propName in props){
		updatedCookie += "; " + propName;
		var propValue = props[propName];
		if(propValue !== true){ updatedCookie += "=" + propValue }
	}
	document.cookie = updatedCookie
}

// удаляет cookie
function deleteCookie(name) {
	setCookie(name, null, { expires: -1 })
}

$(function(){


	$('#colorSelector').ColorPicker({
		color: getColorSchema().bgColor || '#0000ff',
		onShow: function (colpkr) {
			$(colpkr).show(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).hide(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			setColorSchema(null, '#' + hex, null, false);
			applyColorSchema();
		}
	});

	$('#colorSelector1').ColorPicker({
		color: getColorSchema().fontColor || '#0000ff',
		onShow: function (colpkr) {
			$(colpkr).show(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).hide(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			setColorSchema('#' + hex, null, null, false);
			applyColorSchema();
		}
	});

	applyColorSchema();
	applyFontSize();

	//tabs
	$("#show_tabs").hide();

	$("#show_button").on("click",function(event){
		event.preventDefault();

		$("#show_tabs").toggle();

	});

	$('[data-sort]').on('click', function () {
  		var hig = $(this).height();

  		$(this).data('offset',hig + 24)
	});

	//data_more

	$('[data-more]').on('click', function (e) {
		e.preventDefault();

  		$(this).hide();
  		$(this).parent().toggleClass('active');

  		$('[data-bgmore]').eq($(this).index('[data-more]')).toggleClass('active');

  		$('[data-hide]').eq($(this).index('[data-more]')).parent().show();

	});

	$('[data-hide]').on('click', function (e) {
		e.preventDefault();

  		$(this).parent().hide();

  		$('[data-bgmore]').eq($(this).index('[data-hide]')).toggleClass('active');

  		$('[data-more]').eq($(this).index('[data-hide]')).show();
  		$('[data-more]').eq($(this).index('[data-hide]')).parent().toggleClass('active');


	});




	//door
	var work_day = [3,4,5,6,0],
		work_hour = [10,11,12,13,14,15,16,17];

	door(work_day,work_hour);

	window.setInterval(function(){
		door(work_day,work_hour);

	}, 300000);

	function door(day, hour){

		var D = new Date(),
			hour_now = D.getHours(),
			day_now = D.getUTCDay(),
			minutes_now = D.getMinutes(),
			work = false;

		if (day.indexOf(day_now) !== -1) {
			var hour_idx = hour.indexOf(hour_now);


			if (hour_idx !== -1) {
                if (hour_idx === 0 && minutes_now < 30) {
                    work = false;
                } else work = !(hour_idx === hour.length - 1 && minutes_now > 30);
			}
		}

			if (work) {
				$('.work-closed').hide().attr('aria-hidden', true);
				$('.work-open').show().removeAttr('aria-hidden');
				$("#icon_door").addClass('icon-door');
				$("#icon_door").removeClass('icon-door_close');

			}else{
				$('.work-open').hide().attr('aria-hidden', true);
				$('.work-closed').show().removeAttr('aria-hidden');
				$("#icon_door").addClass('icon-door_close');
				$("#icon_door").removeClass('icon-door');
			}


	}


	//nav
	$("#nav_toggle").on("click",function(event){
		event.preventDefault();

		$(this).toggleClass("active");
		$(this).removeClass("view");
		$('nav').toggleClass("fixed");
		$('#search').toggleClass("fixed");

		$('.lang_container_adaptive').css('top', $('nav').innerHeight() - 1);
		$('#lang_cont').toggleClass("fixed");
		$('.lang_container_adaptive').toggleClass("fixed");

		$("#button_view").removeClass('active');
		$("#view_container").removeClass('active');
		$("#header_container").removeClass('view');

	});

	$(window).on('resize', function(){

		setTimeout(setlang, 200);

	});

	function setlang(){
		if ($('.lang_container_adaptive').hasClass('fixed')) {

			$('.lang_container_adaptive').css('top', $('nav').innerHeight() - 1);
			console.log('1');
		}
	};

	//btn_view

	$("#button_view").on("click",function(event){
		event.preventDefault();

		var head_top = $(".header_top").height();
		head_top = head_top + 18 ;

		$(this).toggleClass('active');
		$("#view_container").toggleClass('active');
		$("#header_container").toggleClass('view');

		$("#nav_toggle").removeClass("active");
		$("#nav_toggle").addClass("view");
		$('nav').removeClass("fixed");

		$("#view_container").css('transform', 'translate(0,' + head_top + 'px)');

	});


    $("#button_close").on("click",function(event){
        event.preventDefault();

        var head_top = $(".header_top").height();
        head_top = head_top + 18 ;

        $("#button_view").toggleClass('active');
        $("#view_container").toggleClass('active');
        $("#header_container").toggleClass('view');

        $("#nav_toggle").removeClass("active");
        $("#nav_toggle").addClass("view");
        $('nav').removeClass("fixed");

        $("#view_container").css('transform', 'translate(0,' + head_top + 'px)');
    });

	$("#button_reload").on("click",function(event){
		event.preventDefault();

		resetColorSchema();
		$("html").css('font-size',"18");

		deleteCookie('color_schema');
		deleteCookie('font_size');

		applyFontSize();

	});


	$("[data-font]").on("click",function(event){
		event.preventDefault();

		var font_size = $(this).data("font");
		setCookie('font_size', font_size);
		applyFontSize();

	});

	function applyFontSize()
	{
		var font = getCookie('font_size');

		$("[data-font]").removeClass('active');
		if (font) {
			$('[data-font="' + font + '"]').addClass('active');
			$("html").css('font-size', parseInt(font));

			var head_top = $(".header_top").height();
			head_top = head_top + 18 ;
			$("#view_container").css('transform', 'translate(0,' + head_top + 'px)');
		} else {
			$("[data-font]").eq(0).addClass('active');
		}

	}

	function setColorSchema(fontcolor, background, filter, inversion)
	{
		var schema = {
			'fontColor': fontcolor,
			'bgColor': background,
			'imgFilter': filter,
			'inversion': inversion
		};

		setCookie('color_schema', JSON.stringify(schema));
	}

	function getColorSchema()
	{
		return JSON.parse(getCookie('color_schema') || null) || {};
	}

	function applyColorSchema()
	{
		var schema = getColorSchema();

		if (schema && !schema.inversion) {
			$("html").css('filter', 'invert(0%)');
			if (schema.imgFilter) {
				$("img").css('filter', schema.imgFilter);
			}

			if (schema.fontColor) {
				$(".font_color").css('color', schema.fontColor);
				$(".font_color").css('border-color', schema.fontColor);
				$("option").css('color', schema.fontColor);
				$("select").css('color', schema.fontColor);
				$(".nav_toggle_item").css('background', schema.fontColor);
				$(".font_color").children('span').css('color', schema.fontColor);
			}

			if (schema.bgColor) {
				$(".bg_color").css('background', schema.bgColor);
				$("option").css('background', schema.bgColor);
				$('#colorSelector div').css('backgroundColor', schema.bgColor);
			}
		} else if (schema.inversion) {
			resetColorSchema();
			$("html").css('filter', 'invert(100%)');
		}
	}

    //dropdown

    var dropri = false;
    function mouseDropMenu(object)
	{
		if ($("html").innerWidth() > 991) {
			if (object.parents('.toggled').length === 0) {
                $('.dropdown-menu').hide().removeClass('toggled');
			} else {
				object.parents('.toggled').find('.dropdown-menu').hide().removeClass('toggled');
			}

			var target = '#' + object.data('target');
            $(target).show().addClass('toggled');
        }
	}

	function mouseDropMenuHide()
	{
		if (($("html").innerWidth() > 991) && drop_active == false) {
			$('.dropdown-menu').hide().removeClass('toggled');
        }
	}



	$("nav [dropdown-toggle]").on("mouseout",function(event){
        event.preventDefault();

        var tab_ = $(this);


		setTimeout(function () {
			mouseDropMenuHide();
		}, 500);

    });


    $("nav [dropdown-toggle]").on("mouseover",function(event){
        event.preventDefault();

        var tab_ = $(this);


		setTimeout(function () {
			mouseDropMenu(tab_);
			console.log('1');
		}, 500);

    });

    var drop_active = false;

    $("nav .dropdown-menu").on("mouseover",function(event){
        event.preventDefault();

        drop_active = true;


    });
     $("nav .dropdown-menu").on("mouseout",function(event){
        event.preventDefault();

        drop_active = false;

	   	setTimeout(function () {
			mouseDropMenuHide();
		}, 500);


    });


	// $(".dropdown-item").on("click",function(event){
	// 	event.preventDefault();
	//
	// 	if ($("html").innerWidth() > 991) {
	//
	// 		window.location = $(this).attr('href');
	//
	// 	}
	//
	// 	if ($("html").innerWidth() < 991 && $(this).next('div').is(':visible') ) {
	//
	// 		window.location = $(this).attr('href');
	//
	// 	}
	// });



    $("nav [dropdown-toggle]").on("click",function(event){
        event.preventDefault();

        if ($("html").innerWidth() > 991) {
            if ($(this).parent().hasClass('dropdown')) {
                window.location = $(this).attr('href');
            }
            if ($(this).parent().hasClass('dropright')) {
                window.location = $(this).attr('href');
            }
            if ($(this).parent().parent().hasClass('dropright')) {
                window.location = $(this).attr('href');
            }
        }

        var obg = $(this);

		if ($("html").innerWidth() < 991) {

			if (obg.hasClass('open')) {
                window.location = obg.attr('href');
			}

			var target = '#' + obg.data('target');
			$(target).show();
			obg.addClass('open');
        }

    });



	function resetColorSchema()
	{
		$("html").removeAttr('style');
		$("img").removeAttr('style');
		$(".font_color").removeAttr('style');
		$(".font_color").removeAttr('style');
		$("option").removeAttr('style');
		$("select").removeAttr('style');
		$(".nav_toggle_item").removeAttr('style');
		$(".font_color").children('span').removeAttr('style');
		$(".bg_color").removeAttr('style');
		$("option").removeAttr('style');
	}

	$("#color1").on("click",function(event){
		event.preventDefault();

		setColorSchema('#773D08','#F0D3A8', 'sepia(100%)', false);
		applyColorSchema();
	});

	$("#color2").on("click",function(event){
		event.preventDefault();

		setColorSchema('#34404F','#FFFFFF', 'greyscale(100%)', false);
		applyColorSchema();
	});

	$("#color3").on("click",function(event){
		event.preventDefault();

		setColorSchema('#333','#FFC400', 'inherit', false);
		applyColorSchema();
	});

	$("#color4").on("click",function(event){
		event.preventDefault();

		setColorSchema('rgba(28, 38, 87, 0.84)','#FFC400', 'inherit', false);
		applyColorSchema();
	});

	$("#color5").on("click",function(event){
		event.preventDefault();

		setColorSchema('#138B0F','#FFFFFF', 'inherit', false);
		applyColorSchema();
	});

	$("#color6").on("click",function(event){
		event.preventDefault();

		setColorSchema(null, null, null, true);
		applyColorSchema();
	});

	$("#color7").on("click",function(event){
		event.preventDefault();

		setColorSchema('#FFFCFC','#000000', 'grayscale(100%)', false);
		applyColorSchema();
	});

	$("#color8").on("click",function(event){
		event.preventDefault();

		setColorSchema('#FFC400','#000000', 'inherit', false);
		applyColorSchema();
	});

	$("#color9").on("click",function(event){
		event.preventDefault();

		setColorSchema('#FFC400','rgba(28, 38, 87, 1)', 'inherit', false);
		applyColorSchema();
	});

	$("#color10").on("click",function(event){
		event.preventDefault();

		setColorSchema('#fff','#138B0F', 'inherit', false);
		applyColorSchema();
	});

	//flag
	if ($("#lang").val() == "uk") {
		$("#flag_container").addClass('type1');
	} else{
		$("#flag_container").removeClass('type1');
	}


	$("#lang").on("change",function(event){
		event.preventDefault();

		if ($(this).val() == "uk") {
			$("#flag_container").addClass('type1');

		} else{
			$("#flag_container").removeClass('type1');
		}


	});

	//slider_play_pause


	$("#btn_pause").on("click",function(event){
		event.preventDefault();

		$(this).toggleClass("active");
		$("#btn_play").toggleClass("active");

		$('#slider').slick("slickPause");

	});

	$("#btn_play").on("click",function(event){
		event.preventDefault();

		$(this).toggleClass("active");
		$("#btn_pause").toggleClass("active");

		$('#slider').slick("slickPlay");



	});

	$("[data-pause]").on("click",function(event){
		event.preventDefault();

		$(this).toggleClass("active");

		$(this).parent().children('.btn_play').toggleClass("active");

		$(this).parent().parent().children().slick("slickPause");

	});

	$("[data-play]").on("click",function(event){
		event.preventDefault();

		$(this).toggleClass("active");

		$(this).parent().children('.btn_pause').toggleClass("active");


		$(this).parent().parent().children().slick("slickPlay");
	});

	//slider

	$('#main_slider').slick({
		arrows: true,
		prevArrow: "<button type='button' title='попередній слайд' aria-label='попередній слайд' class='btn-prev arrow_btn type1 bg_color font_color'><span class='icon-arrow font_color'></span></button>",
		nextArrow: "<button type='button' title='наступний слайд' aria-label='наступний слайд' class='btn-next arrow_btn type1 bg_color font_color'><span class='icon-arrow font_color'></span></button>",
		infinite: true,
		slidesToShow: 1,
		responsive: [
			{
			breakpoint: 575,
			settings: {
			arrows: true,
			slidesToShow: 1
			}
			}
		]
	});

	$('#slider').on('init', function() {
		$('.slider_container').removeClass('nowrap');
	});

	$('#slider').slick({
		centerMode: true,
		arrows: true,
		appendArrows: ".slider_btn_container",
		prevArrow: "<button type='button' title='попередній слайд' aria-label='попередній слайд' class='btn-prev arrow_btn bg_color font_color'><span class='icon-arrow font_color'></span></button>",
		nextArrow: "<button type='button' title='наступний слайд' aria-label='наступний слайд' class='btn-next arrow_btn bg_color font_color'><span class='icon-arrow font_color'></span></button>",
		infinite: true,
		centerPadding: '12%',
		slidesToShow: 1,
		focusOnSelect: true,
		autoplay: false,
		autoplaySpeed: 3000,
		responsive: [
			{
			breakpoint: 575,
			settings: {
			centerMode: true,
			arrows: true,
			infinite: true,
			centerPadding: '10%',
			slidesToShow: 1,
			focusOnSelect: true,
			autoplay: false,
			autoplaySpeed: 3000,
			}
			}
		]
	});

	$('#partners_slider').slick({
		dots: false,
		infinite: false,
		speed: 300,
		prevArrow: "<button type='button' title='попередній слайд' aria-label='попередній слайд' class='btn-prev arrow_btn bg_color font_color'><span class='icon-arrow font_color'></span></button>",
		nextArrow: "<button type='button' title='наступний слайд' aria-label='наступний слайд' class='btn-next arrow_btn bg_color font_color'><span class='icon-arrow font_color'></span></button>",
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [
			{
				breakpoint: 1424,
				settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true
			}
			},
			{
				breakpoint: 991,
				settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
			},
			{
				breakpoint: 767,
				settings: {
				centerMode: true,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				centerPadding: '12%',
				slidesToShow: 1,
				autoplay: false,
				focusOnSelect: true,
			}
			}
		]
	});

	var index2 = 0;

	$('[data-bigslider]').each(function(index, el) {

		var data_cont = $('.slider_btn_container').eq(index2);

			$(this).slick({
			centerMode: true,
			arrows: true,
			appendArrows: data_cont,
			prevArrow: "<button type='button' title='попередній слайд' aria-label='попередній слайд' class='btn-prev arrow_btn bg_color font_color'><span class='icon-arrow font_color'></span></button>",
			nextArrow: "<button type='button' title='наступний слайд' aria-label='наступний слайд' class='btn-next arrow_btn bg_color font_color'><span class='icon-arrow font_color'></span></button>",
			infinite: true,
			centerPadding: '12%',
			slidesToShow: 1,
			autoplay: false,
			autoplaySpeed: 3000,
			responsive: [
				{
				breakpoint: 575,
				settings: {
				centerMode: true,
				arrows: true,
				infinite: true,
				centerPadding: '10%',
				slidesToShow: 1,
				autoplay: false,
				autoplaySpeed: 3000,
				}
				}
			]
		});

		index2++;
	});

	var index1 = 0;

	$('[data-slider]').each(function(index, el) {

		var data_cont = $('.slider_btn_container').eq(index1);

			$(this).slick({
			arrows: true,
			appendArrows: data_cont,
			prevArrow: "<button type='button' title='попередній слайд' aria-label='попередній слайд' class='btn-prev arrow_btn bg_color font_color'><span class='icon-arrow font_color'></span></button>",
			nextArrow: "<button type='button' title='наступний слайд' aria-label='наступний слайд' class='btn-next arrow_btn bg_color font_color'><span class='icon-arrow font_color'></span></button>",
			infinite: true,
			slidesToShow: 1,
			autoplay: false,
			autoplaySpeed: 3000,
			responsive: [
				{
				breakpoint: 575,
				settings: {
				arrows: true,
				autoplay: false,
				autoplaySpeed: 3000
				}
				}
			]
		});

		index1++;
	});

	$('.email-modal form, .contact-form').on('submit', function () {
        let $form = $(this);
        event.preventDefault();
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            dataType: 'JSON',
            success: function (html) {
				$form.find('.alert-success').show(200);
				setTimeout(function () {
                    $form.find('.alert-success').hide(200);
                    $form[0].reset();

                    if ($form.parents('.modal').length > 0) {
                        $form.parents('.modal').modal('hide');
                    }
				}, 3000)
            },
            error: function (xhr) {

            }
        });
	});


});




