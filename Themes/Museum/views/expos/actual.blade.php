@extends('layouts.master')

@section('content')
    <div class="main_container type1 font_color bg_color" id="content">
        <div class="way_list_container font_color bg_color">
            <div class="way_list font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title font_color">
                <h1>{{$page->title}}</h1>
            </div>

            <div class="work_text mod1 font_color">
                {!! $page->body !!}
            </div>
        </div>
    </div>

    {!! Expo::actual() !!}

    <section><!--column-->
        <div class="column_container exhi_column font_color bg_color">
            <div class="container_section">
                <div class="row justify-content-center">
                    @include('expos.links')
                </div>
            </div>
        </div>
    </section><!--column-->
@endsection
