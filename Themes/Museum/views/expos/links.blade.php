<div class="col-sm-12 col-md-6 col-xl-4">
    <a href="{!! route('page', Slug::page(9)->slug) !!}" aria-label="Актуальні виставки">
        <div class="column_foto">
            <img src="{!! Slug::page(9)->image !!}" alt="">
        </div>
        <div class="column_info font_color bg_color">
            <p>{!! Slug::page(9)->title !!}</p>
        </div>
    </a>
</div>
<div class="col-sm-12 col-md-6 col-xl-4">
    <a href="{!! route('page', Slug::page(10)->slug) !!}" aria-label="Майбутні виставки">
        <div class="column_foto">
            <img src="{!! Slug::page(10)->image !!}" alt="">
        </div>
        <div class="column_info font_color bg_color">
            <p>{{ Slug::page(10)->title }}</p>
        </div>
    </a>
</div>
<div class="col-sm-12 col-md-6 col-xl-4">
    <a href="{!! route('page', Slug::page(12)->slug) !!}" aria-label="Архів">
        <div class="column_foto">
            <img src="{!! Slug::page(12)->image !!}" alt="">
        </div>
        <div class="column_info font_color bg_color">
            <p>{{Slug::page(12)->title}}</p>
        </div>
    </a>
</div>
