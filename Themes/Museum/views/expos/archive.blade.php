@extends('layouts.master')

@section('content')
    <div class="main_container action_main type1 font_color bg_color" id="content">
        <div class="way_list_container font_color bg_color">
            <div class="way_list font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title font_color">
                <h1>{{$page->title}}</h1>
            </div>

            <div class="work_text mod1 font_color">
                {!! $page->body !!}
            </div>
        </div>
    </div>

    {!! Expo::archive() !!}
@endsection
