@if($data['type'] == 'request')
    <p>{{$data['replyTo']['name']}} надіслав запит @if($data['program'])щодо програми {{$data['program']->name}} @endif:</p>
@else
    <p>{{$data['replyTo']['name']}} надіслав коментар:</p>
@endif
<blockquote>
    {!! $data['body'] !!}
</blockquote>

@if($data['phone'])
    <p>Телефон: <strong>{{$data['phone']}}</strong></p>
@endif
