@extends('layouts.master')

@section('content')
    <div class="main_container type1 font_color bg_color" id="content">
        <div class="way_list_container font_color bg_color">
            <div class="way_list font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title font_color">
                <h1>{{ $page->title }}</h1>
            </div>
        </div>

        <section>
            <div class="cost_section font_color bg_color">
                <div class="content_container">
                    <div class="cost_section_title">
                        <h2>{{ $parts->firstWhere('system_name', 'museum-entry')->title }}</h2>
                    </div>
                    {!! Pricing::entry() !!}
                    <div class="sub_cost_item font_color">
                        {!! $parts->firstWhere('system_name', 'museum-entry')->body !!}
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="free_entry_container">
                <div class="content_container">
                    <div class="free_entry_title font_color">
                        <h2>{{ $parts->firstWhere('system_name', 'free-entry')->title }}</h2>
                    </div>
                    {!! $parts->firstWhere('system_name', 'free-entry')->body !!}
                </div>
            </div>
        </section>

        <section>
            <div class="cost_section type2 font_color bg_color">
                <div class="content_container">
                    {!! Pricing::tours() !!}
                </div>
            </div>
        </section>

    </div>
@stop
