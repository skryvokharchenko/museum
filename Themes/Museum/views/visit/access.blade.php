@extends('layouts.master')

@section('content')
    <section>
        <div class="main_container type1 access_main font_color bg_color" id="content">
            <div class="way_list_container font_color bg_color">
                <div class="way_list font_color bg_color" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
            <div class="content_container">
                <div class="work_title font_color">
                    <h1>{{ $page->title }}</h1>
                </div>

                <div class="work_text type_access font_color">
                    {!! $page->body !!}
                </div>
            </div>
        </div>
    </section>

    <section><!--column-->
        <div class="column_container font_color bg_color">
            <div class="container_section">
                <div class="row justify-content-center">
                    <div class="col-sm-12 col-md-6 col-xl-4">
                        <a href="{!! route('page', Slug::page(27)->slug) !!}">
                            <div class="column_foto">
                                <img src="{!! Slug::page(27)->image !!}" alt="">
                            </div>
                            <div class="column_info font_color bg_color">
                                <p>{{ Slug::page(27)->title }}</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-4">
                        <a href="{!! route('page', Slug::page(33)->slug) !!}">
                            <div class="column_foto">
                                <img src="{!! Slug::page(33)->image !!}" alt="">
                            </div>
                            <div class="column_info font_color bg_color">
                                <p>{{ Slug::page(33)->title }}</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-4">
                        <a href="{!! route('page', Slug::page(4)->slug) !!}">
                            <div class="column_foto">
                                <img src="{!! Slug::page(4)->image !!}" alt="">
                            </div>
                            <div class="column_info font_color bg_color">
                                <p>{{ Slug::page(4)->title }}</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section><!--column-->

    <section><!--text and video-->
        <div class="text_video_container bg_color">
            <div class="content_container">
                <div class="work_text font_color">
                    {!! $parts->firstWhere('system_name', 'access-content')->body !!}
                </div>

                <div class="video_container">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/meNjhZcRCGM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section><!--text and video-->
@stop
