@extends('layouts.master')

@section('content')
    <section>
        <div class="main_container type1 adress_cont font_color bg_color" id="content">
            <div class="way_list_container font_color bg_color">
                <div class="way_list font_color bg_color" aria-label="ви знаходитесь тут" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
            <div class="content_container">
                <div class="work_title font_color">
                    <h1>{{$page->title}}</h1>
                </div>

                <div class="adress_container">
                    <div class="adress_body font_color bg_color">
                        <div class="adress_title font_color">
                            <span class="icon-geo_top"></span>
                            <h2>{{trans('front.address')}}</h2>
                        </div>
                        <p>{{ trans('front.address value') }}</p>
                    </div>
                </div>


            </div>
        </div>
    </section>

    <section><!--map-->
        <div class="map_container">
            <iframe id="mapFrame" src="https://www.google.com/maps/d/embed?mid=1JaD-cec24GGbEFOSsWm5LGHDKoqJVgS_" width="100%" height="100%" title="Мапа від Google" aria-label="Мапа від Google"></iframe>
        </div>
    </section><!--map-->

    <section>
        <div class="adress_bottom_container font_color bg_color">
            <div class="content_container">

                <div class="section_button">
                    <a href="#" role="button" class="button_show font_color bg_color" aria-label="Показати текстовий опис проїзду" title="Показати текстовий опис проїзду" id="show_button">{{ trans('front.show route description') }}</a>
                </div>

                <div class="work_text font_color" id="show_tabs">
                    {!! $page->body !!}
                </div>

                <div class="video_container">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/meNjhZcRCGM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
@stop

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#mapFrame').addClass('scrolloff');                // set the mouse events to none when doc is ready

            $('.map_container').on("mouseup",function(){          // lock it when mouse up
                $('#mapFrame').addClass('scrolloff');
                //somehow the mouseup event doesn't get call...
            });
            $('.map_container').on("mousedown",function(){        // when mouse down, set the mouse events free
                $('#mapFrame').removeClass('scrolloff');
            });
            $("#mapFrame").mouseleave(function () {              // becuase the mouse up doesn't work...
                $('#mapFrame').addClass('scrolloff');            // set the pointer events to none when mouse leaves the map area
                                                            // or you can do it on some other event
            });

        });
    </script>
    @endpush