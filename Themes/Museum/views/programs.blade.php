@extends('layouts.master')

@section('content')
    <div class="main_container type1 font_color bg_color" id="content">
        <div class="way_list_container font_color bg_color">
            <div class="way_list font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
                {{--<a href="#" class="way_list_link" aria-label="розділ сайту">Програми<span class="icon-arrow_down"></span></a>--}}
                {{--<a href="#" class="way_list_link" aria-label="сторінка"><p>{{ $page->title }}</p></a>--}}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title programs_title font_color">
                <h1>{{ $page->title }}</h1>
            </div>

            <div class="work_text mod1 font_color">
                <p>{!! $page->body !!}</p>

                <div class="programs_btn_container">
                    <a href="#" role="button" data-toggle="modal" data-target="#modal1" class="programs_btn font_color" aria-label="{{ trans('programs::programs.frontend.leave_comment') }}">{{ trans('programs::programs.frontend.leave_comment') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Programs::show($page->id) !!}
    <div class="modal fade email-modal" tabindex="-1" role="dialog" id="modal1" aria-labelledby="{{trans('programs::programs.frontend.leave_comment')}}" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content bg_color">

                <div class="modal-header font_color">
                    <h5 class="modal-title" id="exampleModalLabel">{{trans('programs::programs.frontend.leave_comment')}}</h5>
                </div>

                <button type="button" class="close btn_modal" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross btn_modal_close font_color"></span>
                </button>

                <div class="modal-body font_color">
                    <form action="/email/comment/send">
                        <div class="alert alert-success" style="display: none;" role="alert">
                            {{ trans('front.email.success') }}
                        </div>
                        <input required type="text" name="name" class="modal_text font_color" aria-label="{{ trans('front.email.name') }}" placeholder="{{ trans('front.email.name')  }}">
                        <input required type="email" name="email" class="modal_text font_color" aria-label="{{ trans('front.email.email')  }}" placeholder="{{ trans('front.email.email')  }}">
                        <textarea required name="comment" name="comment" class="modal_comment font_color" id="comment" cols="30" rows="10" placeholder="{{ trans('front.email.question')  }}"></textarea>
                        <input type="submit" value="{{ trans('front.form.send') }}" role="button" class="modal_submit programs_btn font_color">
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade email-modal" tabindex="-1" role="dialog" id="modal2" aria-labelledby="{{trans('programs::programs.frontend.leave_request')}}" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content bg_color">

                <div class="modal-header font_color">
                    <h5 class="modal-title" id="exampleModalLabel">{{trans('programs::programs.frontend.leave_request')}}</h5>
                    <p>({{trans('front.email.notice')}})</p>
                </div>

                <button type="button" class="close btn_modal" data-dismiss="modal" aria-label="Close">
                    <span class="icon-cross btn_modal_close font_color"></span>
                </button>

                <div class="modal-body font_color">
                    <form action="/email/request/send">
                        <div class="alert alert-success" style="display: none;" role="alert">
                            {{ trans('front.email.success') }}
                        </div>
                        <input required type="text" name="name" class="modal_text font_color" aria-label="{{ trans('front.email.full_name')  }}" placeholder="{{trans('front.email.full_name')}}">
                        <input required type="tel" name="phone" class="modal_text font_color" aria-label="{{ trans('front.email.phone')  }}" placeholder="{{ trans('front.email.phone')  }}">
                        <input required type="email" name="email" class="modal_text font_color" aria-label="{{ trans('front.email.email')  }}" placeholder="{{ trans('front.email.email')  }}">
                        <input hidden name="program_id">

                        <textarea required name="comment" class="modal_comment font_color" id="comment" cols="30" rows="10" placeholder="{{ trans('front.email.comment')  }}"></textarea>
                        <input type="submit" value="{{ trans('front.form.send') }}" role="button" class="modal_submit programs_btn font_color">
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('[name="kind"]').on('change', function() {
                var targetId = $(this).attr('id') + 'Tab';
                $('.pseudo-tab').hide(200);
                $('#' + targetId).show(200);

                $('.programs_slider').slick('setPosition');

                targetId = $(this).attr('id') + 'Text';
                $('#excursionsText, #programsText').hide(200);
                $('#' + targetId).show(200);
            });
        });
    </script>
@endpush