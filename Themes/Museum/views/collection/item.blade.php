@extends('layouts.master')

@section('header')
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e00b99a3285330013fc0c50&product=custom-share-buttons&cms=sop' async='async'></script>
@endsection

@section('content')
<div class="main_container back3 color2 font_color bg_color" id="content">
    <div class="way_list_container back3 font_color bg_color">
        <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут">
            <?php $root_page = Slug::page(43) ?>
            <a href="{!! route('page', $root_page->slug) !!}" class="way_list_link" aria-label="розділ сайту">{{ $root_page->title }}<span class="icon-arrow_down"></span></a>
            <?php $sub_page = Slug::page($item->featured ? 15 : 4) ?>
            <a href="{!! route('page', $sub_page->slug) !!}" class="way_list_link type1" aria-label="розділ сторінки">{{ $sub_page->title }}<span class="icon-arrow_down"></span></a>
            <a href="#" class="way_list_link type2" aria-label="сторінка"><p>{{ $item->name }}</p></a>
        </div>
    </div>

    <div class="collection_page_container">
        <div class="collection_page_title_foto" role="region" aria-label="слайдер" id="main_slider">
            @foreach ($item->images as $idx => $image)
                <div class="collection_page_item" aria-live="polite" aria-label="{{ $idx }} слайд із {{ count($item->files) }}">
                    <img src="{!! $image->path !!}" alt="{{ $image->alt_attribute }}">
                </div>
            @endforeach
        </div>


        <div class="collection_page_link">
            <div class="work_title color2 type_coll_page type_collection font_color">
                <h1>{{ $item->name }}</h1>
            </div>

            <div class="collection_link_container font_color">
                <a href="#">
                    <div class="collection_link bg_color" data-network="">
                        <span class="icon-insta"></span>
                    </div>
                </a>
                <a href="#">
                    <div class="collection_link bg_color st-custom-button" data-network="facebook">
                        <span class="icon-f"></span>
                    </div>
                </a>
                <a href="#">
                    <div class="collection_link bg_color st-custom-button" data-network="linkedin">
                        <span class="icon-in"></span>
                    </div>
                </a>
                <a href="#">
                    <div class="collection_link bg_color st-custom-button" data-network="twitter">
                        <span class="icon-t"></span>
                    </div>
                </a>
                <a href="#">
                    <div class="collection_link_like font_color st-custom-button" data-network="facebook">
                        <span class="icon-love"></span>
                        <p><span class="count"></span></p>
                    </div>
                </a>
            </div>
        </div>

        <div class="collection_page_info">
            <div class="row h-100">
                <div class="col-12 col-lg-6 order-2 order-lg-1">
                    <div class="collection_info_type bg_color">
                        @if ($item->author)
                            <div class="collection_info_item">
                                <div class="collection_info_item_name">
                                    <h4>{{ trans('collection::items.frontend.author') }}:</h4>
                                </div>
                                <div class="collection_info_item_text">
                                    <p> {{ $item->author }} </p>
                                </div>
                            </div>
                        @endif
                        @if($item->name)
                            <div class="collection_info_item">
                                <div class="collection_info_item_name">
                                    <h4>{{ trans('collection::items.frontend.name') }}:</h4>
                                </div>
                                <div class="collection_info_item_text">
                                    <p>{{ $item->name }}</p>
                                </div>
                            </div>
                        @endif
                        @if($item->creation_time)
                            <div class="collection_info_item">
                                <div class="collection_info_item_name">
                                    <h4>{{ trans('collection::items.frontend.creation_time') }}:</h4>
                                </div>
                                <div class="collection_info_item_text">
                                    <p>{{ $item->creation_time }}</p>
                                </div>
                            </div>
                        @endif
                        <div class="collection_info_item">
                            <div class="collection_info_item_name">
                                <h4>{{ trans('collection::items.frontend.place') }}:</h4>
                            </div>
                            <div class="collection_info_item_text">
                                <p>{{ $item->place }}</p>
                            </div>
                        </div>
                        @if ($item->material)
                            <div class="collection_info_item">
                                <div class="collection_info_item_name">
                                    <h4>{{ trans('collection::items.frontend.material') }}:</h4>
                                </div>
                                <div class="collection_info_item_text">
                                    <p>{{ $item->material }}</p>
                                </div>
                            </div>
                        @endif
                        @if($item->size)
                            <div class="collection_info_item">
                                <div class="collection_info_item_name">
                                    <h4>{{ trans('collection::items.frontend.size') }}:</h4>
                                </div>
                                <div class="collection_info_item_text">
                                    <p>{{ $item->size }}</p>
                                </div>
                            </div>
                        @endif
                        @if($item->inventory_number)
                            <div class="collection_info_item">
                                <div class="collection_info_item_name">
                                    <h4>{{ trans('collection::items.frontend.inventory_number') }}:</h4>
                                </div>
                                <div class="collection_info_item_text">
                                    <p>{{ $item->inventory_number }}</p>
                                </div>
                            </div>
                        @endif
                        @if($item->source)
                            <div class="collection_info_item">
                                <div class="collection_info_item_name">
                                    <h4>{{ trans('collection::items.frontend.source') }}:</h4>
                                </div>
                                <div class="collection_info_item_text">
                                    <p>{{ $item->source }}</p>
                                </div>
                            </div>
                        @endif
                        @if($item->publications)
                                <div class="collection_info_item">
                                    <div class="collection_info_item_name">
                                        <h4>{{ trans('collection::items.frontend.publications') }}:</h4>
                                    </div>
                                    <div class="collection_info_item_text">
                                        <p>{{ $item->publications }}</p>
                                    </div>
                                </div>
                        @endif
                            @if($item->expositions)
                                <div class="collection_info_item">
                                    <div class="collection_info_item_name">
                                        <h4>{{ trans('collection::items.frontend.expositions') }}:</h4>
                                    </div>
                                    <div class="collection_info_item_text">
                                        <p>{{ $item->expositions }}</p>
                                    </div>
                                </div>
                            @endif
                        <div class="collection_info_item">
                            <div class="collection_info_item_name">
                                <h4>{{ trans('collection::items.frontend.section') }}:</h4>
                            </div>
                            <div class="collection_info_item_text">
                                <p>{{ $item->section->name }}</p>
                            </div>
                        </div>
                        {{--<div class="collection_info_item">--}}
                            {{--<div class="collection_info_item_name">--}}
                                {{--<h4>{{ trans('collection::items.frontend.type') }}:</h4>--}}
                            {{--</div>--}}
                            {{--<div class="collection_info_item_text">--}}
                                {{--<p>{{ $item->type->name }}</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
                <div class="col-12 col-lg-6 order-1 order-lg-2">
                    <div class="collection_info_type type1 bg_color">
                        @if($item->description)
                            <h2>{{ trans('collection::items.frontend.description') }}</h2>
                            <div style="text-indent: 20px; text-align: justify;">
                                {!! $item->formatted_text !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>



    </div>



</div>
@endsection
