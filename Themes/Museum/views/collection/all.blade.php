@extends('layouts.master')

@section('content')
    <div class="main_container back3 color2 font_color bg_color" id="content">
        <div class="way_list_container back3 font_color bg_color">
            <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection font_color">
                <h1>{{$page->title}}</h1>
            </div>

            <div class="work_text color2 type_sort font_color">
                <p>{{$page->body}}</p>
            </div>

            {!! Collection::filters() !!}
        </div>

        <section>
            <div class="all_coll_content">
                {!! Collection::all() !!}
            </div>
        </section>


    </div>

@endsection
