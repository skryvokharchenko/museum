@extends('layouts.master')

@section('content')
    <div class="main_container back3 color2 font_color bg_color" id="content">
        <div class="way_list_container back3 font_color bg_color">
            <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection font_color">
                <h1>{{ $page->title }}</h1>
            </div>

            <div class="work_text color2 type_col font_color">
                {!! $page->body !!}
            </div>
        </div>

        <?php list($random, $featured) = Collection::random() ?>

        <section>
            <div class="collection_container bg_color">
                <div class="content_container">
                    <div class="collection_foto_container">
                        <?php $page_item = Slug::page(15) ?>
                            <a style="text-decoration: none; outline: none;" href="{!! route('page', $page_item->slug) !!}" aria-label="посилання на сторінку">
                        <img src="{!! $featured->image_url !!}" alt="{{ $featured->alt_text }}">

                        <div class="collection_section_title font_color">
                            <h2>{{ $page_item->title }}</h2>
                        </div>
                            </a>
                    </div>
                </div>
            </div>
        </section>


        <section>
            <div class="collection_container bg_color type2">
                <div class="content_container">
                    <div class="collection_foto_container type2">
                        <?php $page_item = Slug::page(4) ?>
                            <a style="text-decoration: none; outline: none;" href="{!! route('page', $page_item->slug) !!}" aria-label="посилання на сторінку">
                        <img src="{!! $random->image_url !!}" alt="{{ $random->alt_text }}">

                        <div class="collection_section_title type2 font_color">
                            <h2>{{ $page_item->title }}</h2>
                        </div>
                            </a>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
