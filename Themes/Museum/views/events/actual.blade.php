@extends('layouts.master')

@section('content')
    <div class="main_container action_main type1 font_color bg_color" id="content">
        <div class="way_list_container font_color bg_color">
            <div class="way_list font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title font_color">
                <h1>{{ $page->title }}</h1>
            </div>

            <div class="work_text mod1 font_color">
                {!! $page->body !!}
            </div>
        </div>
    </div>

    <section><!--action-->
        <div class="action_container font_color bg_color">
            <div class="content_container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-7">
                        <div class="action_it bg_color">
                            <a href="{!! route('page', Slug::page(11)->slug) !!}" aria-label="Актуальні події">
                                <div class="action_it_foto">
                                    <img src="{!! Slug::page(11)->image !!}" alt="">
                                </div>
                                <div class="action_it_info font_color">
                                    <h2>{{ Slug::page(11)->title }}</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-5">
                        <div class="action_it bg_color">
                            <a href="{!! route('page', Slug::page(13)->slug) !!}" aria-label="Архів">
                                <div class="action_it_foto">
                                    <img src="{!! Slug::page(13)->image !!}" alt="">
                                </div>
                                <div class="action_it_info font_color">
                                    <h2>{{ Slug::page(13)->title }}</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--action-->

    {!! Events::actual() !!}
@endsection
