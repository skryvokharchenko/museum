@foreach ($items as $title => $url)
    <a href="{{$url}}" class="way_list_link" aria-label="{{$loop->last ? 'сторінка' : 'розділ сайту'}}">{{$title}}@if(!$loop->last)
<span class="icon-arrow_down"></span>
@endif
</a>
@endforeach