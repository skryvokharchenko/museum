<div class="slider_container nowrap" id="slider" role="region" aria-label="слайдер">
    @foreach($slider->slides as $index => $slide)
        <div class="slider_item" aria-live="polite" aria-label="{{$index + 1}} слайд із {{count($slider->slides)}}">
            <div class="slider_foto bg_color">
                <img src="{{$slide->getImageUrl()}}" role="tabpanel" alt="{{$slide->title}}">
            </div>
            <article>
                <div class="slider_info font_color bg_color">
                    <a href="{{$slide->getLinkUrl()}}">
                        <h2>{{$slide->caption}}</h2>
                    </a>
                </div>
            </article>
        </div>
    @endforeach
</div>

<div class="slider_btn_container">
    <div class="btn_pause bg_color font_color active" tabindex="-1" title="призупинити слайдер" aria-label="зупинити слайдер" role="button" id="btn_pause">
        <span class="icon-iconfinder_icon-pause_211871"></span>
    </div>
    <div class="btn_play bg_color font_color active" tabindex="-1" title="увімкнути слайдер" aria-label="програти слайдер" role="button" id="btn_play">
        <span class="icon-iconfinder_icon-play_211876"></span>
    </div>
</div>
