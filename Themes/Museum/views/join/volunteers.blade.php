@extends('layouts.master')

@section('content')
    <div class="main_container back2 color2 font_color bg_color" id="content">
        <div class="way_list_container back2 font_color bg_color">
            <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 font_color">
                <h1>{{ $page->title }}</h1>
            </div>

            <div class="visit_text color2 font_color">
                {!! $page->body !!}
            </div>
            <div class="download_btn_container">
                <a href="/assets/media/pdf/zasadi-volonterskoi-roboti-001-2.pdf" target="_blank" class="download_btn font_color bg_color" role="button" aria-label="завантажити" title="завантажити">PDF</a>
            </div>

            <section>
                <div class="volunteers_container">
                    {!! Publications::grid($page->id) !!}
                </div>
            </section>
        </div>
    </div>
@endsection
