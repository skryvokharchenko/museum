@extends('layouts.master')

@section('content')
    <div class="main_container back2 color2 font_color bg_color" id="content">
        <div class="way_list_container back2 font_color bg_color">
            <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 font_color">
                <h1>{!! $page->title !!}</h1>
            </div>

            <div class="work_text color2 mod1 font_color">
                <p>{!! $page->body !!}</p>
            </div>
        </div>

        <section>
            <div class="partners_slider_container" role="region" aria-label="слайдер" id="partners_slider">
                @foreach([1, 2, 3, 4, 1] as $item)
                    <div class="partners_slider_item " aria-live="polite" aria-label="{{$item}} слайд із 5">
                        <div class="partners_slider_it_container bg_color">
                            <img src="{!! Theme::url('img/join/slide' . $item .'.svg') !!}" alt="">
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection