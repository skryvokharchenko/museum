@extends('layouts.master')

@section('content')
    <div class="main_container back2 color2 font_color bg_color" id="content">
        <div class="way_list_container back2 font_color bg_color">
            <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 font_color">
                <h1>{{ $page->title }}</h1>
            </div>

            <div class="work_text color2 mod1 font_color">
                <p>{!! $page->body !!}</p>
            </div>
        </div>

        <section>
            {!! Slider::render('organisation', 'templates/slider') !!}
        </section>



        <section>
            <div class="organization_section font_color bg_color">
                <div class="contact_form_container bg_color font_color">
                    <form action="/email/comment/send" class="contact-form">
                        <div class="alert alert-success" style="display: none;" role="alert">
                            {{ trans('front.email.success') }}
                        </div>
                        <fieldset class="form_fieldset">
                            <Legend class="form_title font_color">Напишіть нам</legend>
                            <div class="form_flex_container">
                                <Label for="mail">Email</label>
                                <Input type="email" name="email" class="input_text font_color" id="mail" placeholder="Email">
                                <Label for="topic">Тема</label>
                                <Input type="text" name="subject" class="input_text mod1 font_color" placeholder="Тема" id="topic">
                                <Label for="text">Повідомлення</label>
                                <textarea placeholder="Повідомлення" name="comment" class="input_textatea font_color" id="text"></textarea>
                                <Label for="btn_form">Надіслати</label>
                                <Input type="submit" role="button" class="input_btn bg_color font_color" id="btn_form" value="Надіслати">
                            </div>
                        </fieldset>

                    </form>
                </div>
            </div>
        </section>
    </div>
@stop
