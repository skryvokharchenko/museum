@extends('layouts.master')

@section('content')
    <div class="main_container back2 color2 font_color bg_color" id="content">
        <div class="way_list_container back2 font_color bg_color">
            <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 font_color">
                <h1>{{ $page->title }}</h1>
            </div>
        </div>

        <section>
            {!! Slider::render('patreons', 'templates/slider') !!}
        </section>



        <section>
            <div class="homelend_section font_color bg_color">
                {!! Publications::list($page->id, 'type2') !!}
            </div>
        </section>
    </div>
@stop
