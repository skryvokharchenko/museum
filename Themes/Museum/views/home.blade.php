@extends('layouts.master')

@section('content')
    <div class="main_container font_color bg_color" id="content">
        <div class="main_title font_color custom-underline">
            <h1>{{ $page->title }}</h1>
        </div>

        <section>
            {!! Slider::render('homepage-slider', 'templates/slider') !!}
        </section>
    </div>
@endsection
