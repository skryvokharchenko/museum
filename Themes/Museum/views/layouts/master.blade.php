<!doctype html>
<html lang="uk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1" >
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @yield('header')

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="//khanenkomuseum.kiev.ua/themes/museum/css/colorpicker.css">
    <link rel="stylesheet" href="//khanenkomuseum.kiev.ua/themes/museum/css/icon.css">
    <link rel="stylesheet" href="//khanenkomuseum.kiev.ua/themes/museum/css/fonts/font.css">
    <link rel="stylesheet" href="//khanenkomuseum.kiev.ua/themes/museum/css/style.css">
    <link rel="stylesheet" href="//khanenkomuseum.kiev.ua/themes/museum/css/madia.css">


    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    <title>Найцінніша колекція світового мистецтва в Україні, заснована Богданом і Варварою Ханенками</title>

</head>
<body>
    @include('partials.header')
    <main>
        @yield('content')
    </main>
    @include('partials.footer')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="//khanenkomuseum.kiev.ua/themes/museum/js/slick.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="//khanenkomuseum.kiev.ua/themes/museum/js/colorpicker.js"></script>
    <script src="//khanenkomuseum.kiev.ua/themes/museum/js/app.js"></script>
@stack('scripts')
</body>
</html>
