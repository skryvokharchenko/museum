@extends('layouts.master')

@section('content')
    <div class="main_container back4 color2 font_color bg_color" id="content">
        <div class="way_list_container back4 font_color bg_color">
            <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection font_color">
                <h1>{!! $page->title !!}</h1>
            </div>

            <div class="work_text type_about color2 font_color">
                {!! $page->body !!}
            </div>
        </div>

        <section>
            <div class="about_container bg_color">
                <div class="content_container">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-5">
                            <div class="action_it type2 bg_color">
                                <?php $page_item = Slug::page(35) ?>
                                <a href="{!! route('page', $page_item->slug) !!}" aria-label="Архів">
                                    <div class="action_it_foto">
                                        <img src="{!! $page_item->image !!}" alt="">
                                    </div>
                                    <div class="action_it_info type2 font_color">
                                        <h2>{{ trans('front.3d tour') }}</h2>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-7">
                            <div class="action_it type2 bg_color">
                                <?php $page_item = Slug::page(32) ?>
                                <a href="{!! route('page', $page_item->slug) !!}" aria-label="Актуальні події">
                                    <div class="action_it_foto">
                                        <img src="{!! $page_item->image !!}" alt="">
                                    </div>
                                    <div class="action_it_info type2 font_color">
                                        <h2>{{ $page_item->title }}</h2>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-7">
                            <div class="action_it type2 bg_color">
                                <?php $page_item = Slug::page(36) ?>
                                <a href="{!! route('page', $page_item->slug) !!}" aria-label="Актуальні події">
                                    <div class="action_it_foto">
                                        <img src="{!! $page_item->image !!}" alt="">
                                    </div>
                                    <div class="action_it_info type2 font_color">
                                        <h2>{{ $page_item->title }}</h2>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-5">
                            <div class="action_it type2 bg_color">
                                <?php $page_item = Slug::page(25) ?>
                                <a href="{!! route('page', $page_item->slug) !!}" aria-label="Архів">
                                    <div class="action_it_foto">
                                        <img src="{!! $page_item->image !!}" alt="">
                                    </div>
                                    <div class="action_it_info type2 font_color">
                                        <h2>{{ $page_item->title }}</h2>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-5">
                            <div class="action_it type2 bg_color">
                                <?php $page_item = Slug::page(37) ?>
                                <a href="{!! route('page', $page_item->slug) !!}" aria-label="Архів">
                                    <div class="action_it_foto">
                                        <img src="{!! $page_item->image !!}" alt="">
                                    </div>
                                    <div class="action_it_info type2 font_color">
                                        <h2>{{ $page_item->title }}</h2>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-7">
                            <div class="action_it type2 bg_color">
                                <?php $page_item = Slug::page(23) ?>
                                <a href="{!! route('page', $page_item->slug) !!}" aria-label="Актуальні події">
                                    <div class="action_it_foto">
                                        <img src="{!! $page_item->image !!}" alt="">
                                    </div>
                                    <div class="action_it_info type2 font_color">
                                        <h2>{{ $page_item->title }}</h2>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            {!! Slider::render('about-slider', 'templates/slider') !!}
        </section>
    </div>
@stop
