@extends('layouts.master')

@section('content')
    <main>
        <div class="main_container back4 press_style color2 font_color bg_color" id="content">
            <div class="way_list_container back4 font_color bg_color">
                <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
            <div class="content_container">
                <div class="work_title color2 type_collection font_color">
                    <h1>{{ $page->title }}</h1>
                </div>

                <div class="work_text type_press type1 color2 font_color">
                    {!! $page->body !!}
                </div>
            </div>

            <section>
                <div class="experts_container bg_color">
                    <div class="content_container">
                        <div class="expert_title font_color">
                            <h2>{{ trans('team::departments.front.experts') }}:</h2>
                        </div>
                        {!! Team::experts() !!}
                    </div>
                </div>
            </section>

            <section>
                <div class="press_section bg_color">
                    <div class="press_section_info bg_color">
                        <h2>{{ $parts->firstWhere('system_name', 'expertise-contacts')->title }}:</h2>
                        <div class="ex_bottom_section">
                            {!! $parts->firstWhere('system_name', 'expertise-contacts')->body !!}
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@stop
