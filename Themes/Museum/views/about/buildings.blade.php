@extends('layouts.master')

@section('content')
    <div class="main_container back4 buildings_main color2 font_color bg_color" id="content">
        <div class="way_list_container back4 font_color bg_color">
            <div class="way_list_container back4 font_color bg_color">
                <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection font_color">
                <h1>{{ $page->title }}</h1>
            </div>

            <div class="work_text mod1 type_about color2 font_color">
                {!! $page->body !!}
            </div>
        </div>

        <section>
            <div class="building_container bg_color">
                <div class="content_container">
                    <div class="work_text mod1 type_about last_p active color2 font_color">
                        <div class="bild_section_title">
                            <h2>{{ $parts->firstWhere('system_name', 'dim-khanenkiv')->title }}</h2>
                        </div>
                        <article>
                            {!! $parts->firstWhere('system_name', 'dim-khanenkiv')->body !!}
                        </article>


                        <a href="#worK_text_link" role="button" role="button" data-more class="worK_text_link type2 font_color" aria-label="читати більше">{{ trans('front.read more') }}..</a>
                    </div>

                    <div class="work_text mod1 text_more color2 font_color">
                        <article>
                            {!! $parts->firstWhere('system_name', 'dim-khanenkiv-hidden')->body !!}
                        </article>

                        <a href="#worK_text_link" role="button" data-hide class="worK_text_link type2 font_color" aria-label="{{ trans('front.hide') }}">{{ trans('front.hide') }}</a>
                    </div>


                </div>
            </div>
        </section>

        <section>
            {!! Slider::render('dim-khanenkiv', 'templates/bigslider') !!}
        </section>

        <section>
            <div class="building_container type1 bg_color">
                <div class="content_container">
                    <div class="work_text mod1 type_about last_p active color2 font_color">
                        <div class="bild_section_title">
                            <h2>{{ $parts->firstWhere('system_name', 'dim-saknovskyh')->title }}</h2>
                        </div>
                        <article>
                            {!! $parts->firstWhere('system_name', 'dim-saknovskyh')->body !!}
                        </article>


                        <a href="#worK_text_link" role="button" data-more class="worK_text_link type2 font_color" aria-label="читати більше">{{ trans('front.read more') }}..</a>
                    </div>

                    <div class="work_text mod1 text_more color2 font_color">
                        <article>
                            {!! $parts->firstWhere('system_name', 'dim-saknovskyh-hidden')->body !!}
                        </article>

                        <a href="#worK_text_link" role="button" data-hide class="worK_text_link type2 font_color" aria-label="{{ trans('front.hide') }}">{{ trans('front.hide') }}</a>
                    </div>


                </div>
            </div>
        </section>

        <section>
            {!! Slider::render('dim-saknovskyh', 'templates/bigslider') !!}
        </section>


    </div>
@stop
