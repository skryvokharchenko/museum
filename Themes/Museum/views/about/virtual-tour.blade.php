@extends('layouts.master')

@section('content')
    <div class="main_container back4 mod1 color2 font_color bg_color" id="content">
        <div class="way_list_container back4 font_color bg_color">
            <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection type_tour font_color">
                <h1>{{ $page->title }}</h1>
                <p>{!! $page->body !!}</p>
            </div>

            <div class="store_item type3 font_color bg_color">
                <div class="item_foto">
                    <iframe width="700" height="472" src="https://my.matterport.com/show/?m=GR38x621JeA" frameborder="0" allowfullscreen allow="vr"></iframe>
                </div>
                <div class="store_info color2 font_color">
                    <div class="store_info_name virtual_info_name">
                        <p>{{ $parts->firstWhere('system_name', '3d-tour-1')->title }}</p>
                        <p>{!! $parts->firstWhere('system_name', '3d-tour-1')->body !!}</p>
                    </div>
                </div>
            </div>
            <div class="store_item type3 font_color bg_color">
                <div class="item_foto">
                    <iframe width="700" height="472" src="https://my.matterport.com/show/?m=zSQs9HAWM25" frameborder="0" allowfullscreen allow="vr"></iframe>
                </div>
                <div class="store_info color2 font_color">
                    <div class="store_info_name virtual_info_name">
                        <p>{{ $parts->firstWhere('system_name', '3d-tour-2')->title }}</p>
                        <p>{!! $parts->firstWhere('system_name', '3d-tour-2')->body !!}</p>
                    </div>
                </div>
            </div>

        </div>

    </div>
@stop
