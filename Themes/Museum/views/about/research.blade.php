@extends('layouts.master')

@section('content')
    <div class="main_container back4 history_main color2 font_color bg_color" id="content">
        <div class="way_list_container back4 font_color bg_color">
            <div class="way_list_container back4 font_color bg_color">
                <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection font_color">
                <h1>{{ $page->title }}</h1>
            </div>

            <div class="work_text type_about color2 font_color">
                {!! $page->body !!}
            </div>
        </div>

        <section>
            <div class="research_container bg_color">
                <div class="content_container">
                    <div class="row justify-content-center ramp_mod">
                        <div class="col-12 col-md-6 col-xl-4">
                            <div class="research_column">
                                <a href="{!! route('page', Slug::page(34)->slug) !!}">
                                    <div class="column_foto">
                                        <img src="{!! Slug::page(34)->image !!}" alt="">
                                    </div>
                                    <div class="column_info type1 font_color bg_color">
                                        <p>{{ Slug::page(34)->title }}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-xl-4">
                            <div class="research_column">
                                <a href="{!! route('page', Slug::page(29)->slug) !!}">
                                    <div class="column_foto">
                                        <img src="{!! Slug::page(29)->image !!}" alt="">
                                    </div>
                                    <div class="column_info type1 font_color bg_color">
                                        <p>{{ Slug::page(29)->title }}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-xl-4">
                            <div class="research_column">
                                <a href="{!! route('page', Slug::page(30)->slug) !!}">
                                    <div class="column_foto">
                                        <img src="{!! Slug::page(30)->image !!}" alt="">
                                    </div>
                                    <div class="column_info type1 font_color bg_color">
                                        <p>{{ Slug::page(30)->title }}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
