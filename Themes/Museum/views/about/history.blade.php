@extends('layouts.master')

@section('content')
    <div class="main_container back4 history_main color2 font_color bg_color" id="content">
        <div class="way_list_container back4 font_color bg_color">
            <div class="way_list_container back4 font_color bg_color">
                <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection font_color">
                <h1>{{ $page->title }}</h1>
            </div>

            <div class="work_text type_about color2 font_color">
                {!! $page->body !!}
            </div>
        </div>

        <section>
            {!! Slider::render('about-history', 'templates/slider') !!}
        </section>

        <section>
            <div class="history_section font_color bg_color">
                {!! Publications::list($page->id) !!}
            </div>
        </section>


    </div>
@stop
