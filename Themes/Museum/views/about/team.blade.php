@extends('layouts.master')

@section('content')
    <div class="main_container back4 team_main collection_main color2 font_color bg_color" id="content">
        <div class="way_list_container back4 font_color bg_color">
            <div class="way_list_container back4 font_color bg_color">
                <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection font_color">
                <h1>{{ $page->title }}</h1>
            </div>
        </div>

        {!! Team::departments() !!}
    </div>
@stop
