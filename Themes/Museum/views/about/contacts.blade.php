@extends('layouts.master')

@section('content')
    <div class="main_container back4 collection_main color2 font_color bg_color" id="content">
        <div class="way_list_container back4 font_color bg_color">
            <div class="way_list_container back4 font_color bg_color">
                <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection font_color">
                <h1>{{ $page->title }}</h1>
            </div>

            <div class="work_text color2 font_color">
                {!! $page->body !!}
            </div>

            <div class="contact_container font_color">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="about_contact_it">
                            <div class="contact_it_title">
                                <span class="icon-mail1"></span>
                                <h2>Email</h2>
                            </div>
                            <div class="contact_it_text">
                                <p>khanenkomuseum@ukr.net <span>({{ trans('front.reception') }})</span></p>
                                <p>museumkhanenko@gmail.com <span>({{ trans('front.for press') }})</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="about_contact_it">
                            <div class="contact_it_title">
                                <span class="icon-phone"></span>
                                <h2>{{ trans('front.reception phones') }}</h2>
                            </div>
                            <div class="contact_it_text">
                                <p>+38 (044) 235-32-90</p>
                                <p>+38 (099) 667-49-06</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="about_contact_it">
                            <div class="contact_it_title">
                                <span class="icon-geo2"></span>
                                <h2>{{ trans('front.address') }}</h2>
                            </div>
                            <div class="contact_it_text">
                                <p>{{ trans('front.address first line') }}</p>
                                <p>{{ trans('front.address second line') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section><!--map-->
            <div class="map_container type1">
                <iframe src="https://www.google.com/maps/d/embed?mid=1JaD-cec24GGbEFOSsWm5LGHDKoqJVgS_" width="100%" height="100%" title="Мапа від Google" aria-label="Мапа від Google"></iframe>
            </div>
        </section><!--map-->

        <div class="content_container">
            <div class="section_button contact_button">
                <a href="#work_text" role="button" class="button_show font_color bg_color" aria-label="{{ trans('front.show route description') }}" title="{{ trans('front.show route description') }}" id="show_button">{{ trans('front.show route description') }}</a>
            </div>

            <div class="work_text font_color color2" id="show_tabs">
                {!! $parts->firstWhere('system_name', 'about-contacts-description')->body !!}
            </div>
        </div>

        <section>
            <div class="organization_section type2 font_color bg_color">
                <div class="contact_form_container bg_color font_color">
                    <form action="/email/comment/send" class="contact-form">
                        <div class="alert alert-success" style="display: none;" role="alert">
                            {{ trans('front.email.success') }}
                        </div>
                        <fieldset class="form_fieldset">
                            <Legend class="form_title font_color">{{ trans('front.write us') }}</legend>
                            <div class="form_flex_container">
                                <Label for="mail">Email</label>
                                <Input type="email" required name="email" class="input_text font_color" id="mail" placeholder="Email">
                                <Label for="topic">{{ trans('front.form.subject') }}</label>
                                <Input type="text" required name="subject" class="input_text mod1 font_color" placeholder="{{ trans('front.form.subject') }}" id="topic">
                                <Label for="text">{{ trans('front.form.message') }}</label>
                                <textarea name="comment" requried placeholder="{{ trans('front.form.message') }}" class="input_textatea font_color" id="text"></textarea>
                                <Label for="btn_form">{{ trans('front.form.send') }}</label>
                                <Input type="submit" class="input_btn type1 bg_color font_color" id="btn_form" value="{{ trans('front.form.send') }}">
                            </div>
                        </fieldset>

                    </form>
                </div>
            </div>
        </section>

    </div>
@stop
