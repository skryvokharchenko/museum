@extends('layouts.master')

@section('content')
    <div class="main_container back4 color2 font_color bg_color" id="content">
        <div class="way_list_container back4 font_color bg_color">
            <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут" aria-label="ви знаходитесь тут">
                {!! Slug::bread($page->id) !!}
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_founded type_collection type_tour font_color">
                <h1>{{ $page->title }}</h1>
                <p>{!! $page->body !!}</p>
            </div>

        </div>

        <section>
            <div class="founded_container bg_color" data-bgmore>
                <div class="content_container founded">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="founded_foto_container">
                                <img src="{!! $parts->firstWhere('system_name', 'founders-bogdan-show')->image_url !!}" alt="">
                                <p>{{ $parts->firstWhere('system_name', 'founders-bogdan-show')->alt_text }}</p>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="founded_info_container">
                                <div class="work_text mod1 type_about last_p active color2 font_color">
                                    <div class="bild_section_title">
                                        <h2>{{ $parts->firstWhere('system_name', 'founders-bogdan-show')->title }}</h2>
                                    </div>
                                    <article>
                                        {!! $parts->firstWhere('system_name', 'founders-bogdan-show')->body !!}
                                    </article>


                                    <a href="#worK_text_link" role="button" data-more class="worK_text_link type2 font_color" aria-label="читати більше">{{ trans('front.read more') }}..</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="work_text mod1 text_more founded_text color2 font_color">
                                <article>
                                    {!! $parts->firstWhere('system_name', 'founders-bogdan-hidden')->body !!}
                                </article>

                                <a href="#worK_text_link" role="button" data-hide class="worK_text_link type2 font_color" aria-label="{{ trans('front.hide') }}">{{ trans('front.hide') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="founded_container type1 bg_color" data-bgmore>
                <div class="content_container founded">
                    <div class="row">
                        <div class="col-12 col-lg-6 order-lg-2">
                            <div class="founded_foto_container type1">
                                <img src="{!! $parts->firstWhere('system_name', 'founders-varvara-show')->image_url !!}" alt="">
                                <p>{{ $parts->firstWhere('system_name', 'founders-varvara-show')->alt_text }}</p>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 order-lg-1">
                            <div class="founded_info_container type1">
                                <div class="work_text mod1 type_about last_p active color2 font_color">
                                    <div class="bild_section_title">
                                        <h2>{{$parts->firstWhere('system_name', 'founders-varvara-show')->title}}</h2>
                                    </div>
                                    <article>
                                        {!! $parts->firstWhere('system_name', 'founders-varvara-show')->body !!}
                                    </article>


                                    <a href="#worK_text_link" role="button" data-more class="worK_text_link type2 font_color" aria-label="читати більше">{{ trans('front.read more') }}..</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 order-lg-3">
                            <div class="work_text mod1 text_more founded_text type1 color2 font_color">
                                <article>
                                    {!! $parts->firstWhere('system_name', 'founders-varvara-hidden')->body !!}
                                </article>

                                <a href="#worK_text_link" role="button" data-hide class="worK_text_link type2 font_color" aria-label="{{ trans('front.hide') }}">{{ trans('front.hide') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@stop
