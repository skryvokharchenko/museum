@extends('layouts.master')

@section('content')
    <div class="main_container back4 press_style color2 font_color bg_color" id="content">
        <div class="way_list_container back4 font_color bg_color">
            <div class="way_list_container back4 font_color bg_color">
                <div class="way_list color2 font_color bg_color" aria-label="ви знаходитесь тут" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
        </div>
        <div class="content_container">
            <div class="work_title color2 type_collection font_color">
                <h1>{{ $page->title }}</h1>
            </div>
        </div>

        <section>
            <div class="library_container bg_color">
                <div class="library_content">
                    <div class="row">
                        <div class="col-12 col-md-6 col-xl-5">
                            <div class="library_foto">
                                <img src="{!! $parts->firstWhere('system_name', 'library-1')->image_url !!}"
                                     alt="{{ $parts->firstWhere('system_name', 'library-1')->alt_text }}" role="presentation">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-xl-7">
                            <div class="library_section_info">
                                <h2>{{$parts->firstWhere('system_name', 'library-1')->title}}</h2>
                                {!! $parts->firstWhere('system_name', 'library-1')->body !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section>
            <div class="library_container type2 bg_color">
                <div class="library_content">
                    <div class="row">
                        <div class="col-12 col-md-6 order-2 order-md-1">
                            <div class="library_section_info type1">
                                <h2>{{ $parts->firstWhere('system_name', 'library-2')->title}}</h2>
                                {!! $parts->firstWhere('system_name', 'library-2')->body !!}
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-1 order-md-2">
                            <div class="library_foto_section">
                                <img src="{!! $parts->firstWhere('system_name', 'library-2')->image_url !!}"
                                     alt="{{ $parts->firstWhere('system_name', 'library-2')->alt_text }}" role="presentation">
                            </div>

                            <div class="library_section_info type2">
                                {!! $parts->firstWhere('system_name', 'library-3')->body !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
