@extends('layouts.master')

@section('content')
    <section>
        <div class="main_container audio_main type1 font_color bg_color" id="content">
            <div class="way_list_container font_color bg_color">
                <div class="way_list font_color bg_color" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
            <div class="content_container">
                <div class="work_title gaide_type font_color">
                    <h1>{{ $page->title }}</h1>
                </div>
            </div>

            <div class="gaide_container"><!--gaide-->
                <div class="row ">

                    <div class="col-12 col-lg-6">
                        <div class="photographing_text font_color">
                            {!! $page->body !!}
                        </div>
                    </div>

                    <div class="col-12 col-lg-6">
                        <div class="photographing_img_container bg_color">
                            <img src="{!! $page->image !!}" alt="{{ $page->image_alt }}" role="presentation">
                        </div>
                    </div>


                </div>
            </div><!--gaide-->
        </div>
    </section>
@stop
