@extends('layouts.master')

@section('content')
    <section>
        <div class="main_container audio_main type1 font_color bg_color" id="content">
            <div class="way_list_container font_color bg_color">
                <div class="way_list font_color bg_color" aria-label="ви знаходитесь тут">
                    {!! Slug::bread($page->id) !!}
                </div>
            </div>
            <div class="content_container">
                <div class="work_title gaide_type font_color">
                    <h1>{{ $page->title }}</h1>
                </div>

                <div class="visit_text font_color">
                    {!! $page->body !!}
                </div>
            </div>

            <div class="gaide_container">
                <div class="row ">

                    <div class="col-12 col-lg-6">
                        <div class="photographing_text visiting_rules_text font_color">
                            {!! $parts->firstWhere('system_name', 'rules-content')->body !!}
                        </div>
                    </div>

                    <div class="col-12 col-lg-6">
                        <div class="visiting_rules_img_container bg_color">
                            <img src="{!! $parts->firstWhere('system_name', 'rules-content')->image_url !!}" alt="" role="presentation">
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
@stop
