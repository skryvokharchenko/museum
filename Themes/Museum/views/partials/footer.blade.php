<footer>
    <div class="footer_container">
        <div class="footer_top font_color bg_color">
            <div class="logo_footer_container">
                <span class="{{ trans('front.logo class') }} font_color"></span>

                <ul class="link_container font_color" aria-label='сторінки соціальниз мереж'>
                    <li>
                        <a href="#" class="font_color" aria-label="Інстаграм"><span class="icon-insta font_color"></span></a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/khanenkomuseum/" class="font_color" aria-label="Фейсбук"><span class="icon-f font_color"></span></a>
                    </li>
                    <li>
                        <a href="#" class="font_color" aria-label="Линкен Ин"><span class="icon-in font_color"></span></a>
                    </li>
                    <li>
                        <a href="#" class="font_color" aria-label="Твіттер"><span class="icon-t font_color"></span></a>
                    </li>
                </ul>
            </div>

            <div class="footer_item geo font_color">
                @if(App::getLocale() == 'en')
                    <div class="footer_it_title">
                        <h3>Address</h3>
                    </div>
                    <div class="footer_it_text">
                        <p>Ukraine, Kyiv,</p>
                        <p>Tereshchenkivska St., 15-17</p>
                    </div>
                    @else
                    <div class="footer_it_title">
                        <h3>Адреса</h3>
                    </div>
                    <div class="footer_it_text">
                        <p>Україна, м. Київ,</p>
                        <p>вул. Терещенківська, 15-17</p>
                    </div>
                    @endif
                <span class="icon-geo font_color"></span>
            </div>

            <div class="footer_item phone font_color">
                <div class="footer_it_title">
                    <h3>{{trans('front.phone')}}</h3>
                </div>
                <div class="footer_it_text">
                    <p>+380 44 288-14-50</p>
                    <p>+380 44 235-32-90</p>
                </div>

                <span class="icon-phone font_color"></span>
            </div>

            <div class="footer_item font_color">
                <div class="footer_it_title">
                    <h3>Email</h3>
                </div>
                <div class="footer_it_text">
                    <p>khanenkomuseum@ukr.net</p>
                    <p>museumkhanenko@gmail.com</p>
                </div>

                <span class="icon-mail font_color"></span>
            </div>

            <div class="footer_logo_comp">
                <a href="#">
                    <img src="//khanenkomuseum.kiev.ua/themes/museum/img/index/com_logo.svg" alt="Логотип Beonline.Studio">
                </a>
            </div>



        </div>
        <div class="footer_bottom bg_color">
            <h2 class="sr-only">Наші партнери</h2>
            <a href="#">
                <img src="//khanenkomuseum.kiev.ua/themes/museum/img/index/log2.svg" alt="Логотип Національного науково-дослідного реставраційного центру України">
            </a>
            <a href="#">
                <img src="//khanenkomuseum.kiev.ua/themes/museum/img/index/log7.svg" alt="Логотип Департаменту культури Київської міської державної адміністрації">
            </a>
            <a href="#">
                <img src="//khanenkomuseum.kiev.ua/themes/museum/img/index/log10.svg" alt="Логотип Українського культурного фонду">
            </a>
            <a href="#">
                <img src="//khanenkomuseum.kiev.ua/themes/museum/img/index/log9.svg" alt="Герб міста Київ">
            </a>
            <a href="#">
                <img src="//khanenkomuseum.kiev.ua/themes/museum/img/index/log11.svg" alt="Логотип Inclusive IT">
            </a>
            <a href="#">
                <img src="//khanenkomuseum.kiev.ua/themes/museum/img/index/com_logo.svg" alt="Логотип Beonline.Studio  ">
            </a>
        </div>
    </div>
</footer>
