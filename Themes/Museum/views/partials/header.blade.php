<header>
    <div class="go_to_main">
        <a href="#content" tabindex="0">
            <span class="sr-only">{{ trans('front.skip navigation') }}</span>
        </a>
    </div>
    <div class="header_container bg_color font_color" id="header_container">
        <div class="header_top">
            <div class="header_time_work font_color">
                <span class="icon-door font_color" id="icon_door"></span>
                <p id="work" class="work-open" role="presentation" aria-label="{{trans('front.open')}}">{{trans('front.open')}}</p>
                <p id="work" class="work-closed" role="presentation" aria-hidden="true" style="display: none;" aria-label="{{trans('front.closed')}}">{{trans('front.closed')}}</p>
                <p>{{trans('front.week.work days')}}</p>
            </div>

            <div class="header_seporation"></div>

            <div class="header_time_work mod1 font_color">
                <span class="icon-geo_top font_color"></span>
                @if(App::getLocale() == 'en')
                    <p>Kyiv, Tereshchenkivska St., 15-17</p>
                    @else
                    <p>м. Київ, вул. Терещенківська 15-17</p>
                    @endif
            </div>

            <div class="header_seporation type1"></div>

            <div class="lang_container_adaptive bg_color">
                <div class="lang_container font_color bg_color" id="lang_cont">
                    <div class="flag_container" id="flag_container"></div>
                    <select name="lang" id="lang" aria-label="вибір мови сайта" class="lang" onchange="location = this.options[this.selectedIndex].dataset.to">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <option {{ App::getLocale() == $localeCode ? 'selected' : '' }} value="{{$localeCode}}" data-to="{!! LaravelLocalization::getLocalizedURL($localeCode, Request::url(), Route::current()->parameters()) !!}">{{$localeCode}}</option>
                        @endforeach
                    </select>
                    <span class="icon-arrow_down font_color"></span>
                </div>
            </div>

            <div class="header_seporation type2"></div>

            <a href="#content" role="link" class="button_view" id="button_view" aria-label="Налаштування шрифтів та кольору сайту" title="Обрати зручну версію для зору">
                <span class="icon-view font_color"></span>
                <span class="sr-only">{{trans('front.color.title')}}</span>
            </a>

            <div class="view_container bg_color font_color" id="view_container">

                <div class="view_it_container type1">
                    <fieldset>
                        <legend>
                            <div class="view_title">
                                <p>{{trans('front.color.name')}}:</p>
                            </div>
                        </legend>
                        <div class="color_container">
                            <div class="color_it">
                                <a href="#" role="button" id="color1" class="color_type type0">{{trans('front.color.sepia')}}</a>
                            </div>
                            <div class="color_it">
                                <a href="#" role="button" id="color2" class="color_type type1">{{trans('front.color.black-white')}}</a>
                            </div>
                            <div class="color_it">
                                <a href="#" role="button" id="color3" class="color_type type2">{{trans('front.color.black-yellow')}}</a>
                            </div>
                            <div class="color_it">
                                <a href="#" role="button" id="color4" class="color_type type3">{{trans('front.color.blue-yellow')}}</a>
                            </div>
                            <div class="color_it">
                                <a href="#" role="button" id="color5" class="color_type type4">{{trans('front.color.green-white')}}</a>
                            </div>
                            <div class="color_it">
                                <a href="#" role="button" id="color6" class="color_type type5">{{trans('front.color.inverted')}}</a>
                            </div>
                            <div class="color_it">
                                <a href="#" role="button" id="color7" class="color_type type6">{{trans('front.color.white-black')}}</a>
                            </div>
                            <div class="color_it">
                                <a href="#" role="button" id="color8" class="color_type type7">{{trans('front.color.yellow-black')}}</a>
                            </div>
                            <div class="color_it">
                                <a href="#" role="button" id="color9" class="color_type type8">{{trans('front.color.yellow-blue')}}</a>
                            </div>
                            <div class="color_it">
                                <a href="#" role="button" id="color10" class="color_type type9">{{trans('front.color.white-green')}}</a>
                            </div>
                        </div>
                    </fieldset>
                </div>

                <div class="view_it_sep"></div>

                <div class="view_it_container">
                    <fieldset>
                        <legend>
                            <div class="view_title">
                                <p>{{trans('front.color.select')}}:</p>
                            </div>
                        </legend>
                        <div class="colorSelector font_color" id="colorSelector1">
                            <div role="button" tabindex="0" style="background-color: #FFFFFF">
                                <p class="font_color color_picker mod">{{trans('front.color.select text color')}}</p>
                            </div>
                        </div>

                        <div class="colorSelector" id="colorSelector">
                            <div role="button" tabindex="0" style="background-color: #3BAEF4">
                                <p class="font_color color_picker">{{trans('front.color.select back color')}}</p>
                            </div>
                        </div>
                    </fieldset>
                </div>

                <div class="view_it_sep mod1"></div>

                <div class="view_it_container">
                    <fieldset>
                        <legend>
                            <div class="view_title mod1">
                                <p>{{trans('front.font.size')}}:</p>
                            </div>
                        </legend>
                        <div class="fonts_container">
                            <a href="#" role="button" data-font="18" class="view_fonts mod1 active" title='шрифт 100%'>Aa</a>
                            <a href="#" role="button" data-font="24" class="view_fonts mod2" title='шрифт 130%'>Aa</a>
                            <a href="#" role="button" data-font="28" class="view_fonts mod3" title='шрифт 160%'>Aa</a>
                            <a href="#" role="button" data-font="36" class="view_fonts mod4" title='шрифт 200%'>Aa</a>
                        </div>
                    </fieldset>
                </div>

                <a href="#" role="button" class="button_close font_color" id="button_reload" area-lebel="відміна спеціальних налаштувань" title="відміна спеціальних налаштувань">
                    <span class="sr-only">відміна спеціальних налаштувань</span>
                    <span class="icon-reload"></span>
                </a>
                <a href="#" role="button" class="button_close type1 font_color" id="button_close" area-lebel="закрити налаштування шрифтів та кольору сайту" title="закрити налаштування шрифтів та кольору сайту">
                    <span class="sr-only">закрити налаштування шрифтів та кольору сайту</span>
                    <span class="icon-cross"></span>
                </a>
            </div>

            <button class="nav_toggle " id="nav_toggle"	type="button" title="Меню">
                <span class="nav_toggle_item">menu</span>
            </button>

        </div>
        <div class="header_bottom bg_color font_color">

            <div class="logo_container bg_color">
                <a href="/{{App::getLocale()}}" aria-label='логотип сайту музею Ханенків' class="font_color bg_color logo_img">
                    <span class="{{ trans('front.logo class') }}"></span>
                </a>
            </div>

            <nav class="bg_color">
                @menu('main')
            </nav>

            <div class="search_container" id="search">
                <form action="#" class="font_color">
                    <input type="search" class="input_search font_color" placeholder="{{ trans('front.search') }}" aria-label="{{ trans('front.search input') }}">
                    <div class="search_btn">
                        <input type="submit" aria-label="{{ trans('front.search') }}" title="{{ trans('front.search') }}" value="">
                        <span class="icon-search pos_icon font_color"></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>
