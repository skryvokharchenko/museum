<div class="work_main_bottom_container font_color bg_color">
    <div class="content_container">
        <div class="time_container">
            <div class="time_work font_color bg_color">
                <div class="name_day_container">
                    <div class="day_no_work font_color">
                        <div class="day_name"><span>{{trans('front.week.mo')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.tu')}}</span></div>
                    </div>
                    <div class="day_work font_color">
                        <div class="day_name"><span>{{trans('front.week.we')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.th')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.fr')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.sa')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.su')}}</span></div>
                    </div>
                </div>
                <div class="time_work_container font_color bg_color">
                    <span class="icon-clock"></span>
                    <p>10:30 - 17:30</p>
                </div>
            </div>
        </div>
        <div class="time_container">
            <div class="time_work type1 font_color bg_color">
                <div class="name_day_container">
                    <div class="day_work font_color">
                        <div class="day_name"><span>{{trans('front.week.mo')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.tu')}}</span></div>
                    </div>
                    <div class="day_no_work font_color">
                        <div class="day_name"><span>{{trans('front.week.we')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.th')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.fr')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.sa')}}</span></div>
                        <div class="day_name"><span>{{trans('front.week.su')}}</span></div>
                    </div>
                </div>
                <div class="time_work_container type1 font_color bg_color">
                    <p>{{trans('front.week.days off')}}</p>
                </div>
            </div>
        </div>

    </div>
</div>