<?php


namespace App\Facades;


use App\Presenters\SlugPresenter;
use Illuminate\Support\Facades\Facade;

class SlugFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return SlugPresenter::class;
    }
}
