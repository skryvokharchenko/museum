<?php


namespace App\Presenters;


use Illuminate\Support\Facades\View;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Repositories\Eloquent\EloquentPageRepository;
use Modules\Page\Repositories\PageRepository;

class SlugPresenter
{
    /**
     * @var PageRepository
     */
    private $page;
    /**
     * @var MenuItemRepository
     */
    private $menuItem;

    public function __construct(PageRepository $page, MenuItemRepository $menuItem)
    {

        $this->page = $page;
        $this->menuItem = $menuItem;
    }


    public function page($id)
    {
        $page = $this->page->find($id);

        if ($page == null) {
            return new Page;
        }

        return $page;
    }

    private function getParentItem($items, $parts, $item = null)
    {
        if (!$item) {
            return [$items, $parts];
        }

        $parentItem = $this->menuItem->find($item->parent_id);

        if (!$parentItem || $parentItem->title == 'root') {
            return [$items, $parts];
        }

        if ($parentItem->parent_id) {
            list($items, $parts) = $this->getParentItem($items, $parts, $parentItem);
        }

        if (count($parts) > 1) {
            $items[$parentItem->title] = route('page', $parts[0]);
        } else {
            $items[$parentItem->title] = $parentItem->uri ?? '#';
        }

        $key = array_shift($parts);
        if (count($parts) > 0) {
            $parts[0] = $key . "/" . $parts[0];
        }
        return [$items, $parts];

    }

    public function bread($page_id = null, $list = [])
    {
        if ($page_id) {
            $menuItem = $this->menuItem->findByAttributes(['page_id' => $page_id]);
            $page = $this->page->find($page_id);
            $parts = explode('/', $page->slug);

            list($items) = $this->getParentItem([], $parts, $menuItem);

            $items[$page->title] = route('page', $page->slug);
            array_merge($items, $list);

            $view = View::make('templates.breadcrumb')->with(compact('items'));
            return $view->render();
        }
        return '';
    }
}
