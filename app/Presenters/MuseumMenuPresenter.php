<?php

namespace App\Presenters;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Nwidart\Menus\MenuItem;
use Nwidart\Menus\Presenters\Presenter;

class MuseumMenuPresenter extends Presenter
{
    public function setLocale($item)
    {
        if (starts_with($item->url, 'http')) {
            return;
        }
        if (LaravelLocalization::hideDefaultLocaleInURL() === false) {
            $item->url = locale() . '/' . preg_replace('%^/?' . locale() . '/%', '$1', $item->url);
        }
    }
    /**
     * {@inheritdoc }.
     */
    public function getOpenTagWrapper()
    {
        return PHP_EOL . '<ul class="nav">' . PHP_EOL;
    }

    /**
     * {@inheritdoc }.
     */
    public function getCloseTagWrapper()
    {
        return PHP_EOL . '</ul>' . PHP_EOL;
    }

    /**
     * {@inheritdoc }.
     */
    public function getMenuWithoutDropdownWrapper($item)
    {
        $this->setLocale($item);

        return '<li ' . 'class="nav-item font_color' . $this->getActiveState($item) . '"><a href="' . $item->getUrl() . '" ' . $item->getAttributes() . '>' . $item->getIcon() . ' ' . $item->title . '</a></li>' . PHP_EOL;
    }

    public function getSubMenuItem($item)
    {
        $this->setLocale($item);

        return '<a href="' . $item->getUrl() . '" ' . $item->getAttributes() . '>' . $item->getIcon() . ' ' . $item->title . '</a>' . PHP_EOL ;
    }

    /**
     * {@inheritdoc }.
     */
    public function getActiveState($item, $state = 'active')
    {
        return $item->isActive() ? $state : null;
    }

    /**
     * Get active state on child items.
     *
     * @param $item
     * @param string $state
     *
     * @return null|string
     */
    public function getActiveStateOnChild($item, $state = 'active')
    {
        return $item->hasActiveOnChild() ? $state : null;
    }

    /**
     * {@inheritdoc }.
     */
    public function getMenuWithDropDownWrapper($item)
    {
        $hash = bin2hex(random_bytes(10));
        return '<li class="dropdown' . $this->getActiveStateOnChild($item, ' active') . '">
                  <a ' . $item->getAttributes() . ' class="nav-link" data-target="dropdown-'. $hash . '" dropdown-toggle role="button" aria-haspopup="true" aria-expanded="false">
                    ' . $item->getIcon() . ' ' . $item->title . '
                    <span class="icon-arrow_down font_color"></span>
                  </a>
                  <div class="dropdown-menu font_color bg_color" id="dropdown-' . $hash .'">
                    ' . $this->getChildMenuItems($item) . '
                  </div>
                </li>'
        . PHP_EOL;
    }

    /**
     * Get multilevel menu wrapper.
     *
     * @param MenuItem $item
     *
     * @return string`
     */
//    public function getMultiLevelDropdownWrapper($item)
//    {
//        return '<div class="dropdown' . $this->getActiveStateOnChild($item, ' active') . '">
//                  <a href="#" class="nav-link" dropdown-toggle data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
//                    ' . $item->getIcon() . ' ' . $item->title . '
//                    <span class="icon-arrow_down font_color"></span>
//                  </a>
//                  <div class="dropdown-menu font_color bg_color">
//                    ' . $this->getChildMenuItems($item) . '
//                  </div>
//                </div>'
//        . PHP_EOL;
//    }

    public function getMultiLevelDropdownWrapper($item)
    {
        $hash = bin2hex(random_bytes(10));
        return '<div class="dropright submenu' . $this->getActiveStateOnChild($item, ' active') . '">
                  <a ' . $item->getAttributes() .' class="dropdown-item" data-target="dropdown-'. $hash . '" dropdown-toggle role="button" aria-haspopup="true" aria-expanded="false">
                    ' . $item->getIcon() . ' ' . $item->title . '
                    <span class="icon-arrow_down font_color"></span>
                  </a>
                  <div class="dropdown-menu font_color bg_color" id="dropdown-' . $hash .'">
                    ' . $this->getChildMenuItems($item) . '
                  </div>
                </div>'
            . PHP_EOL;
    }

    public function getChildMenuItems(MenuItem $item)
    {
        $results = '';
        foreach ($item->getChilds() as $child) {
            if ($child->hidden()) {
                continue;
            }

            if ($child->hasSubMenu()) {
                $results .= $this->getMultiLevelDropdownWrapper($child);
            } elseif ($child->isHeader()) {
                $results .= $this->getHeaderWrapper($child);
            } elseif ($child->isDivider()) {
                $results .= $this->getDividerWrapper();
            } else {
                $results .= $this->getSubMenuItem($child);
            }
        }

        return $results;
    }
}
