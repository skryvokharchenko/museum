<?php

namespace App\Http\Controllers;

use App\Mail\RequestEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Modules\Programs\Repositories\ProgramRepository;

class EmailController extends Controller
{
    protected $program;

    public function __construct(ProgramRepository $programRepository)
    {
        $this->program =  $programRepository;
    }

    public function send($request_type)
    {
        $type = $request_type;
        $data = request()->all();

        if (!isset($data['subject'])) {
            $data['subject'] = ($type === 'request') ? 'Запит' : 'Коментар';
        }

        $program = null;
        if (isset($data['program_id'])) {
            $program = $this->program->find($data['program_id']);
        }

        $body = $data['comment'];
        $replyTo = ['address' => $data['email'], 'name' => $data['name'] ?? null];
        $phone = request()->get('phone', false);
        $subject = $data['subject'];

        try {
            Mail::send(new RequestEmail(compact('program', 'body', 'replyTo', 'type', 'phone', 'subject')));
        } catch (\Exception $e) {
            dd($e);
        }


        return response()->json(['success' => true]);

    }
}
